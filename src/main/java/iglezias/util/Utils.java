package iglezias.util;

import java.io.File;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.EnumSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class Utils {

	public static int getTamanhoTotalArquivosEmKb(List<File> arqs) {
		int res = 0;
		for (File f : arqs) {
			if (f.exists()) {
				res += f.length()/1024;
			}
		}
		return res;
	}

    /**
     * @param fpath
     * @param nivel nao utilizado no momento. passe 0.
     * @return a lista dos arquivos do fpath passado (incluindo arquivos em sub-pastas). Pastas vazias NAO serao retornadas.
     */
    public static List<File> listaPathsArquivosDiretorioRecursivamente(File fpath, int nivel)
	{
    	return listaPathsArquivosDiretorioRecursivamente(fpath, nivel, false);
	}

    /**
     * @param fpath
     * @param nivel
     * @param incluirPastasVazias se TRUE, as pastas vazias serao retornados como entradas no retorno.
     * @return
     */
    public static List<File> listaPathsArquivosDiretorioRecursivamente(File fpath, int nivel, boolean incluirPastasVazias)
	{
		List<File> arquivos = new ArrayList<>();
		//if(fpath.exists()) {
			File[] files = fpath.listFiles();
        	if (incluirPastasVazias && files.length==0) {
        		arquivos.add(fpath);
        	}
        	if (files!=null) {
        		for (File file : files) {
        			if (file.isDirectory()) {
        				arquivos.addAll(listaPathsArquivosDiretorioRecursivamente(file, ++nivel, incluirPastasVazias));
        			} else {
        				arquivos.add(file);
        			}
        		}
        	}
		//}
		return arquivos;
	}

	public static String substringEx(String str, int tam) {
		if (tam>=str.length()) {
			tam = str.length();
		}
		return str.substring(0, tam);
	}
	
	/**
	 * @param date1
	 * @param date2
	 * @return date2-date1
	 * ex: 
	 *   <code>
	 *      Map<TimeUnit, Long> dif = computeDiff(dt3,dt);
	 *      System.out.println(dif.get(TimeUnit.MINUTES)+" : "+dif.get(TimeUnit.SECONDS) );
	 *   </code>
	 */
	public static Map<TimeUnit,Long> computeDiff(Date date1, Date date2) {

	    long diffInMillies = date2.getTime() - date1.getTime();

	    //create the list
	    List<TimeUnit> units = new ArrayList<TimeUnit>(EnumSet.allOf(TimeUnit.class));
	    Collections.reverse(units);

	    //create the result map of TimeUnit and difference
	    Map<TimeUnit,Long> result = new LinkedHashMap<TimeUnit,Long>();
	    long milliesRest = diffInMillies;

	    for ( TimeUnit unit : units ) {
	        //calculate difference in millisecond 
	        long diff = unit.convert(milliesRest,TimeUnit.MILLISECONDS);
	        long diffInMilliesForUnit = unit.toMillis(diff);
	        milliesRest = milliesRest - diffInMilliesForUnit;

	        //put the result in the map
	        result.put(unit,diff);
	    }

	    return result;
	}

    /**
	 * @param valores
	 * @return a media aritmetica dos valores long passados.
	 */
	public static long mediaAritmetica(Long... valores) {
		if (valores==null || valores.length==0) {
			return 0;
		}
		long soma=0;
		for (long l : valores) {
			soma += l;
		}
		return soma / valores.length;
	}

	/**
	 * @param valores
	 * @return a media aritmetica dos valores long passados.
	 */
	public static long mediaAritmetica(List<Long> valores) {
		if (valores==null || valores.isEmpty()) {
			return 0;
		}
		
		long soma=0;
		for (long l : valores) {
			soma += l;
		}
		return soma / valores.size();
	}

	/**
	 * @param milis nro de milisegundos
	 * @param formatoTempo ex: mm:ss:SS   vide help mascaras para SimpleDateFormat.
	 * @return o tempo formatado em mm:ss:SS
	 */
	public static String getTempoFormatado(long milis, String formatoTempo) {
		Date tDecorrido = new Date(); /*tDecorrido.setTime(0);*/ tDecorrido.setTime(milis);
		SimpleDateFormat df = new SimpleDateFormat(formatoTempo);
		df.setTimeZone(TimeZone.getTimeZone("GMT")); //se nao setarmos GMT0 aki, a formatacao da hora fica errada (pega o fuso do brasil). Ex: 1h12min fica 20h12min (pq fica gmt-3)...
		String tempoFinalFmt = df.format(tDecorrido);
		return tempoFinalFmt;
	}

	/**
	 * 
	 * @param qtdDivisoesTabela
	 * @param percentual
	 * @param exibirTextoPercentual
	 * @return um html (sem as tags de <html>e</html>) de tabela que simula uma barra de progresso
	 */
	public static String getHTMLProgressoEmFormaTabela(int qtdDivisoesTabela, int percentual, boolean exibirTextoPercentual) {
		StringBuilder testeHTML = new StringBuilder("<table frame=\"box\" width=\"100\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >");
		testeHTML.append("<tr>");
		for (int i=0; i<=qtdDivisoesTabela; i++) {
			testeHTML.append("<td ");
			if (i < percentual / ((100/qtdDivisoesTabela)) ) {
				testeHTML.append("bgcolor=\"green\"");
			} else {
				testeHTML.append("bgcolor=\"white\"");
			}
			testeHTML.append(">");
//			if (exibirTextoPercentual && i==(qtdDivisoesTabela-1)) {
//				testeHTML.append(DecimalFormat.getPercentInstance().format(percentual/100f));
//			}
			testeHTML.append("</td>");
		}
		if (exibirTextoPercentual) {
			testeHTML.append("<td>&nbsp;").append(DecimalFormat.getPercentInstance().format(percentual/100f)).append("</td>");
		}
		testeHTML.append("</tr>");
		testeHTML.append("</table>");
		return testeHTML.toString();
	}

    public static final String getExtensaoSemPonto(File f) {
        return getExtensaoSemPonto(f.getName());
    }
    public static final String getExtensaoSemPonto(String filename) {
        int whereDot = filename.lastIndexOf( '.' );
        if (whereDot>=0  &&  filename.lastIndexOf(File.separator)>whereDot) {
			whereDot=filename.length(); //evita problemas com arquivos sem extensao do tipo: C:\Inetpub.yyy\wwwrootOrig
		}
        if (whereDot>0  &&  whereDot<=(filename.length()-2) ) {
			return filename.substring(whereDot+1);
		} else {
			return "";
		}
    }
    public static final File trocaExtensao(File f, String novaExt) {
        novaExt = ( (novaExt!=null && !novaExt.trim().equals("")) ? "."+novaExt  : "" );
        return new File(extraiNomeAquivoSemExtensao(f.getAbsolutePath()) + novaExt );
    }

    /**
     * FROM serpro.ppgd.negocio.util.UtilitariosArquivo
     * A partir do string com o nome do arquivo, retorna um string contendo nome
     * do arquivo sem a extensao. Considera como separador de nome de arquivo a constante definida em
     * ConstantesGlobais.
     * obs. É apenas eliminada extensao. Se for passado o caminho completo, retornara o caminho completo
     * menos a extensao.
     */
    public static String extraiNomeAquivoSemExtensao( String nomeArquivo ){
        int idxFim=nomeArquivo.lastIndexOf( "." );
        if (idxFim>=0  &&  nomeArquivo.lastIndexOf(File.separator)>idxFim) {
			idxFim=nomeArquivo.length(); //evita problemas com arquivos sem extensao do tipo: C:\Inetpub.yyy\wwwrootOrig
		}
        return nomeArquivo.substring( 0, (idxFim>=0) ? idxFim : nomeArquivo.length());
    }
    
}
