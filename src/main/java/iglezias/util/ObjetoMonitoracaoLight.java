package iglezias.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

public class ObjetoMonitoracaoLight {
	private static class ParametrosMonitoracao {
		String chave;
		long horaInicioMonitoracao;
		long memInicioMonitoracao;
		List<Long> ultimasEstimativasTempoTotal;
		long horaUltimaMedicaoEstimativaTempoTotal;
		public ParametrosMonitoracao(String chave) {
			this.chave = chave;
			this.horaInicioMonitoracao = System.currentTimeMillis();
			this.memInicioMonitoracao = Runtime.getRuntime().freeMemory();
			this.ultimasEstimativasTempoTotal = new ListaCircular<>(10); //ultimas 10 estimativas de tempo total
			this.horaUltimaMedicaoEstimativaTempoTotal = 0; //tempo ultima medicao estimativa
		}
		public long getTempoAteAgora() {
			return System.currentTimeMillis() - this.horaInicioMonitoracao;
		}
	}
	
	
	/**
	 * Exemplo de uso: {@link EventoProgressoComposto#main(String[])}
	 * @param chave
	 * @param progressoPercentual
	 * @return o1 = tempoDecorrido, o2 = tempoEstimadoTotal
	 */
	public static Dupla<Long,Long> getEstimativaTempoTotal(String chave, double progressoPercentual) {
		synchronized (monitoracoes) {
			long tempoAtual = System.currentTimeMillis();
			ParametrosMonitoracao reg = getParametrosMonitoracao(chave);
			if (reg==null) {
				return new Dupla<>(-1L,-1L);
			}
			long tDec =  tempoAtual - reg.horaInicioMonitoracao; //getTempoDecorrido(chave);

			long estimativaTempoCorrente;
			if (progressoPercentual<2) {
				estimativaTempoCorrente = 0;
			} else {
				estimativaTempoCorrente = (long) (100f*tDec / progressoPercentual);
			}
			if (progressoPercentual<97 && estimativaTempoCorrente>0) {
				long ultMedia = Utils.mediaAritmetica(reg.ultimasEstimativasTempoTotal);
				if (reg.ultimasEstimativasTempoTotal.size()<=0
						|| tempoAtual > (reg.horaUltimaMedicaoEstimativaTempoTotal + 3000)
						) { //a cada 2s, coloca nova estimativa na media
					reg.ultimasEstimativasTempoTotal.add(estimativaTempoCorrente);
					reg.horaUltimaMedicaoEstimativaTempoTotal = tempoAtual;
				}
				return new Dupla<>(tDec,
						(long) (ultMedia * 0.7 + estimativaTempoCorrente * 0.3) //considera a ultima media e mais a estimativa atual.
						);
			} else {
				return new Dupla<>(tDec,estimativaTempoCorrente );
			}
		}
	}
	
	/**
	 * @param chave
	 * @param progressoPercentualAtual
	 * @param formatoTempo ex: mm:ss:SS   vide help mascaras para SimpleDateFormat.
	 * @return Dupla.o1 = mm:ss:SS tempo decorrido;
	 *         Dupla.o2 = mm:ss:SS tempo total estimado;
	 * 
	 */
	public static Dupla<String,String> getTempoFormatadoEstimativa(String chave, double progressoPercentualAtual, String formatoTempo) {
		Dupla<Long, Long> estimativaTempo = ObjetoMonitoracaoLight.getEstimativaTempoTotal(chave, progressoPercentualAtual);
		if (estimativaTempo.o1==-1 && estimativaTempo.o2==-1) {
			return new Dupla<>("","");
		}
		Date tDecorrido = new Date(); /*tDecorrido.setTime(0);*/ tDecorrido.setTime(estimativaTempo.o1);
		Date tTotal = new Date(); /*tTotal.setTime(0);*/ tTotal.setTime(estimativaTempo.o2);
		SimpleDateFormat df = new SimpleDateFormat(formatoTempo);
		df.setTimeZone(TimeZone.getTimeZone("GMT")); //se nao setarmos GMT0 aki, a formatacao da hora fica errada (pega o fuso do brasil). Ex: 1h12min fica 20h12min (pq fica gmt-3)...
		
		String tempoFinalFmt;
		if (estimativaTempo.o2<=800 || estimativaTempo.o2>(100*60*60*1000)) {
			tempoFinalFmt = "?";
		} else {
			tempoFinalFmt = df.format(tTotal);
		}
		return new Dupla<>( df.format(tDecorrido), tempoFinalFmt);
	}
	
	public static long getTempoAtual(String chave) {
		return getParametrosMonitoracao(chave).getTempoAteAgora();
	}
	
	private static ParametrosMonitoracao getParametrosMonitoracao(String chave) {
		return monitoracoes.get(chave);
	}

	private static Map<String, ParametrosMonitoracao> monitoracoes = new HashMap<>();
	
	/**
	 * Inicia a monitoção de tempo nesse trecho de código
	 * @param chave
	 */
	public static void iniciarContagem(String chave) {
	    synchronized (monitoracoes) {
	    	monitoracoes.put(chave, new ParametrosMonitoracao(chave));
	    }
	}

	/**
	 * Finaliza a monitoção de tempo nesse trecho de código
	 * @param chave
	 */
	public static void finalizarContagem(String chave) {
		finalizarContagem(chave, false);
	}
	

	/**
	 * Finaliza a monitoção de tempo nesse trecho de código
	 * @param chave
	 */
	public static void finalizarContagem(String chave, boolean apenasFinalizarSemLogar)	{
	    synchronized (monitoracoes) {
	    	ParametrosMonitoracao param = monitoracoes.remove(chave);
	    	if (param!=null) {
                if (!apenasFinalizarSemLogar) {
                    System.out.println("Monitoracao ["+chave+"] -> "+(param.getTempoAteAgora()/1000)+" s");
                }
	    	}
	    }
	}	

}
