/**
 *
 */
package iglezias.util;

import java.util.Arrays;
import java.util.List;

/**
 * Classe �til quando existem listas de um elemento associado a outro (uma Dupla).
 * Assim, normalmente esta classe vir� utilizada da seguinte forma:
 * (no exemplo, uma lista de Fields, cada um associado com a classe onde houve sua declara��o)
 *  <code>
 *     List<Dupla<Field,Class>>lista=new ArrayList<Dupla<Field,Class>>();
 *  </code>
 * @see ListaDupla
 * @author 91913837068
 */
public class Dupla<T1,T2> {
    public T1 o1;
    public T2 o2;
    public Dupla() {
        this.o1 = null;
        this.o2 = null;
    }
    public Dupla(T1 o1, T2 o2) {
        this.o1 = o1;
        this.o2 = o2;
    }
    @Override
    public String toString() {
        return Arrays.toString(new String[]{toStringObj(o1),toStringObj(o2)});
    }
	private String toStringObj(Object o) {
		if (o==null) {
			return null;
		} else if ( o instanceof Object[]) {
			return Arrays.toString( (Object[])o );
		} else if (o instanceof List) {
			return Arrays.toString( ((List<?>)o).toArray() );
		} else {
			return o.toString();
		}
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((o1 == null) ? 0 : o1.hashCode());
		result = prime * result + ((o2 == null) ? 0 : o2.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Dupla other = (Dupla) obj;
		if (o1 == null) {
			if (other.o1 != null) {
				return false;
			}
		} else if (!o1.equals(other.o1)) {
			return false;
		}
		if (o2 == null) {
			if (other.o2 != null) {
				return false;
			}
		} else if (!o2.equals(other.o2)) {
			return false;
		}
		return true;
	}

	public static void main(String[] args) {
		Dupla<Object[], Object[]> x = new Dupla<>(new Object[]{3L}, new Object[]{4L});
		System.out.println(x);
	}
}
