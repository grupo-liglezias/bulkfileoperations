/**
 *
 */
package iglezias.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.swing.JFileChooser;
import javax.swing.plaf.basic.BasicFileChooserUI;

import com.adobe.internal.xmp.XMPException;
import com.adobe.internal.xmp.XMPIterator;
import com.adobe.internal.xmp.XMPMeta;
import com.adobe.internal.xmp.properties.XMPPropertyInfo;
import com.drew.imaging.ImageProcessingException;
import com.drew.imaging.mp4.Mp4Reader;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;
import com.drew.metadata.file.FileSystemMetadataReader;
import com.drew.metadata.mp4.Mp4BoxHandler;
import com.drew.metadata.mp4.Mp4Directory;
import com.drew.metadata.xmp.XmpDirectory;


/**
 * @author iglezias
 * Modificado por Lucas Alberto em 27/03/2007 - fiz merge com o antigo c�digo do ppgd.
 */
public class UtilitariosArquivo {
	public static final String SEPARADOR_DIRETORIO = File.separator;
//	public static final String CODIFICACAO_ARQUIVOS_TEXTO = System.getProperty("file.encoding");
//	private static final String TITULO_MSG_ERRO_EDITOR_ODT_NAO_CONFIGURADO = "Edi��o de texto ODT";
//	private static final String MENSAGEM_EDITOR_ODT_NAO_CONFIGURADO = "N�o foi poss�vel encontrar um editor para o formato padr�o ODT.";

	static {
		getPastaDeArquivosTemporariosSistemaOperacional(); //certifica de guardar em cache a pasta de arquivos temporarios do SO, pois quem usa esta pasta TMP do jserpro pode sobrescrever o system.properties.
	}

    public static final String getExtensaoSemPonto(File f) {
        return getExtensaoSemPonto(f.getName());
    }
    public static final String getExtensaoSemPonto(String filename) {
        int whereDot = filename.lastIndexOf( '.' );
        if (whereDot>=0  &&  filename.lastIndexOf(File.separator)>whereDot) {
			whereDot=filename.length(); //evita problemas com arquivos sem extensao do tipo: C:\Inetpub.yyy\wwwrootOrig
		}
        if (whereDot>0  &&  whereDot<=(filename.length()-2) ) {
			return filename.substring(whereDot+1);
		}
		return "";
    }
    public static final File trocaExtensao(File f, String novaExt) {
        novaExt = ( (novaExt!=null && !"".equals(novaExt.trim())) ? "."+novaExt  : "" );
        return new File(extraiNomeAquivoSemExtensao(f.getAbsolutePath()) + novaExt );
    }

//    public static String getJarContainingFolder(Class aclass) throws Exception { //nao funciona quando rodando via JavaWebStart (jnlp)
//    	CodeSource codeSource = aclass.getProtectionDomain().getCodeSource();
//
//    	File jarFile;
//
//    	if (codeSource.getLocation() != null) {
//    		jarFile = new File(codeSource.getLocation().toURI());
//    	}
//    	else {
//    		String path = aclass.getResource(aclass.getSimpleName() + ".class").getPath();
//    		String jarFilePath = path.substring(path.indexOf(":") + 1, path.indexOf("!"));
//    		jarFilePath = URLDecoder.decode(jarFilePath, "UTF-8");
//    		jarFile = new File(jarFilePath);
//    	}
//    	return jarFile.getParentFile().getAbsolutePath();
//    }

//    public String getPath() { //nao funciona quando rodando via JavaWebStart (jnlp)
//    	String path = JSerproApp.class.getProtectionDomain().getCodeSource().getLocation().getPath();
//    	String decodedPath = path;
//    	try {
//    		decodedPath = URLDecoder.decode(path, "UTF-8");
//    	} catch (UnsupportedEncodingException e) {
//    		e.printStackTrace();
//    		return null;
//    	}
//
//    	String absolutePath = decodedPath.substring(0, decodedPath.lastIndexOf("/"))+"\\";
//    	return absolutePath;
//    }

    /**
     * @return true caso a aplicacao esteja rodando empacotada em um JAR. False caso contr�rio (indicando que roda a partir de um diret�rio /bin ou similar).
     * TODO verificar se nao eh equivalente a {@link ConstantesGlobais#getTipoExecucaoSistema()}
     */
    public static boolean rodandoAPartirDeJar() {
    	File arq = new File(System.getProperty("java.class.path")); //"esafira-fiscal-versao-0.0.1.exe";
    	return arq.isFile();
    }

    /**
     * @return O arquivo JAR da aplicacao rodando no momento, caso aplicacao empacotada.
     *         Caso contrario, retorna a pasta raiz do classpath (normalmente diretorio /bin do projeto)
     *         FIXME lucio: quando rodando via JavaWebStart, retorna o "deploy.jar".
     */
    public static File getArquivoJarOuPastaBin() {
//    	try {  //nao funciona quando rodando via JavaWebStart (jnlp)
//    		//http://stackoverflow.com/questions/320542/how-to-get-the-path-of-a-running-jar-file
//    		CodeSource codeSource = JSerproApp.class.getProtectionDomain().getCodeSource();
//    		File jarFile = new File(codeSource.getLocation().toURI().getPath());
//    		String jarDir = jarFile.getParentFile().getPath();
//    		JSLogger.loga(NivelLog.ALERTA, "jarDir = "+jarDir);
//
//    		String path = JSerproApp.class.getProtectionDomain().getCodeSource().getLocation().getPath();
//    		path = URLDecoder.decode(path, "UTF-8");
//    		JSLogger.loga(NivelLog.ALERTA, "path = "+path);
//
//    		//JSLogger.loga(NivelLog.ALERTA, "JAR corrente = "+getJarContainingFolder(JSerproApp.class));
//    	} catch (Exception e) {
//    		e.printStackTrace();
//    	}

    	File arq = new File(System.getProperty("java.class.path")); //"esafira-fiscal-versao-0.0.1.exe";
    	if (arq.isFile()) {
    		return new File(arq.getAbsolutePath()); //pode parecer estranho, mas fazer um new File() aqui faz sentido pois �s vezes um File.getparentFile() do Java retornava null sem esse mangue.
    	}
		String[] caminhosOrigem;
		caminhosOrigem = System.getProperty("java.class.path").split("["+File.pathSeparatorChar+"]");
		File binDeTeste = null;
		for (String caminho : caminhosOrigem) {
			arq = new File(caminho);
			if (!arq.isFile() && arq.isDirectory()) { //pasta BIN por exemplo...
				if (!arq.getName().toLowerCase().contains("test")) {
					return new File(arq.getAbsolutePath());
				}
				binDeTeste = new File(arq.getAbsolutePath());
				//pode ser BIN de teste... ignora e soh usa se nao achar outro...
			}
		}
		if (binDeTeste!=null) { //se nao existe outra pasta "bin", usa a que encontrou q contem "test" mesmo...
			return binDeTeste;
		}
    	return null;
    }

    /**
     * FROM serpro.ppgd.negocio.util.UtilitariosArquivo
     * A partir do string com o nome do arquivo, retorna um string contendo nome
     * do arquivo sem a extensao. Considera como separador de nome de arquivo a constante definida em
     * ConstantesGlobais.
     * obs. � apenas eliminada extensao. Se for passado o caminho completo, retornara o caminho completo
     * menos a extensao.
     */
    public static String extraiNomeAquivoSemExtensao( String nomeArquivo ){
        int idxFim=nomeArquivo.lastIndexOf( "." );
        if (idxFim>=0  &&  nomeArquivo.lastIndexOf(File.separator)>idxFim) {
			idxFim=nomeArquivo.length(); //evita problemas com arquivos sem extensao do tipo: C:\Inetpub.yyy\wwwrootOrig
		}
        return nomeArquivo.substring( 0, (idxFim>=0) ? idxFim : nomeArquivo.length());
    }

//    /**
//     * Le o arquivo especificado e coloca o seu conteudo como resultado. Codifica��o suportada: ISO-8859-1.
//     * @param nomeArquivo
//     * @param converterNovaLinha Caracteres de nova linha s�o convertidos de \n\r (#13#10) para \n caso true
//     * @return
//     * @throws IOException
//     */
//    public static String leArquivoTexto(String nomeArquivo, boolean converterNovaLinha) throws IOException {
//        return leArquivoTexto(new File(nomeArquivo), converterNovaLinha);
//    }
//
//    /**
//     * Le o arquivo especificado e coloca o seu conteudo como resultado. Codifica��o suportada: ISO-8859-1.
//     * @param nomeArquivo
//     * @param converterNovaLinha Caracteres de nova linha s�o convertidos de \n\r (#13#10) para \n caso true
//     * @return
//     * @throws IOException
//     */
//    @SuppressWarnings("deprecation")
//    public static String leArquivoTexto(File arquivo, boolean converterNovaLinha) throws IOException {
//        String result;
//        File f;
//        FileInputStream is;
//		FileChannel fc = (is = new FileInputStream(f=arquivo)).getChannel();
//        try {
//            fc.position(0);
//            if (f.length() > Integer.MAX_VALUE) {
//				throw new RuntimeException("Arquivo muito grande para ser lido por este m�todo: String leArquivoTexto(String nomeArquivo)");
//			}
//
//            ByteBuffer buf = ByteBuffer.allocate((int)f.length());
//            fc.read(buf);
//            buf.position(0);
//            result=new String(buf.array(), 0); //Equivalente a usar a codificacao "ISO-8859-1", mas BEM mais r�pido...
//            if (converterNovaLinha) {
//				result=result.replaceAll("\r\n","\n");
//			}
//            return result;
//        } finally {
//            fc.close();
//            is.close();
//        }
//    }
//
//
//    /**
//     * Grava o string contido no par�metro "texto" no arquivo especificado.
//     * Caracteres de nova linha s�o \n.
//     * @param nomeArquivo
//     * @param texto
//     * @throws IOException
//     */
//    public static void gravaArquivoTexto(String nomeArquivo, String texto) throws IOException {
//    	gravaArquivoTexto(nomeArquivo, texto, false, CODIFICACAO_ARQUIVOS_TEXTO);
//    }
//
//    /**
//     * Grava o string contido no par�metro "texto" no arquivo especificado.
//     * Caracteres de nova linha s�o \n.
//     * @param nomeArquivo
//     * @param texto
//     * @param codificacao
//     * @throws IOException
//     */
//    public static void gravaArquivoTexto(String nomeArquivo, String texto, String codificacao) throws IOException {
//    	gravaArquivoTexto(nomeArquivo, texto, false, codificacao);
//    }
//
//    /**
//     * Cria o arquivo informado.
//     * Se os diretorios do caminho do arquivo n�o existirem, ent�o ser�o criados.
//     * Caminhos absolutos no linux n�o ser�o criados por motivos de seguran�a.
//     * Tenha aten��o especial na utiliza��o desta funcionalidade!
//     * @param caminhoArquivo
//     * @param texto
//     * @return
//     * @throws IOException
//     */
//    public static boolean gravaArquivoTextoCriaDiretorios(String caminhoArquivo, String texto) throws IOException
//    {
//    	if (caminhoArquivo.startsWith(File.separator))
//    	{
//            	JSLogger.loga(NivelLog.ERRO, "Cannot create absolute paths");
//            	return false;
//        }
//
//    	caminhoArquivo = new File(caminhoArquivo).getAbsolutePath();
//
//    	SortedSet<String> dirsMade = new TreeSet<>();
//        int ix = caminhoArquivo.lastIndexOf(File.separator);
//        /* se possui diretorios para criar */
//        if (ix > 0)
//        {
//	          String dirName = caminhoArquivo.substring(0, ix);
//	          if (!dirsMade.contains(dirName))
//	          {
//		            File d = new File(dirName);
//		            /* cria o diretorio se necessario */
//		            if (!(d.exists() && d.isDirectory()))
//		            {
//			            /* cria o diretorio e mostra possiveis erros */
//			            JSLogger.loga(NivelLog.INFORMACAO, "Creating Directory: " + d.getCanonicalPath());
//			            if (!d.mkdirs()) {
//			              	JSLogger.loga(NivelLog.ERRO, "Warning: unable to mkdir "+ d.getCanonicalPath());
//			            }
//			            /* diretorio jah criado */
//			            dirsMade.add(dirName);
//		            }
//	          }
//        }
//
//        /* grava o arquivo nos diretorios criados */
//        gravaArquivoTexto(caminhoArquivo, texto);
//
//        return true;
//    }
//
//    /**
//     * Cria todo o caminho do diretorio informado.
//     * Se os diretorios do caminho n�o existirem, ent�o ser�o criados.
//     * Tenha aten��o especial na utiliza��o desta funcionalidade!
//     * @param caminhoDiretorio
//     * @return
//     * @throws IOException
//     */
//    public static boolean criaDiretorios(File caminhoDiretorio) throws IOException
//    {
//        /* cria o diretorio se necessario */
//        if (!(caminhoDiretorio.exists() && caminhoDiretorio.isDirectory()))
//        {
//            /* cria o diretorio e mostra possiveis erros */
//            JSLogger.loga(NivelLog.INFORMACAO, "Creating Directory: " + caminhoDiretorio.getCanonicalPath());
//            if (!caminhoDiretorio.mkdirs()) {
//              	JSLogger.loga(NivelLog.ERRO, "Warning: unable to mkdir "+ caminhoDiretorio.getCanonicalPath());
//              	return false;
//            }
//        }
//        return true;
//    }
//
//    /**
//     * Cria um diret�rio tempor�rio e retorna sua refer�ncia.
//     * Copia de {@link com.google.common.io.Files#createTempDir()} , mas usando o diretorio temporario padrao da aplicacao (que eh faxinado)...
//     * vide {@link #getPastaDeArquivosTemporariosJSerpro()}
//     */
//    public static File criaDiretorioTemporario() {
////    	int TEMP_DIR_ATTEMPTS = 10000;
////    	File baseDir = new File(System.getProperty("java.io.tmpdir"));
////        String baseName = System.currentTimeMillis() + "-";
////
////        for (int counter = 0; counter < TEMP_DIR_ATTEMPTS; counter++) {
////          File tempDir = new File(baseDir, baseName + counter);
////          if (tempDir.mkdir()) {
////            return tempDir;
////          }
////        }
////        throw new IllegalStateException("Failed to create directory within "
////            + TEMP_DIR_ATTEMPTS + " attempts (tried "
////            + baseName + "0 to " + baseName + (TEMP_DIR_ATTEMPTS - 1) + ')');
//
//    	//ajusta java.io.tmpdir e, dai, podemos usar o createTempDir do guava...
//    	System.setProperty("java.io.tmpdir",getPastaDeArquivosTemporariosJSerpro().getAbsolutePath());
//
//    	return Files.createTempDir();
//    }
//
//    /**
//     * Grava o string contido no par�metro "texto" no arquivo especificado.
//     * Use o outro m�todo "mais simples" se n�o precisa decidir sobre caracteres de nova linha.
//     * @param caminhoArquivo
//     * @param texto
//     * @param converterNovaLinha Caracteres de nova linha s�o convertidos de \n para \n\r (#13#10) caso true
//     * @throws IOException
//     */
//    public static void gravaArquivoTexto(String caminhoArquivo, String texto, boolean converterNovaLinha, String codificacao) throws IOException {
//
//    	caminhoArquivo = new File(caminhoArquivo).getAbsolutePath();
//
//        if (converterNovaLinha) {
//			texto=texto.replaceAll("\n", "\r\n");
//		}
//        File f=new File(caminhoArquivo);
//        FileOutputStream is;
//		FileChannel fc = (is = new FileOutputStream(f)).getChannel();
//        //new BufferedWriter(new OutputStreamWriter(new FileOutputStream("outfilename"), "UTF8")).;
//        try {
//            fc.position(0);
//            byte[] bytesTexto = texto.getBytes(codificacao);
//            if (bytesTexto.length > Integer.MAX_VALUE) {
//				throw new RuntimeException("String muito grande para ser gravada por este m�todo: void gravaArquivoTexto(String nomeArquivo, String texto)");
//			}
//
//            ByteBuffer buf = ByteBuffer.allocate(bytesTexto.length);
//            /* cria uma arquivo com a codifica��o informada */
//            buf.put(bytesTexto);
//            buf.position(0);
//            fc.write(buf);
//            buf=null;
//        } finally {
//            fc.close();
//            is.close();
//        }
//
//        JSLogger.loga(NivelLog.INFORMACAO, "Criado arquivo: " + f.getCanonicalPath());
//    }
//
//    /**
//     * Grava arquivo no diret�rio indicado
//     * @param diretorio
//     * @param nomeArquivo
//     * @param conteudoArquivo
//     * @throws IOException
//     */
//    public static void gravaArquivo(String diretorio, String nomeArquivo, InputStream conteudoArquivo) throws IOException {
//    	FileOutputStream fos = new FileOutputStream(diretorio + File.separator + nomeArquivo);
//    	OutputStream out = new BufferedOutputStream(fos);
//    	try {
//    		new BypassDecodeCodec(conteudoArquivo).decodeTo(out);
//    	} finally {
//    		out.flush(); out.close(); fos.flush(); fos.close();
//    	}
//    }
//
//    /** A partir do string com o nome do arquivo, retorna um string contendo  o caminho
//     * do arquivo. Considera como separador de nome de arquivo a constante definida em
//     * ConstantesGlobais.
//     */
//    public static String extraiPath( String nomeArquivo ) {
//        // trata paths passados de url
//        if (nomeArquivo.startsWith("file:")) {
//			nomeArquivo = nomeArquivo.substring(5);
//		}
//
//        // trata paths passados do jar
//        int posjar = nomeArquivo.indexOf('!');
//        if (posjar != -1) {
//			nomeArquivo = nomeArquivo.substring(0, posjar);
//		}
//
//        return nomeArquivo.substring( 0, nomeArquivo.lastIndexOf(
//                new File( nomeArquivo ).getName() ));
//    }
//
//    /**
//     * Retorna o InputStream (j� bufferizado!) de um arquivo que est� no classpath.
//     * Pode localizar arquivos dentro de Jars.
//     * Retorna null caso n�o econtre o resource especificado no par�metro "path"
//     */
//    public static InputStream getResource(String path, ClassLoader classLoader) {
//        InputStream inRes=classLoader.getResourceAsStream(path);
//        if (inRes==null) {
//            return null;
//        } else {
//            InputStream in = new BufferedInputStream(inRes);
//            return in;
//        }
//    }
//
//    /**
//     * Retorna o InputStream (j� bufferizado!) de um arquivo que est� no classpath.
//     * Pode localizar arquivos dentro de Jars.
//     * Retorna null caso n�o econtre o resource especificado no par�metro "path"
//     */
//    public static InputStream getResource(String path) {
//        return getResource(path, UtilitariosReflexao.getClassLoader());
//    }
//
//    public static boolean resourceExistente(String path) {
//        return UtilitariosReflexao.getClassLoader().getResource(path)!=null;
//    }
//
////    /**
////     * Copia um arquivo de um destino para outro.
////     * Localiza o arquivo em qualquer local do classpath, inclusive dentro de Jars.
////     * @param filename
////     * IMPORTANTE!: filename deve ser caminho como o do linux. ex: pastax/pastay/arquivoxpto.pdf
////     *              Logo, NAO use File.Separator para separar pastas e arquivos.
////     * @param toPath
////     * @return
////     * @deprecated utilizar {@link #copiaArquivoDoJarParaPasta(String, File)}
////     */
////    @Deprecated
////    public static boolean copiaArquivoDoJar(String filename, String toPath)
////    {
////    	return copiaArquivoDoJar(filename, new File(toPath, filename));
////    }
//
//    /**
//     * Copia um arquivo do JAR para a PASTA especificada.
//     * O arquivo � localizado em qualquer local do classpath, inclusive dentro de Jars.
//     * @param filename
//     * IMPORTANTE!: filename deve ser caminho como o do linux. ex: pastax/pastay/arquivoxpto.pdf
//     *              Logo, NAO use File.Separator para separar pastas e arquivos.
//     * @param pastaDestino a pasta destino onde serah colocado o arquivo do jar
//     *        OBS: se filename = 'patch_atalho/xpto.ico' e pastaDestino = 'c:\teste\' entao o
//     *        arquivo serah colocado em: 'c:\teste\patch_atalho\xpto.ico'
//     * @return true se copiou ok. false se nao conseguiu copiar (ocorreu algum IOException)
//     */
//    public static boolean copiaArquivoDoJarParaPasta(String filename, File pastaDestino) {
//    	try {
//			return copiaArquivoDoJar(filename, new File(pastaDestino, filename).getCanonicalFile());
//		} catch (IOException e) {
//			e.printStackTrace();
//            return false;
//		}
//    }
//
//    /**
//     * Copia um arquivo de um destino para outro.
//     * Localiza o arquivo em qualquer local do classpath, inclusive dentro de Jars.
//     * @param filename
//     * IMPORTANTE!: filename deve ser caminho como o do linux. ex: pastax/pastay/arquivoxpto.pdf
//     *              Logo, NAO use File.Separator para separar pastas e arquivos.
//     * @param arquivoDestino o arquivo destino (incluindo pasta) onde serah colocado o arquivo do jar.
//     * @return true se copiou ok. false se nao conseguiu copiar (ocorreu algum IOException)
//     */
//    public static boolean copiaArquivoDoJar(String filename, File arquivoDestino)
//    {
//        try {
//        	if (!arquivoDestino.getParentFile().exists()) {
//        		try {
//        			arquivoDestino.getParentFile().mkdirs();
//        		} catch (Throwable e) {
//        			new Exception("Erro ao criar pasta para extracao arquivo(s):["+arquivoDestino.getAbsolutePath()+"]",e).printStackTrace();
//        		}
//        	}
//            InputStream is = getResource(filename, UtilitariosReflexao.getClassLoader());
//            OutputStream o = new FileOutputStream(arquivoDestino);
//            new BypassEncodeCodec(o).encodeFrom(is);
//            o.flush();
//            o.close();
//            is.close();
//
//        } catch (FileNotFoundException fnf) {
//            fnf.printStackTrace();
//            return false;
//        } catch (IOException ioe) {
//            ioe.printStackTrace();
//            return false;
//        }
//
//        try {
//        	arquivoDestino.getCanonicalPath();
//		} catch (IOException e) {
//			e.printStackTrace();
//			return false;
//		}
//        //JSLogger.loga(NivelLog.INFORMACAO, "Copiado arquivo "+ filename + " do classpath para o diretorio: "+arquivoDestino);
//
//        return arquivoDestino.exists();
//    }
//
//    /**
//     * Copia um arquivo de um destino para outro.
//     * Localiza o arquivo em qualquer local do classpath, inclusive dentro de Jars.
//     * @param filename
//     * @param toPath
//     * @return
//     */
//    public static boolean copiaArquivoDoJarPathRelativo(String filename, String toPath)
//    {
//    	String[] split = filename.split("/");
//    	String nomeDoArquivo = split[split.length-1];
//    	String toFilePath = toPath+File.separator+nomeDoArquivo;
//    	File file = new File(toFilePath);
//
//        try {
//            InputStream is = getResource(filename, UtilitariosReflexao.getClassLoader());
//            OutputStream o = new FileOutputStream(file.getAbsolutePath());
//            new BypassEncodeCodec(o).encodeFrom(is);
//            o.flush();
//            o.close();
//            is.close();
//
//        } catch (FileNotFoundException fnf) {
//            fnf.printStackTrace();
//            return false;
//        } catch (IOException ioe) {
//            ioe.printStackTrace();
//            return false;
//        }
//
//        @SuppressWarnings("unused")
//        String caminho = "";
//        try {
//			caminho = file.getCanonicalPath();
//		} catch (IOException e) {
//			e.printStackTrace();
//			return false;
//		}
//        //JSLogger.loga(NivelLog.INFORMACAO, "Copiado arquivo "+ filename + " do classpath para o diretorio: "+caminho);
//
//        return file.exists();
//    }
//
//    /** A partir do nome completo do arquivo (path + nomeArquivo)
//     * Retorna um string contendo somente o nome do arquivo sem o path
//     */
//    public static String extraiNomeArquivo( String nomeArquivoComPath ){
//        return new File( nomeArquivoComPath ).getName();
//    }
//
//    /**
//     * Copia arquivo da origem para destino (FileChannel)
//     * @param origem dados de origem
//     * @param destino canal que receberah todos os dados da origem
//     * @throws IOException
//     */
//	public static void copiaArquivo(InputStream in, FileChannel fcout) throws IOException {
//		if (in instanceof FileInputStream) {
//			copiaArquivo(((FileInputStream) in).getChannel(), fcout);
//		} else if (in instanceof TempFileInputStream) {
//			File file = ((TempFileInputStream) in).getFile();
//			FileChannel fin;
//			copiaArquivo(fin = new FileInputStream(file).getChannel(), fcout);
//			fin.close();
//		} else {
//			byte[] buf=new byte[2*1024*1024]; //2mb
//			int lidos;
//			while ( (lidos=in.read(buf)) >= 0) {
//				ByteBuffer origem = ByteBuffer.wrap(buf, 0, lidos);
//				fcout.write(origem);
//			}
//		}
//	}
//
    /**
     * Copia arquivo da origem para destino (FileChannel)
     * @param origem dados de origem
     * @param destino canal que receberah todos os dados da origem
     * @throws IOException
     */
    public static void copiaArquivo(FileChannel origem, FileChannel destino) throws IOException {
        //try {
    		final long TAM_MAX_BUF = 5*1024*1024;
        	long transferidos=0;
        	long pos=0;
			do {
        		transferidos = origem.transferTo(pos,TAM_MAX_BUF, destino);
        		pos+=transferidos;
        	} while ( transferidos > 0);
			if (origem.size() != pos) {
				throw new IOException("erro na c�pia de arquivo. Tamanho que deveria ser copiado:["+origem.size()+"]. Tamanho efetivamente copiado:["+pos+"]");
			}

        //} finally {
        //    try {
        //        origem.close();
        //        destino.close();
        //    } catch (IOException e) {}
        //}
    }

    /**
     * Copia arquivo da origem para destino (podendo o destino ser um caminho absoluto ou uma pasta)
     * @param origem Caminho completo do arquivo Origem
     * @param destino Caminho completo do arquivo Destino (pode ser apenas uma pasta, vide parametro destinoEhPasta)
     * @param destinoEhPasta, se sim, destino representa uma PASTA, e o arquivo origem serah copiado com mesmo nome nesta pasta.
     *
     * Se destino j� existir, o mesmo serah SOBRESCRITO!
     * @return false caso algum PROBLEMA aconteceu com a c�pia (arquivo origem n�o encontrado, falta de espa�o em disco, etc);
     *         true caso o arquivo foi devidamente copiado.
     */
    public static boolean copiaArquivo(File origem, File destino, boolean destinoEhPasta) {
        try {
        	copiaArquivoLancandoExcecao(origem, destino, destinoEhPasta);
        	return true;
        } catch (Exception e) {
        	//JSLogger.loga(NivelLog.ALERTA, e.getLocalizedMessage());
            return false;
        }
    }

    /**
     * Copia arquivo da origem para destino (podendo o destino ser um caminho absoluto ou uma pasta)
     * @param origem Caminho completo do arquivo Origem
     * @param destino Caminho completo do arquivo Destino (pode ser apenas uma pasta, vide parametro destinoEhPasta)
     * @param destinoEhPasta, se sim, destino representa uma PASTA, e o arquivo origem serah copiado com mesmo nome nesta pasta.
     *
     * Se destino j� existir, o mesmo serah SOBRESCRITO!
     * @throws caso algum PROBLEMA aconteceu com a c�pia (arquivo origem n�o encontrado, falta de espa�o em disco, etc);
     */
    public static void copiaArquivoLancandoExcecao(File origem, File destino, boolean destinoEhPasta) throws FileNotFoundException, IOException {
        if (destinoEhPasta) {
            destino=new File(destino, origem.getName());
        }
        FileInputStream fIn = new FileInputStream(origem);
        FileChannel fcIn=fIn.getChannel();
        if (destino.exists()) {
        	destino.delete();
        } else {
        	destino.getParentFile().mkdirs();
        }

        FileOutputStream fos=new FileOutputStream(destino, false);
        FileChannel fcOut = fos.getChannel();

        try {
        	copiaArquivo(fcIn, fcOut);
        } finally {
        	try {
        		fcIn.close();
        		fIn.close();
        		fcOut.close();
        		fos.close();
        	} catch (IOException e) {}
        }
    }

//    /** Faz c�pia de arquivos para um diret�rio indicado.
//     * in - arquivo a ser copiado
//     * outPath = Path com a PASTA destino da c�pia
//     * @see #copiaArquivo(File, File, boolean)
//     */
//    public static boolean copiaArquivo(String in, String outPath) {
//        return copiaArquivo(new File(in), new File(outPath), true);
//    }
//
////    /** Faz c�pia de arquivos para um diret�rio indicado.
////     * in - arquivo a ser copiado
////     * outPath = Path com o destino da c�pia
////     */
////    public static boolean copiaArquivo(String in, String outPath){
////       FileWriter fw = null;
////       FileReader fr = null;
////       BufferedReader br = null;
////       BufferedWriter bw = null;
////       File source = null;
////
////       in = new File(in).getAbsolutePath();
////       outPath = new File(outPath).getAbsolutePath();
////
////       try {
////           /* Determina o tamanho do buffer para alocar  */
////           source = new File(in);
////           int fileLength = (int) source.length();
////           char charBuff[] = new char[fileLength];
////
////           File flSaida = new File(outPath);
////           //Cria diret�rio lib, se n�o existir
////           if (!flSaida.exists())
////               flSaida.mkdirs();
////
////           outPath = outPath + "/" + source.getName();
////
////           fr = new FileReader(in);
////           fw = new FileWriter(outPath);
////           br = new BufferedReader(fr);
////           bw = new BufferedWriter(fw);
////
////
////           while (br.read(charBuff,0,fileLength) != -1)
////                bw.write(charBuff,0,fileLength);
////       }
////       catch(FileNotFoundException fnfe){
////            fnfe.printStackTrace();
////            return false;
////       }
////       catch(IOException ioe) {
////    	   ioe.printStackTrace();
////           return false;
////       }
////       finally {
////            try {
////                if (br != null)
////                    br.close();
////                if (bw != null)
////                    bw.close();
////            }
////            catch(IOException ioe) {
////            	return false;
////            }
////       }
////
////       return true;
////    }
//
//    public static boolean ehDisquete(File pFile){
//  	  	return (pFile.getPath().toUpperCase().indexOf("A:") != -1);
//    }
//
//    /**
//     * Tenta apagar o <tt>arquivo</tt> informado. S� realiza a opera��o se o arquivo existir, se n�o for um diret�rio e se n�o for somente leitura.
//     * @param arquivo
//     * @return true se conseguiu apagar o arquivo, false caso contr�rio.
//     */
//    public static boolean apagaArquivo(File arquivo) {
//    	return deleteArquivo(arquivo);
//    }

    /**
     * Faz o delete do "arquivo". Equivalente a arquivo.delete();
	 * Tenta apagar o <tt>arquivo</tt> informado. S� realiza a opera��o se o arquivo existir, se n�o for um diret�rio e se n�o for somente leitura.
     * Por�m, se n�o conseguiu deletar, tenta por ateh 2s fazer outras tentativas (pode ser que o arquivo desbloqueie para delete apos uma destas tentativas)
     * @param arquivo
     * @return true caso o delete tenha sucesso (foi efetivado),
     *         false se nao conseguiu fazer o delete.
     */
    public static boolean deleteArquivo(File arquivo) {
    	if (arquivo == null) {
			return false;
		}

   		return deleteArquivoComRetry(arquivo, 2000);
    }
    
    /**
     * Faz o delete do "arquivo". Equivalente a arquivo.delete();
	 * Tenta apagar o <tt>arquivo</tt> informado. S� realiza a opera��o se o arquivo existir, se n�o for um diret�rio e se n�o for somente leitura.
     * Por�m, se n�o conseguiu deletar, tenta por ateh 2s fazer outras tentativas (pode ser que o arquivo desbloqueie para delete apos uma destas tentativas)
     * @param qtdMsParaDarRetry tempo maximo (em ms) para ficar tentando renomear ateh conseguir.
     * @param arquivo
     * @return true caso o delete tenha sucesso (foi efetivado),
     *         false se nao conseguiu fazer o delete.
     */
    public static boolean deleteArquivoComRetry(final File arquivo, int qtdMsParaDarRetry) {
    	try {
			Boolean res = UtilitariosThread.runComRetry(
				new UtilitariosThread.RunnableComRetornoETeste<Boolean>() {
					@Override
					public Boolean runComRetorno() throws Exception {
						return arquivo.exists() && !arquivo.isDirectory() && arquivo.canWrite() && arquivo.delete();
					}
					@Override
					public boolean testeOk(Boolean retorno, Exception excecaoRetorno) {
						return retorno!=null && retorno.booleanValue();
					}
				}
				, qtdMsParaDarRetry);
			return (res==null) ? false : res;
		} catch (Exception e) {
//			JSLogger.loga(NivelLog.ERRO, e);
			return false;
		}
//    	long msAntes = System.currentTimeMillis();
//		do {
//			if (arquivo.exists() && !arquivo.isDirectory()
//					&& arquivo.canWrite() && arquivo.delete()
//			) {
//				return true;
//			} else {
//				try {
//					Thread.sleep(100);
//				} catch (InterruptedException e) {
//					break;
//				}
//			}
//		} while ( (System.currentTimeMillis()-msAntes) < qtdMsParaDarRetry);
//		return false;
	}

    /**
     * Apaga arquivos e subpastas da pasta pFile
     * @param pasta
     * @param apagarPastaPrincipal se true, apaga tambem a pasta "pasta". Se false, deixa esta pasta, que estar� vazia.
     * @throws IOException caso algum arquivo n�o p�de ser exclu�do.
     */
    public static void apagaDiretorio(File pasta, boolean apagarPastaPrincipal) throws IOException{
    	File[] arquivos = pasta.listFiles();
    	if (arquivos==null) { //pasta inexistente...
    		return;
    	}
    	for (File element : arquivos) {
    		if(element.isDirectory()){
    			apagaDiretorio(element, true);
    		} else if (!deleteArquivo( element ) ) {
				throw new IOException("N�o foi poss�vel excluir arquivo ["+element.getAbsolutePath()+"]");
			}
    	}

    	if (apagarPastaPrincipal) {
    		if (!pasta.delete()) {
    			throw new IOException("N�o foi poss�vel excluir arquivo ["+pasta.getAbsolutePath()+"]");
    		}
    	}
    }

    /**
     * Apaga arquivos e subpastas da pasta pFile
     * @param pasta
     * @param apagarPastaPrincipal se true, apaga tambem a pasta "pasta". Se false, deixa esta pasta, que estar� vazia.
     * @param arquivosNaoExcluidos se algum arquivo n�o puder ser exclu�do, ele ser� acrescido nessa lista (caso seja <>null)
	 * @return a lista dos arquivos/pastas removidos.
     */
    public static List<File> apagaDiretorio(File pasta, boolean apagarPastaPrincipal, List<File> arquivosNaoExcluidos) {
    	List<File> arqsApagados= new ArrayList<>();

    	File[] arquivos = pasta.listFiles();
    	if (arquivos==null) { //pasta inexistente...
    		return arqsApagados;
    	}
    	List<File> arquivosErros = new ArrayList<>();
    	for (File element : arquivos) {
    		if(element.isDirectory()){
   				arqsApagados.addAll( apagaDiretorio(element, true, arquivosErros) );
    		} else if (deleteArquivo( element ) ) {
				arqsApagados.add(element);
			} else {
				element.deleteOnExit();
				arquivosErros.add(element);
			}
    	}

    	arquivosNaoExcluidos.addAll(arquivosErros);

    	if (apagarPastaPrincipal) {
    		if (!pasta.delete()) {
    			pasta.deleteOnExit();
    		}
    	}

    	return arqsApagados;
    }

//    /**
//     * retorna True (no {@link #accept(File)}) caso a extencao do arquivo esteja dentre as extensoes passadas
//     * @author lucio
//     */
//    public static class FileFilterInExtensoes implements java.io.FileFilter {
//    	private Set<String> extensoesArquivoParaRetornarTrue = new HashSet<>();
//
//		public FileFilterInExtensoes(Collection<String> extensoesArquivoParaRetornarTrue) {
//			for (String ex : extensoesArquivoParaRetornarTrue) {
//				if (ex.startsWith(".")) {
//					ex = ex.substring(1); //remove ponto inicial
//				}
//				this.extensoesArquivoParaRetornarTrue.add(ex.toUpperCase());
//			}
//		}
//
//		@Override
//    	public boolean accept(File f) { //retorna TRUE se o arquivo contem alguma das extensoes passadas
//    		return extensoesArquivoParaRetornarTrue.contains( UtilitariosArquivo.getExtensaoSemPonto(f).toUpperCase() );
//    	}
//
//		public Set<String> getExtensoesArquivoParaRetornarTrue() {
//			return extensoesArquivoParaRetornarTrue;
//		}
//    }
//
//    /**
//     * retorna True (no {@link #accept(File)}) caso a extencao do arquivo NAO esteja dentre as extensoes passadas
//     * ou seja, retorna o NOT do {@link FileFilterInExtensoes}.
//     * @author lucio
//     */
//    public static class FileFilterNotInExtensoes extends FileFilterInExtensoes {
//		public FileFilterNotInExtensoes(Collection<String> extensoesArquivoParaNaoApagar) {
//			super(extensoesArquivoParaNaoApagar);
//		}
//
//		@Override
//    	public boolean accept(File f) { //retorna TRUE dizendo que o arquivo deve ser apagado
//    		return !super.accept(f);
//    	}
//    }
//
//    /**
//     * Apaga arquivos e subpastas da pasta passada
//     * @param pasta
//     * @param apagarPastaPrincipal se true, apaga tambem a pasta "pasta". Se false, deixa esta pasta, que estar� vazia.
//     * @param extensoesArquivoParaNaoApagar. Se algum arquivo (da pasta ou subpastas) tiver alguma dessas extensoes, esse arquivo n�o ser� exclu�do.
//     *        OBS: assume-se que essas extensoes contem apenas caracteres MAIUSCULOS.
//     * @param arquivosNaoExcluidos se algum arquivo n�o puder ser exclu�do, ele ser� acrescido nessa lista (caso seja <>null)
//     * @return true caso conseguiu apagar o diret�rio (pois todos seus arquivos e pastas puderam ser excluidos.e
//     *         false caso contr�rio.
//     * OBS: nao lanca excecao.
//     */
//    public static boolean apagaDiretorio(File pasta, boolean apagarPastaPrincipal, Collection<String> extensoesArquivoParaNaoApagar, List<File> arquivosNaoExcluidos) {
//    	FileFilterNotInExtensoes filtroExtensoes = new FileFilterNotInExtensoes(extensoesArquivoParaNaoApagar);
//    	return apagaDiretorio(pasta, apagarPastaPrincipal, filtroExtensoes, arquivosNaoExcluidos);
//    }
//
//    /**
//     * Apaga arquivos e subpastas da pasta passada
//     * @param pasta
//     * @param apagarPastaPrincipal se true, apaga tambem a pasta "pasta". Se false, deixa esta pasta, que estar� vazia.
//     * @param extensoesArquivoParaNaoApagar. Se algum arquivo (da pasta ou subpastas) tiver alguma dessas extensoes, esse arquivo n�o ser� exclu�do.
//     *        OBS: assume-se que essas extensoes contem apenas caracteres MAIUSCULOS, e n�o cont�m "."
//     * @param arquivosNaoExcluidos se algum arquivo n�o puder ser exclu�do, ele ser� acrescido nessa lista (caso seja <>null)
//     * @return true caso conseguiu apagar o diret�rio (pois todos seus arquivos e pastas puderam ser excluidos.e
//     *         false caso contr�rio.
//     * OBS: nao lanca excecao.
//     */
//    public static boolean apagaDiretorio(File pasta, boolean apagarPastaPrincipal, java.io.FileFilter consultaDeveApagar, List<File> arquivosNaoExcluidos) {
//    	boolean result = true;
//    	if (!pasta.exists() || pasta.isFile()) {
//    		return false;
//    	}
//    	File[] arquivos = pasta.listFiles();
//    	for (File element : arquivos) {
//    		if(element.isDirectory()){
//    			result &= apagaDiretorio(element, true, consultaDeveApagar, arquivosNaoExcluidos);
//    		} else {
//    			if (consultaDeveApagar.accept( element )
//    					&& !element.delete()
//    			) {
//    				element.deleteOnExit();
//    				if (arquivosNaoExcluidos!=null) {
//    					arquivosNaoExcluidos.add(element);
//    				}
//    				result = false;
//    			}
//    		}
//    	}
//
//    	if (apagarPastaPrincipal) {
//    		if (result) {
//    			result = pasta.delete();
//    		}
//    		if (!result) {
//    			pasta.deleteOnExit();
//    		}
//    	}
//    	return result;
//    }
//
//    public static File getDisquete(){
//	  	  File[] roots = File.listRoots();
//
//	  	  for (File root : roots) {
//	  	  	 if(ehDisquete(root)){
//	  	  	 	return  root;
//	  	  	 }
//	  	  }
//	  	  return null;
//    }
//
//    public static boolean apagaDisquete(){
//    	  try{
//    	  	  File[] arquivosDisquete = getDisquete().listFiles();
//    	  	  for (File element : arquivosDisquete) {
//    	  	      if(element.isDirectory()){
//    	  	      	apagaDiretorio(element, true);
//    	  	      }else{
//    	  	        element.delete();
//    	  	      }
//    	  	  }
//
//    	  }catch(Exception e){
//    		  e.printStackTrace();
//    		  return false;
//    	  }
//
//    	  return true;
//    }
//
//    /**
//     * Carrega uma URL que aponta para o arquivo informado.
//     * @param aRecurso
//     * @return
//     */
//    public static URL localizaArquivoEmClasspath(String aRecurso) {
//
//    	URL url = null;
//        /* primeiramente, tenta com a classe principal da aplica��o */
//    	if(JSerproApp.getInstancia() != null) {
//			url = JSerproApp.getInstancia().getClass().getResource(aRecurso);
//		}
//
//        /* carrega usando o classloader que carregou esta classe */
//        if (url==null) {
//            url =  UtilitariosArquivo.class.getResource(aRecurso);
//            //JSLogger.loga(NivelLog.INFORMACAO, "Encontrou classes atraves de: UtilitariosArquivo.class.getResource(aRecurso)");
//        }
//
//        /* se n�o encontrou o arquivo ent�o procura usando o classloader da thread corrente */
//        if (url==null) {
//            url =  UtilitariosReflexao.getClassLoader().getResource(aRecurso);
//            //JSLogger.loga(NivelLog.INFORMACAO, "Encontrou classes atraves de: UtilitariosReflexao.getClassLoader().getResource(aRecurso);");
//        }
//
//        if (url==null) {
//            url = ClassLoader.getSystemResource(aRecurso);
//            //JSLogger.loga(NivelLog.INFORMACAO, "Encontrou classes atraves de: ClassLoader.getSystemResource(aRecurso);");
//        }
//
//        return url;
//    }
//
//    /**
//     * @param classPath - Path do arquivo ou package da classe.
//	 * Retorna, como String, o package da classe do file path informado.
//	 * Considera o diretorio "/bin" ou "/src" como delimitador caso exista.
//	 */
//    public static String[] getDiretorio_Pacote(String classPath) {
//		String pacote = new String(classPath);
//		pacote = pacote.replace('\\', '.').replaceFirst(".class", "")
//				.replaceFirst(".java", "");
//
//		int inicioPackage = -1;
//		if (pacote.contains(".src.")) {
//			inicioPackage = pacote.indexOf(".src.") + 5;
//		} else if (pacote.contains(".bin.")) {
//			inicioPackage = pacote.indexOf(".bin.") + 5;
//		}
//
//		if (inicioPackage == -1) {
//			inicioPackage = 0;
//		}
//
//		pacote = pacote.substring(inicioPackage, pacote.length());
//		String diretorio = classPath.substring(0, inicioPackage);
//		return new String[] { diretorio, pacote };
//	}
//
//    public static Class<?> getClasse(String path) {
//		JFileChooser fc = new JFileChooser();
//		fc
//				.setSelectedFile(new File(path));
//		int returnVal = fc.showOpenDialog(null);
//		if (returnVal == JFileChooser.APPROVE_OPTION) {
//			String classPath = fc.getSelectedFile().getAbsolutePath();
//			String dirPac[] = UtilitariosArquivo
//			.getDiretorio_Pacote(classPath);
//			// Create a File object on the root of the directory containing the
//			// class file
//			File file = new File(dirPac[0]);
//			try {
//				// Convert File to a URL
//				URL url = file.toURL();
//				URL[] urls = new URL[] { url };
//				// Create a new class loader with the directory
//				ClassLoader cl = new URLClassLoader(urls);
//				Class<?> cls = cl.loadClass(dirPac[1]);
//				return cls;
//			} catch (MalformedURLException e) {
//				e.printStackTrace();
//			} catch (ClassNotFoundException e) {
//				e.printStackTrace();
//			} catch (SecurityException e) {
//				e.printStackTrace();
//			} catch (IllegalArgumentException e) {
//				e.printStackTrace();
//			}
//		}
//		return null;
//	}
//
//    public static InputStream carregaArquivo(String filePath, boolean classpath) throws Exception
//    {
//        if(classpath) {
//			return UtilitariosReflexao.getClassLoader().getResourceAsStream(filePath);
//		} else {
//			return new TempFileInputStream(new File(filePath), false);
//		}
//    }
//
//    public static class ZipUtils {
//        //*TODO ver se existe a possibilidade de colocar SENHA no ZIP...
//        public static class EntradaZip {
//            protected final InputStream stm;
//            private final String nomeEntrada;
//            public EntradaZip(InputStream stm, String nomeEntrada) {
//                this.stm = stm;
//                this.nomeEntrada = nomeEntrada;
//            }
//            public EntradaZip(File f) throws IOException {
//                this(f, ZipUtils.getCaminhoParaZip(f));
//            }
//            public EntradaZip(File f, String nomeEntrada) throws IOException {
//            	if (f.isDirectory()) {
//            		this.stm = null;
//            		this.nomeEntrada = nomeEntrada.replaceAll("[\\\\]", "/") +'/';
//            	} else {
//            		this.stm = new TempFileInputStream(f, false);
//            		this.nomeEntrada = nomeEntrada;
//            	}
//            }
//            /**
//             * @param pastaCaminhoRelativo se <> null, o nome da entrada serah relativo a essa pasta fornecida.
//             * @return
//             */
//            public String getNomeEntrada(File pastaCaminhoRelativo) {
//            	if (pastaCaminhoRelativo==null) {
//            		return this.nomeEntrada;
//            	} else {
//            		String caminhoBase = ZipUtils.getCaminhoParaZip( pastaCaminhoRelativo );
//            		if (this.nomeEntrada.startsWith(caminhoBase)) {
//            			return this.nomeEntrada.substring(caminhoBase.length() + 1); // + 1 da barra "/"
//            		} else {
//            			return this.nomeEntrada;
//            		}
//            	}
//            }
//        }
//
//        /**
//         * Zipa os arquivos "files" no arquivo "fOut". Se "fOut" existir, o mesmo ser� SOBRESCRITO!
//         * @param files
//         * @param fOut
//         * @throws IOException
//         */
//        public static void zipa(File[] files, File fOut/*, String senha*/) throws IOException {
//            zipa(files, fOut/*, senha*/, true);
//        }
//
//        /**
//         * Acerta o nome do arquivo, com caminho, para ser uma entrada v�lida no ZIP.
//         * @param f o arquivo
//         * @return o nome da entrada ZIP correspondente
//         */
//        public static String getCaminhoParaZip(File f) {
//			String result = f.getPath();
//			result = result.replaceAll("[\\\\]", "/");
//			return result;
//		}
//
//		/**
//         * Zipa os arquivos "files" no arquivo "fOut". Se "fOut" existir, o mesmo ser� SOBRESCRITO!
//         * @param files
//         * @param fOut
//         * @param guardarCaminhosArquivos se true, as entradas do zip guardar�o os caminhos dos respectivos arquivos.
//         *        Sen�o, somente o nome do arquivo servir� como nome das entradas no ZIP
//         * @throws IOException
//         */
//        public static void zipa(File[] files, File fOut/*, String senha*/, boolean guardarCaminhosArquivos) throws IOException {
//            zipa(Arrays.asList(files), fOut/*, senha*/, guardarCaminhosArquivos);
//        }
//
//        /**
//         * Zipa os arquivos "files" no arquivo "fOut". Se "fOut" existir, o mesmo ser� SOBRESCRITO!
//         * @param files
//         * @param fOut
//         * @param guardarCaminhosArquivos se true, as entradas do zip guardar�o os caminhos dos respectivos arquivos.
//         *        Sen�o, somente o nome do arquivo servir� como nome das entradas no ZIP
//         * @throws IOException
//         */
//        public static void zipa(List<File> files, File fOut/*, String senha*/, boolean guardarCaminhosArquivos) throws IOException {
//        	zipa(files, fOut, guardarCaminhosArquivos, Deflater.DEFAULT_COMPRESSION);
//        }
//
//        /**
//         * Zipa os arquivos "files" no arquivo "fOut". Se "fOut" existir, o mesmo ser� SOBRESCRITO!
//         * @param files
//         * @param fOut
//         * @param nivelCompressao de 0 (nenhuma compressao) a 9 (maxima compressao)
//         * @param guardarCaminhosArquivos se true, as entradas do zip guardar�o os caminhos dos respectivos arquivos.
//         *        Sen�o, somente o nome do arquivo servir� como nome das entradas no ZIP
//         * @throws IOException
//         */
//        public static void zipa(List<File> files, File fOut/*, String senha*/, boolean guardarCaminhosArquivos, int nivelCompressao) throws IOException {
//            if (files.contains(fOut)) {
//                throw new RuntimeException("Arquivo de sa�da n�o pode estar dentre os arquivos a serem zipados. Arquivo de saida ["+fOut.getPath()+"]");
//            }
//            OutputStream outStm = new BufferedOutputStream( new FileOutputStream(fOut) );
//            zipa(files, outStm, guardarCaminhosArquivos, nivelCompressao);
//            outStm.close();
//        }
//
//        /**
//         * Zipa os arquivos "files" no arquivo "fOut". Se "fOut" existir, o mesmo ser� SOBRESCRITO!
//         * @param files
//         * @param fOut
//         * @param nivelCompressao de 0 (nenhuma compressao) a 9 (maxima compressao)
//         * @param guardarCaminhosArquivos se true, as entradas do zip guardar�o os caminhos dos respectivos arquivos.
//         *        Sen�o, somente o nome do arquivo servir� como nome das entradas no ZIP
//         * @param pastaRaizCaminhosRelativos se null, nao guarda caminhos relativos (quando guardaCaminhosArquivos=true).
//         *        se <> null, os caminhos serao relativos a essa pasta
//         * @throws IOException
//         */
//        public static void zipa(List<File> files, OutputStream outStm/*, String senha*/, boolean guardarCaminhosArquivos, int nivelCompressao) throws IOException {
//        	zipa(files, outStm/*, String senha*/, guardarCaminhosArquivos, nivelCompressao, null);
//        }
//
//        /**
//         * Zipa os arquivos "files" no arquivo "fOut". Se "fOut" existir, o mesmo ser� SOBRESCRITO!
//         * @param files
//         * @param fOut
//         * @param nivelCompressao de 0 (nenhuma compressao) a 9 (maxima compressao)
//         * @param guardarCaminhosArquivos se true, as entradas do zip guardar�o os caminhos dos respectivos arquivos.
//         *        Sen�o, somente o nome do arquivo servir� como nome das entradas no ZIP
//         * @param pastaRaizCaminhosRelativos se null, nao guarda caminhos relativos (quando guardaCaminhosArquivos=true).
//         *        se <> null, os caminhos serao relativos a essa pasta
//         * @throws IOException
//         */
//        public static void zipa(List<File> files, OutputStream outStm/*, String senha*/, boolean guardarCaminhosArquivos, int nivelCompressao, File pastaRaizCaminhosRelativos) throws IOException {
//            List<EntradaZip> entradas = new ArrayList<>(files.size());
//            for (File f:files) {
//                if (guardarCaminhosArquivos) {
//                    entradas.add( new EntradaZip(f) );
//                } else {
//                    entradas.add( new EntradaZip(f,f.getName()) );
//                }
//            }
//            zipa(entradas, outStm/*, senha*/, nivelCompressao, pastaRaizCaminhosRelativos);
//        }
//
//        /**
//         * Zipa as entradas "ins" no stream "out".
//         * @param files
//         * @param fOut
//         * @throws IOException
//         */
//        public static void zipa(List<EntradaZip> ins, OutputStream out/*, String senha*/) throws IOException {
//        	zipa(ins, out, Deflater.DEFAULT_COMPRESSION);
//        }
//
//        /**
//         * Zipa as entradas "ins" no stream "out".
//         * @param nivelCompressao de 0 (nenhuma compressao) a 9 (maxima compressao)
//         * @param files
//         * @param fOut
//         * @throws IOException
//         */
//        public static void zipa(List<EntradaZip> ins, OutputStream out/*, String senha*/, int nivelCompressao) throws IOException {
//        	zipa(ins, out/*, String senha*/, nivelCompressao, null);
//        }
//
//        /**
//         * Zipa as entradas "ins" no stream "out".
//         * @param nivelCompressao de 0 (nenhuma compressao) a 9 (maxima compressao)
//         * @param files
//         * @param fOut
//         * @throws IOException
//         * @param pastaRaizCaminhosRelativos se null, nao guarda caminhos relativos (quando guardaCaminhosArquivos=true).
//         *        se <> null, os caminhos serao relativos a essa pasta
//         */
//        public static void zipa(List<EntradaZip> ins, OutputStream out/*, String senha*/, int nivelCompressao, File pastaRaizCaminhosRelativos) throws IOException {
//            byte[] buf = new byte[65530];
//            ZipOutputStream zipOut = new ZipOutputStream(out);
//            zipOut.setLevel(nivelCompressao);
//            try {
//                for (EntradaZip inAtu : ins) {
//                	String nomeEntrada = inAtu.getNomeEntrada(pastaRaizCaminhosRelativos);
//					//System.out.println("eZip:["+nomeEntrada+"]");
//                    // Add ZIP entry to output stream.
//                    zipOut.putNextEntry(new ZipEntry(nomeEntrada));
//
//                    if (inAtu.stm!=null) {
//                    	// Transfer bytes from the file to the ZIP file
//                    	int len;
//                    	while ((len = inAtu.stm.read(buf)) > 0) {
//                    		zipOut.write(buf, 0, len);
//                    	}
//                        zipOut.closeEntry();
//                        inAtu.stm.close();
//                    } else {
//                        zipOut.closeEntry();
//                    }
//                }
//            } finally {
//                zipOut.close();
//                zipOut.flush();
//            }
//        }
//    }
//
//
//    /**
//     * Descompacta um arquivo no formato Zip ou Jar: ".zip" ou ".jar".
//     * @param caminho arquivo
//     */
//    public static boolean unzip(String arquivo)
//    {
//    	UnZipUtils unzip = new UnZipUtils();
//    	return unzip.unZip(arquivo);
//    }
//
//    public static void main3(String[] args) {
//    	try {
//    		File arq1 = new File("/home/vmware/Win7Virtual/Windows 7 - Perfil C/Windows 7-s001.vmdk");
//    		File arq2 = new File("/home/vmware/Win7Virtual/Windows 7 - Perfil C/Windows 7-s001.vmdk_COP");
//    		copiaArquivoLancandoExcecao(arq1, arq2, false);
//
//    		arq1 = new File("/home/vmware/Win7Virtual/Windows 7 - Perfil C/Windows 7.nvram");
//    		arq2 = new File("/home/vmware/Win7Virtual/Windows 7 - Perfil C/Windows 7.nvram_COP");
//    		copiaArquivoLancandoExcecao(arq1, arq2, false);
//
//    	} catch (Exception e) {
//    		e.printStackTrace();
//    	}
//
//	}
//
//    public static void main2(String[] args) {
//    	{//testa equals de arquivos...
//    		try {
//				InputStream i1 = new TempFileInputStream(new File("/home/lucio/Documentos/grive/Documentos/ContratoCompraEVendaApartamentoRosely_TerraNovaNature (outra c�pia).pdf"), false);
//				InputStream i2 = new TempFileInputStream(new File("/home/lucio/Documentos/grive/Documentos/ContratoCompraEVendaApartamentoRosely_TerraNovaNature (c�pia).pdf"), false);
//				//InputStream i2 = new TempFileInputStream(new File("/home/lucio/Documentos/grive/Documentos/ContratoCompraEVendaApartamentoRosely_TerraNovaNature_PrimeiraPag.pdf"), false);
//
//				Blob blob1 = MArquivosBinariosSistema.getBlob(i1);
//				Blob blob2 = MArquivosBinariosSistema.getBlob(i2);
//
//				System.out.println("Arquivos mesmo conteudo? "+equals(blob1, blob2));
//
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//
//    	}
//
//        System.out.println( Arrays.toString(args));
//        System.out.println( Arrays.toString( System.getProperties().entrySet().toArray() ) );
//
//        System.out.println( "DirApp="+getDiretorioAplicacao() );
//
////        UnZipUtils unz = new UnZipUtils();
//        System.out.println("deszipando...");
////        unz.unZip( "/home/lucio/testezip/tempCVS.zip", "/home/lucio/testezip");
//        try {
//        System.out.println("DirPara Extrair: "+getDiretorioAplicacao());
////          unz.unZip(new FileInputStream("/home/lucio/testezip/tempCVS.zip"), "/home/lucio/testezip");
//            new UnZipUtils().unZip(new FileInputStream("/home/lucio/testezip/tempCVS.zip"), getDiretorioAplicacao());
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//    }

    public static String getDiretorioAplicacao()
	{
		return System.getProperty("user.dir");
	}

//	/**
//	 * Retorna o caminho (path) absoluto no sistema de arquivos, a partir do caminho parcial informado.
//	 * @param pathRelativo caminho parcial
//	 * @return
//	 */
//	public static String getCaminhoAbsoluto(String pathRelativo) {
//		return new File(pathRelativo).getAbsolutePath();
//	}
//
//	/**
//	 * @param nome um string qualquer
//	 * @param removerAcentuacao se true, os acentos tamb�m ser�o removidos (usando o respectivo caractere sem acentua��o).
//	 * @return um string baseado em nome, por�m com caracteres especiais (que n�o s�o aceitos como nome de arquivo) substitu�dos por '_'
//	 */
//	public static String normalizaNomeParaArquivo(String nome, boolean removerAcentuacao) {
//		return normalizaNomeParaArquivo(nome, removerAcentuacao, '_');
//	}
//
//	/**
//	 * @param nome um string qualquer
//	 * @param removerAcentuacao se true, os acentos tamb�m ser�o removidos (usando o respectivo caractere sem acentua��o).
//	 * @param caractereSubstituicao o caractere que serah usado quando um caractere especial nao aceito deva ser sobrescrito
//	 * @return um string baseado em nome, por�m com caracteres especiais (que n�o s�o aceitos como nome de arquivo) substitu�dos por 'caractereSubstituicao'
//	 * OBS: no resultado, sao "encurtados" multiplos de "caractereSubstituicao".
//	 *     Ex: se o carac for "_" e resultado "asd_sd__da___", o retorno fica: "asd_sd_da_"
//	 * OBS2: espa�os (" ") tamb�m s�o substituidos por "caractereSubstituicao"
//	 */
//	public static String normalizaNomeParaArquivo(String nome, boolean removerAcentuacao, char caractereSubstituicao) {
//		return normalizaNomeParaArquivo(false, nome, removerAcentuacao, caractereSubstituicao);
//	}
//
//	/**
//	 * @param nomeComCaminho se true, o "nome" serah considerado como contendo caminho (pastas tipo /pasta1/pasta2/nome!@#ParaNormalizar.|XPTO)
//	 *        se false, inclusive os caracteres de {@link File#separatorChar} serao substituidos (se o nome representava pastas, pode ser que, no final, o retorno nao mais represente!)
//	 * @param nome um string qualquer
//	 * @param removerAcentuacao se true, os acentos tamb�m ser�o removidos (usando o respectivo caractere sem acentua��o).
//	 * @param caractereSubstituicao o caractere que serah usado quando um caractere especial nao aceito deva ser sobrescrito
//	 * @return um string baseado em nome, por�m com caracteres especiais (que n�o s�o aceitos como nome de arquivo) substitu�dos por 'caractereSubstituicao'
//	 * OBS: no resultado, sao "encurtados" multiplos de "caractereSubstituicao".
//	 *     Ex: se o carac for "_" e resultado "asd_sd__da___", o retorno fica: "asd_sd_da_"
//	 * OBS2: espa�os (" ") tamb�m s�o substituidos por "caractereSubstituicao"
//	 */
//	public static String normalizaNomeParaArquivo(boolean nomeComCaminho, String nome, boolean removerAcentuacao, char caractereSubstituicao) {
//		if (nomeComCaminho) {
//			int posUltBarra = nome.lastIndexOf(File.separatorChar);
//			if (posUltBarra<0) {
//				return normalizaNomeParaArquivo(false, nome, removerAcentuacao, caractereSubstituicao);
//			} else if (posUltBarra==nome.length()-1) { //Final com "/"... sem arquivo (vazio)
//				return nome;
//			} else {
//				return nome.substring(0, posUltBarra+1) + normalizaNomeParaArquivo(false, nome.substring(posUltBarra+1), removerAcentuacao, caractereSubstituicao);
//			}
//		}
//
//	    final String CHARS_INVALIDOS_ARQUIVOS="?!/\"\\*&%|'@`':,><����������";
//
//	    if (removerAcentuacao) {
//	        nome=UtilitariosString.removeAcentosNova(nome);
//	    }
//	    while(nome.contains("  ")) {
//	    	nome = nome.replaceAll("  ", " ");
//	    }
//	    char[] result = nome.toCharArray();
//	    for (int i=0; i<result.length; i++) {
//	        if (CHARS_INVALIDOS_ARQUIVOS.indexOf(result[i])>=0) {
//	            result[i]=caractereSubstituicao;
//	        } else if (result[i]!='.'
//	        		&& !Character.isJavaIdentifierPart(result[i])
//	        ) {
//	            result[i]=caractereSubstituicao;
//	        }
//	    }
//
//	    String retorno = new String(result);
//	    while(retorno.contains(""+caractereSubstituicao+caractereSubstituicao)) {
//	    	retorno = retorno.replaceAll(""+caractereSubstituicao+caractereSubstituicao, caractereSubstituicao+"");
//	    }
//
//	    if (retorno.length()>240) {
//	    	retorno=retorno.substring(0,240);
//	    }
//
//	    return retorno;
//	}
//
//	/**
//	 * Descompacta 'entrada' utilizando algoritmo GZIP e joga resultado em 'saida'
//	 * @param entrada
//	 * @param saida
//	 * @throws IOException
//	 */
//	public static void descompacta(InputStream entrada, OutputStream saida) throws IOException {
//	    new UnGZipCodec(entrada).decodeTo(saida);
//	}
//
//	/**
//	 * Compacta 'entrada' utilizando algoritmo GZIP e joga resultado em 'saida'
//	 * @param entrada
//	 * @param saida
//	 * @throws IOException
//	 */
//	public static void compacta(InputStream entrada, OutputStream saida) throws IOException {
//	    new GZipCodec(saida).encodeFrom(entrada);
//	}
//
//    /**
//     * Retorna um String[] com x elementos, sendo x=qtdLinhasParaTras+qtdLinhasParaFrente+1,
//     * e tendo o elemento m�dio o string conforme o byteInicialLinha
//     * Se as linhas tras/frente forem insuficientes para preencher a sa�da, null ser� colocado no lugar da linha inexistente.
//     * @throws IOException
//     */
//    @SuppressWarnings("deprecation")
//    public static String[] extraiParteArquivoTexto(File f, long byteInicialLinha, int qtdLinhasParaTras, int qtdLinhasParaFrente) throws IOException {
//        String[] result = new String[qtdLinhasParaTras+qtdLinhasParaFrente+1];
//
//        RandomAccessFile rf=new RandomAccessFile( f, "r" );
//        try {
//            int idx;
//
////            long byteIniBuff=Math.max(f.length()-qtdBytesLeituraUltLinha , 0);
////            rf.seek(byteIniBuff);
////            byte[] buff=new byte[(int) (f.length()- byteIniBuff)];
////            int lido=rf.read(buff);
////            assert lido==buff.length;
////
////            int fimLinha=buff.length-1;
////            for (int i=buff.length-1; i>0; i--) {
////                if (buff[i-1]==0x0D && buff[i]==0x0A) {
////                    String s=new String(buff, 0, i+1, fimLinha-i);
////                    fimLinha=i-2;
////                    if (!desconsideraLinhaEmBranco || !s.trim().equals("")) {
////                        result[1]=s;
////                        break;
////                    }
////                }
////            }
////            if (result[1]==null && byteIniBuff==0) {
////                result[1]=result[0];
////            }
//
//            { //faz leitura para tras...
//                for (int i=0;i<qtdLinhasParaTras;i++) {
//                    result[i]=null;
//                }
//
//                int tamBuf=5000;
//                if ((byteInicialLinha-5000)<0) {
//                    tamBuf=(int)byteInicialLinha-1;
//                }
//                byte[] buf=new byte[Math.max(tamBuf,0)];
//                rf.seek(Math.max( byteInicialLinha-tamBuf-1  , 0));
//                tamBuf=rf.read(buf);
//
//                int fimLinha=tamBuf-1;
//                for (int idxLin=qtdLinhasParaTras-1; idxLin>=0; idxLin--) {
//
//                    for (int i=fimLinha; i>1; i--) {
//                        if (buf[i-2]==0x0D && buf[i-1]==0x0A) {
//                            String s=new String(buf, 0, i, fimLinha-i);
//                            fimLinha=i-2;
//                            result[idxLin]=s;
//                            break;
//                        }
//                    }
//                    if (fimLinha>0 && result[idxLin]==null) {
//                        result[idxLin]=new String(buf, 0, 0, fimLinha);
//                        fimLinha=0;
//                    }
//                }
//            }
//
//            idx=qtdLinhasParaTras;
//            rf.seek(byteInicialLinha);
//            result[idx++]=rf.readLine();
//
//            for (;idx<qtdLinhasParaTras+qtdLinhasParaFrente+1; idx++) {
//                result[idx]=rf.readLine();
//            }
//
//        } finally { rf.close(); }
//
//        return result;
//    }
//
//    /**
//     * Codifica o string passado de acordo com o algoritmo indicado
//     * @param caracteres
//     * @param algoritmo
//     * @return
//     * @throws NoSuchAlgorithmException
//     */
//    public static String encripta(String caracteres, String algoritmo) throws NoSuchAlgorithmException {
//		MessageDigest digest = MessageDigest.getInstance(algoritmo);
//		digest.update(caracteres.getBytes());
//		return ByteToBase64Codec.byteArrayToBase64String(digest.digest());
////		BASE64Encoder encoder = new BASE64Encoder();
////		return encoder.encode(digest.digest());
//	}
//
//    /**
//     * Transforma um objeto em um array de bytes
//     * @param objeto
//     * @return
//     * @throws IOException
//     */
//    public static byte[] serializaObjeto(Object objeto) throws IOException {
//
//    	ByteArrayOutputStream bos = new ByteArrayOutputStream();
//		ObjectOutputStream objGravar = new ObjectOutputStream(bos);
//		objGravar.writeObject(objeto);
//		objGravar.flush();
//		objGravar.close();
//
//		return bos.toByteArray();
//    }
//
//    /**
//	 * Transforma um array de bytes novamente em um Objeto
//	 * @param objetoSerializado
//	 * @return
//	 * @throws IOException
//	 * @throws ClassNotFoundException
//	 */
//	@SuppressWarnings("unchecked")
//    public static <T> T desserializaObjeto(byte[] objetoSerializado) throws IOException, ClassNotFoundException {
//		ObjectInputStream objInput = new ObjectInputStream(new ByteArrayInputStream(objetoSerializado));
//		return (T) objInput.readObject();
//	}
//
//	/**
//     * Substitui express�es do tipo ${NOME} com o valor da propriedade de sistema correspondente
//	 * @param propriedadesFonte Propriedades nas quais os valores ser�o buscados
//     */
//    public static String substituiExpressao(String valor) {
//    	return substituiExpressao(valor, System.getProperties());
//    }
//
//	/**
//     * Substitui express�es do tipo ${NOME} com o valor da propriedade correspondente
//	 * @param propriedadesFonte Propriedades nas quais os valores ser�o buscados
//     */
//    public static String substituiExpressao(String valor, Properties propriedadesFonte) {
//        StringBuffer sb = new StringBuffer();
//        int prev = 0;
//        int pos;
//        while ((pos = valor.indexOf("$", prev)) >= 0) {
//            if (pos > 0) {
//                sb.append(valor.substring(prev, pos));
//            }
//            if (pos == (valor.length() - 1)) {
//                sb.append('$');
//                prev = pos + 1;
//            } else if (valor.charAt(pos + 1) != '{') {
//                sb.append('$');
//                prev = pos + 1;
//            } else {
//                int endName = valor.indexOf('}', pos);
//                if (endName < 0) {
//                    sb.append(valor.substring(pos));
//                    prev = valor.length();
//                    continue;
//                }
//                String n = valor.substring(pos + 2, endName);
//                String v = propriedadesFonte.getProperty(n);
//
//                if (v == null) {
//					v = "${" + n + "}";
//				}
//
//                sb.append(v);
//                prev = endName + 1;
//            }
//        }
//
//        if (prev < valor.length()) {
//			sb.append(valor.substring(prev));
//		}
//        return sb.toString();
//    }

    /**
     * @param fpath
     * @param nivel nao utilizado no momento. passe 0.
     * @return a lista dos arquivos do fpath passado (incluindo arquivos em sub-pastas). Pastas vazias NAO serao retornadas.
     */
    public static List<File> listaPathsArquivosDiretorioRecursivamente(File fpath, int nivel)
	{
    	return listaPathsArquivosDiretorioRecursivamente(fpath, nivel, false);
	}

    /**
     * @param fpath
     * @param nivel
     * @param incluirPastasVazias se TRUE, as pastas vazias serao retornados como entradas no retorno.
     * @return
     */
    public static List<File> listaPathsArquivosDiretorioRecursivamente(File fpath, int nivel, boolean incluirPastasVazias)
	{
		List<File> arquivos = new ArrayList<>();
		//if(fpath.exists()) {
			File[] files = fpath.listFiles();
        	if (incluirPastasVazias && files.length==0) {
        		arquivos.add(fpath);
        	}
            for (File file : files) {
                if (file.isDirectory()) {
                	arquivos.addAll(listaPathsArquivosDiretorioRecursivamente(file, ++nivel, incluirPastasVazias));
                } else {
                	arquivos.add(file);
                }
            }
		//}
		return arquivos;
	}

    /**
     * Retorna apenas as pastas contidas no fpath
     * @param fpath
     * @param nivel passe 0 inicialmente
     * @param apenasPastasVazias se true, soh retorna se a pasta nao apresentar arquivos.
     * @return
     */
    public static List<File> listaPathsDiretorioRecursivamente(File fpath, boolean apenasPastasVazias, int nivel) {
		List<File> arquivos = new ArrayList<>();
		if(fpath.exists()) {
			File[] files = fpath.listFiles();
            for (File file : files) {
                if (file.isDirectory()) {
                	if (!apenasPastasVazias || !pastaTemArquivos(file)) {
                		arquivos.add(file);
                	}
                	arquivos.addAll(listaPathsDiretorioRecursivamente(file, apenasPastasVazias, ++nivel));
                }
            }
		}
		return arquivos;
	}

    /**
     * @param pasta
     * @return true se a pasta contiver arquivos diretos (nao conta subpastas).
     */
    public static boolean pastaTemArquivos(File pasta) {
    	for (File f : pasta.listFiles()) {
    		if (f.isFile()) {
    			return true;
    		}
    	}
    	return false;
    }

//    /**
//     * @param arquivo
//     * @return true caso o Sistema Operacional esteja bloqueando o dado arquivo por outro processo
//     * OBS: Linux NAO TEM esse funcionamento de lock de arquivo!
//     */
//    public static boolean podeEscrever_TentandoComLock(File arquivo){
//            try {
//            	RandomAccessFile raf;
//				//System.out.print("tenta lock - ");
//            	FileChannel channel = (raf = new RandomAccessFile(arquivo, "rw")).getChannel();
//                FileLock lock = channel.tryLock();
//
//                if(lock==null){
//                	//System.out.println("if");
//                    channel.close();
//                    raf.close();
//                    return false;
//                }else{
//                	//System.out.println("else");
//                    lock.release();
//                    channel.close();
//                    raf.close();
//                    return true;
//                }
//            } catch (Exception e) {
//            	//System.out.println("catch " + e.getMessage());
//                return false;
//            }
//        }
//
//        public static boolean isSomenteLeitura(File arquivo){
//            return !arquivo.canWrite();
//        }
//
//    /**
//     * Realiza a procura por arquivos que contenham "nomeArquivoProcurado" em seu nome.
//     * @param nomeArquivoProcurado
//     * @param raizProcura
//     * @param maxNivelProcura
//     * @param nivelAtual
//     * @param retornarPrimeiroOcorrencia
//     * @return
//     */
//    public static List<String> procuraArquivos(String nomeArquivoProcurado, String raizProcura, int maxNivelProcura, int nivelAtual, boolean retornarPrimeiroOcorrencia)
//    {
//    	List<String> arquivos = new ArrayList<>();
//    	if(nivelAtual>maxNivelProcura) {
//			return arquivos;
//		}
//    	File fpath = new File(raizProcura);
//    	//if(raizProcura.startsWith(pathIndesejado)) continue;
//    	//System.out.println("procurando no caminho: "+raizProcura);
//    	if(fpath.exists())
//    	{
//    		File[] files = fpath.listFiles();
//    		if(files!=null){
//    			for (File file : files) {
//    				if (file.isDirectory()) {
//    					arquivos.addAll(procuraArquivos( nomeArquivoProcurado,file.getAbsolutePath(),maxNivelProcura,nivelAtual+1,retornarPrimeiroOcorrencia));
//    				} else {
//    					if(file.getName().contains(nomeArquivoProcurado)){
//    						String p = file.getPath();
//    						arquivos.add(p);
//    					}
//    				}
//    				if(retornarPrimeiroOcorrencia){
//    					if(arquivos.size()>0) {
//							break;
//						}
//    				}
//    			}
//    		}
//    	}
//    	return arquivos;
//    }
//
//	private static String criaPathRelativo(String path, int nivel)
//	{
//		return path;
//	}

    /**
     * Retorna todos os ARQUIVOS (pastas nao) contidos em uma determinada pasta (inclui sub-pastas)
     * @param diretorio
     * @return
     * @throws IOException
     */
    public static List<File> getArquivosDiretorio(File diretorio) throws IOException {
    	return getArquivosDiretorio(diretorio, null, false);
    }

    /**
     * Retorna todos os ARQUIVOS contidos em uma determinada pasta (inclui sub-pastas)
     * @param diretorio
     * @param incluirPastas se true, incluira na lista de File as subpastas do diretorio. O diretorio em si NAO constara na lista.
     * @return
     * @throws IOException
     */
    public static List<File> getArquivosDiretorio(File diretorio, FilenameFilter filtro, boolean incluirPastas) throws IOException {
    	ArrayList<File> result = new ArrayList<>(20);
    	getArquivosDiretorioRec(diretorio, result, filtro, incluirPastas);
    	return result;
    }

    private static void getArquivosDiretorioRec(File diretorio, List<File> arquivos, FilenameFilter filtro, boolean incluirPastas) throws IOException{
    	File[] arquivosDir = ((filtro==null) ? diretorio.listFiles() : diretorio.listFiles(filtro) );
    	if (arquivosDir==null) {
    		return;
    	}

    	for (File element : arquivosDir) {
    		if (element.isDirectory()) {
    			getArquivosDiretorioRec(element, arquivos, filtro, incluirPastas);
    			if (incluirPastas) {
    				arquivos.add(element);
    			}
    		} else {
    			arquivos.add(element);
    		}
    	}
    }

//
//    /**
//     * Abre o arquivo com o programa associado no sistema operacional
//     * @param arquivo
//     * @param executarProcessBuilder
//     * Se executarProcessBuilder for false, n�o executa o programa.
//	 * Se for true, executa instantaneamente. Nesse caso, se for um arquivo do BROffice, retorna NULL.
//	 *
//	 * @return o ProcessBuilder que abrir� o arquivo. NULL caso executarProcessBuilder=true e arquivo for BROffice.
//	 *
//     */
//    public static ProcessBuilder abreArquivoProgramaAssociado(File arquivo, boolean executarProcessBuilder) throws IOException {
////        String[] cmdarray = new String[] { "cmd", "/c", "start", "\"\"", arquivo.getCanonicalPath() };
////        try {
////			final int res = Runtime.getRuntime().exec(cmdarray).waitFor();
////		} catch (InterruptedException e1) {
////			// TODO Auto-generated catch block
////			e1.printStackTrace();
////		}
////
//
//    	String nomeArquivo = arquivo.getAbsolutePath();
//		if (executarProcessBuilder) {
//			if (isDocumentoOpenOffice(arquivo)) {
//				try {
//					OOUtils.open(arquivo);
//				} catch (IOException e) {
//					CaixaDialogo caixa = new CaixaDialogo(TITULO_MSG_ERRO_EDITOR_ODT_NAO_CONFIGURADO,
//							MENSAGEM_EDITOR_ODT_NAO_CONFIGURADO, new String[] { "OK" }, "", TipoCaixaDialogo.ERRO);
//					caixa.exibe();
//				}
//			} else {
//				try {
//					FileUtils.openFile(arquivo);
//				} catch (IOException ioException) {
//					JSLogger.loga(NivelLog.ALERTA, "Erro ao abrir nativamente o arquivo ["+arquivo.getAbsolutePath()+"]. Tentando abri-lo com solucao alternativa (s� windows).");
//					switch (SistemaOperacional.getSistemaOperacionalAtual()) {
//					case WINDOWS :
//						//alternativa caso o desktop.open nao funcione (apenas em windows)
//						// http://www.guj.com.br/java/289838-resolvidoproblemas-ao-abrir-arquivo-pdf
//						Runtime.getRuntime().exec("rundll32 SHELL32.DLL,ShellExec_RunDLL "+ arquivo.getCanonicalPath());
//						break;
//					default:
//					}
//				}
//			}
//			return null;
//		} else {
//			ProcessBuilder pb;
//			switch (SistemaOperacional.getSistemaOperacionalAtual()) {
//	    	case WINDOWS: // Se for Windows, chamar com o rundll32
//	    		pb = new ProcessBuilder( new String[]{"rundll32","url.dll,FileProtocolHandler","\""+nomeArquivo+"\""} );
//	    		if (executarProcessBuilder) {
//	    			pb.start();
//	    		}
//	    		break;
//	    	case LINUX: default:
//	    		pb = new ProcessBuilder( new String[]{"xdg-open",nomeArquivo} );
//	    		if (executarProcessBuilder) {
//	    			try {
//	    				pb.start();
//	    			} catch (IOException e) {
//	    				e.printStackTrace();
//	    				pb = new ProcessBuilder("kde-open",nomeArquivo);
//	    				pb.start();
//	    			}
//	    		}
//	    		break;
//	    	}
//	    	return pb;
//		}
//
//    }
//
//    /**
//     * Abre a <tt>URL</tt> informada com o navegador associado no sistema operacional.<br>
//     * Se <tt>executarProcessBuilder</tt> for false, n�o executa o programa. Se for true, executa instantaneamente.
//     * 
//     * @param url
//     * @param executarProcessBuilder
//     * @return
//     * @throws IOException
//     */
//    public static ProcessBuilder abreURLnoNavegador(String url, boolean executarProcessBuilder) throws IOException {
//		ProcessBuilder pb;
//		switch (SistemaOperacional.getSistemaOperacionalAtual()) {
//	    	case WINDOWS: // Se for Windows, chamar com o rundll32
//	    		pb = new ProcessBuilder(new String[]{"rundll32","url.dll,FileProtocolHandler","\""+url+"\""});
//	    		if (executarProcessBuilder) {
//	    			pb.start();
//	    		}
//	    		break;
//	    	case LINUX: default:
//	    		pb = new ProcessBuilder(new String[]{"xdg-open", url});
//	    		if (executarProcessBuilder) {
//	    			try {
//	    				pb.start();
//	    			} catch (IOException e) {
//	    				e.printStackTrace();
//	    				pb = new ProcessBuilder("kde-open", url);
//	    				pb.start();
//	    			}
//	    		}
//	    		break;
//	    	}
//    	return pb;
//    }
//
//    /**
//     * Retorna, pela extencao, se o arquivo � da suite do LibreOffice (ODT, etc.)
//     * @param arquivo
//     * @return
//     */
//    public static boolean isDocumentoOpenOffice(File arquivo) {
//    	Set<String> extencoesBrOffice = new HashSet<>();
//    	extencoesBrOffice.add("odt"); //Texto ODF
//    	extencoesBrOffice.add("ott"); //Modelo de texto ODF
//    	extencoesBrOffice.add("odm"); //Documento mestre ODF
//    	extencoesBrOffice.add("oth"); //Modelo de documento HTML
//    	extencoesBrOffice.add("ods"); //Planilha ODF
//    	extencoesBrOffice.add("ots"); //Modelo de planilha ODF
//    	extencoesBrOffice.add("odg"); //Desenho ODF
//    	extencoesBrOffice.add("otg"); //Modelo de desenho ODF
//    	extencoesBrOffice.add("odp"); //Apresenta��o ODF
//    	extencoesBrOffice.add("otp"); //Modelo de apresenta��o ODF
//    	extencoesBrOffice.add("odf"); //F�rmula ODF
//    	extencoesBrOffice.add("odb"); //Banco de dados ODF
//    	extencoesBrOffice.add("orp"); //Relat�rio banco de dados ODF
//    	extencoesBrOffice.add("oxt"); //Extens�o BrOffice.org
//
//    	String extensao = getExtensaoSemPonto(arquivo).toLowerCase();
//		return extencoesBrOffice.contains(extensao);
//	}
//
//    /**
//     * Retorna o arquivo tempor�rio com os dados salvos.
//     *
//     * @param blob
//     * @param extensao
//     * @return
//     * @throws Exception
//     */
//	public static File getArquivoTemporario(Blob blob, String extensao) throws Exception {
//		InputStream input = blob.getBinaryStream();
//		return UtilitariosArquivo.codificaArquivo(input, extensao);
//	}
//
//	/**
//	 * Retorna o arquivo tempor�rio com os dados salvos.
//	 * @param input
//	 * @param extensao
//	 * @return
//	 * @throws Exception
//	 */
//	public static File getArquivoTemporario(InputStream input, String extensao) throws Exception {
//		return UtilitariosArquivo.codificaArquivo(input, extensao);
//	}
//
//	private static File codificaArquivo(InputStream input, String extensao) throws FileNotFoundException, IOException {
//		File arquivoTemporario = TempOutputStream.getNewTempFile("arq_", "." + extensao);
//		arquivoTemporario.deleteOnExit();
//
//		OutputStream out = new BufferedOutputStream(new FileOutputStream(arquivoTemporario));
//		BypassEncodeCodec bec = new BypassEncodeCodec(out);
//		bec.encodeFrom(input);
//		bec.close();
//		input.close();
//
//		return arquivoTemporario;
//	}
//
//	public static File abreJanelaSelecaoArquivo(String titulo, final String extensao, Component telaPai) {
//		JSFileChooser fc = new JSFileChooser();
//		fc.setDialogTitle(titulo);
//		fc.setAcceptAllFileFilterUsed(false);
//		fc.addChoosableFileFilter(new FileFilter(){
//			@Override
//			public boolean accept(File f) {
//				return UtilitariosArquivo.getExtensaoSemPonto(f).equalsIgnoreCase(extensao.replace(".", "")) || f.isDirectory();
//			}
//
//			@Override
//			public String getDescription() {
//				return "Arquivos ("+extensao+")";
//			}
//		});
//
//		if(fc.showOpenDialog(telaPai) == JSFileChooser.APPROVE_OPTION) {
//			File arquivo = fc.getSelectedFile();
//			return arquivo;
//		} else {
//			return null;
//		}
//	}
//
//	/**
//	 * Retorna o n�mero de folhas que o <tt>arquivoPDF</tt> possui.
//	 */
//	public static int getNumeroFolhasPDF(File arquivoPDF) throws IOException {
//		PDDocument documento = PDDocument.load(arquivoPDF);
//		return documento.getNumberOfPages();
//	}
//
//	/**
//	 * Retorna o n�mero de folhas que o <tt>arquivoPDF</tt> possui.
//	 * Se o stream passado (arquivoPDF) suportar reset(), ao final � uma chamada a este m�todo.
//	 */
//	public static int getNumeroFolhasPDF(InputStream arquivoPDF) throws IOException {
//		PDDocument documento = PDDocument.load(arquivoPDF);
//		if (arquivoPDF.markSupported()) {
//			arquivoPDF.reset();
//		}
//		return documento.getNumberOfPages();
//	}
//
//	/**
//	 * Aplica o <tt>texto</tt> informado no cabe�alho de todas as p�ginas do <tt>arquivoPDF</tt>
//	 * e retorna um <tt>InputStream</tt> do arquivo contendo a marca d'�gua.
//	 * @param arquivoPDF
//	 * @param texto
//	 * @return
//	 */
//	public static InputStream setMarcaDAguaPDF(InputStream arquivoPDF, String texto) {
//		try {
//			PdfReader reader = new PdfReader(arquivoPDF);
//			int n = reader.getNumberOfPages();
//
//			File arquivoTemporario = TempOutputStream.getNewTempFile("arq_"+ UtilitariosString.getIdUnicoTempo(), ".tmp");
//			arquivoTemporario.deleteOnExit();
//			FileOutputStream saidaTmp = new FileOutputStream(arquivoTemporario);
//
//			// Create a stamper that will copy the document to a new file
//			PdfStamper stamp = new PdfStamper(reader, saidaTmp);
//
//			int i = 1;
//
////			PdfContentByte under;
//			PdfContentByte over;
//
////			Image img = Image.getInstance("semAteste.png");
//			BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA,
//					BaseFont.WINANSI, BaseFont.EMBEDDED);
//
//
//			int fontSize = 12;
//			float x, y, width, height;
//
////			img.setAbsolutePosition(200, 400);
//
//			while (i <= n) {
//				// Watermark under the existing page
////				under = stamp.getUnderContent(i);
////				under.addImage(img);
//
//				// se der problema, verificar uso do cropbox
//				//Rectangle cropBox = reader.getCropBox(i);
//
//				Rectangle pageSize = stamp.getReader().getPageSize(i);
//
//				width = 82;
//				height = 14;
//
//				x = pageSize.right() - 15;
//				y = (pageSize.height()/2) + (width/2);
//
//				// Text over the existing page
//				over = stamp.getOverContent(i);
//				AffineTransform ats = new AffineTransform();
//				ats.rotate(-Math.PI/2, x, y);
//				//ats.translate(pageSize.width()/2, -pageSize.height()/2 );
//				over.transform(ats);
//
//				over.rectangle(x, y, width, height);
//				over.setColorStroke(Color.darkGray);
//				over.stroke();
//
//				over.beginText();
//				over.setColorFill(Color.darkGray);
//				over.moveText(x+3, y+3);
//				over.setFontAndSize(bf, fontSize);
//				over.showText(texto);
//
//				over.endText();
//				i++;
//			}
//
//			stamp.close();
//
//			return new TempFileInputStream(arquivoTemporario, false);
//
//		} catch (Exception e) {
//			e.printStackTrace();
//
//			return null;
//		}
//	}
//
//    /**
//     * @param caminhoPastaResources pasta do JAR que cont�m os resources (especificar, como exemplo, "/scripts/oracle")
//     * @param pastaDestinoExtracao Pasta do sistema de arquivos onde ser�o extra�dos todos arquivos
//     * @param recursivo se true, copia pastas contidas dentro do caminhoPastasResources
//     * @return Os arquivos estraidos
//     * @throws IOException
//     */
//    public static List<File> extraiResourcesParaPasta(String caminhoPastaResources, File pastaDestinoExtracao, boolean recursivo) throws IOException {
//    	File arqJarOuPastaBin = getArquivoJarOuPastaBin();
//
//    	//arqJarOuPastaBin = new File("/home/lucio/iglezias_cvs/34535jserpro_1202012/jserpro/lib/JSerpro-3.1.jar");
//    	//arqJarOuPastaBin = new File("/home/lucio/�rea de trabalho/HOMOLOGACAO/GeracaoVersao4.0_convivenvia3.7e4.0_g1/htdocs_ssl/Distribuicoes/Versoes/4.0/eSafira_4_0_gestor.jar");
//    	JSLogger.loga(NivelLog.ALERTA, "Arquivo JAR raiz: "+arqJarOuPastaBin); //FIXME Lucio: remove isso.
//    	if (arqJarOuPastaBin.isFile()) { //JAR, extrai...
//
//    		List<File> deszipados = new ArrayList<>();
//    		{
//    			CodeSource src = JSerproApp.getInstancia().getClass().getProtectionDomain().getCodeSource();
//    			URL jar = src.getLocation();
//    			JSLogger.loga(NivelLog.ALERTA, "JAR da aplicacao:"+jar.toString());
//
//    			ZipInputStream zip = new ZipInputStream(jar.openStream());
//    			//ZipInputStream zip = new ZipInputStream(new FileInputStream(arqJarOuPastaBin));
//    			ZipEntry entradaZip;
//    			while ( (entradaZip = zip.getNextEntry())!=null) {
//    				if (!entradaZip.isDirectory() &&  entradaZip.getName().startsWith(caminhoPastaResources)) {
//    					File arqOut = new File(pastaDestinoExtracao,entradaZip.getName());
//    					arqOut.getParentFile().mkdirs();
//    					//deszipa a entrada...
//    					OutputStream out = new BufferedOutputStream(new FileOutputStream(
//    							arqOut
//    							));
//    					new BypassDecodeCodec(zip).decodeTo(out);
//    					out.flush();
//    					out.close();
//    					deszipados.add(arqOut);
//    				}
//    			}
//    		}
//
////    		{
////    			UnZipUtils uzip = new UnZipUtils();
////    			ZipFile arqJar = new ZipFile(arqJarOuPastaBin);
////    			List<ZipEntry> entradasDoZipParaExtrair = uzip.getListaEntradasZipComPrefixo(arqJar, caminhoPastaResources);
////
////    			List<File> deszipados = uzip.unZipReturningFiles(arqJar, pastaDestinoExtracao.getAbsolutePath(), entradasDoZipParaExtrair);
////    		}
//
//			if (caminhoPastaResources.endsWith("/")) {
//				caminhoPastaResources = caminhoPastaResources.substring(0, caminhoPastaResources.length()-1);
//			}
//
//			File pastaLogoAposExtracao = new File(pastaDestinoExtracao, caminhoPastaResources);
//			moveArquivoOuPasta(pastaLogoAposExtracao, pastaDestinoExtracao);
//
//			List<File> result = new ArrayList<>(deszipados.size());
//			for (File deszipado : deszipados) {
//				File arqMovido = new File( pastaDestinoExtracao,
//						deszipado.getAbsolutePath().substring( pastaLogoAposExtracao.getAbsolutePath().length() )
//						);
//				result.add(arqMovido);
//				System.out.println("Extraiu: "+arqMovido);
//			}
//			return result;
//
//    	} else { //pastaBin, copia...
//    		if (caminhoPastaResources.endsWith("/")) {
//    			caminhoPastaResources = caminhoPastaResources.substring(0, caminhoPastaResources.length()-1);
//    		}
//    		File dirOrigem = new File(arqJarOuPastaBin,caminhoPastaResources);
//    		return UtilitariosArquivo.copiaDiretorio(dirOrigem, pastaDestinoExtracao, recursivo);
//    	}
//    }

    /**
     * Move a origem (pasta ou arquivo) para o destino (pasta ou arquivo), utilizando sistema de arquivos diretamente.
     * @param origem
     * @param destino
     * @throws IOException
     */
    public static void moveArquivoOuPasta(File origem, File destino) throws IOException {
    	if (origem.isFile()) {
    		//java.nio.file.Files.move(source, target, options); //okTODO usar o Files.copy() da jre do Java 7.
    		if (!destino.getParentFile().exists()) {
    			destino.getParentFile().mkdirs();
    		}
//    		Files.move(origem,destino); //okTODO esse Files.move do Guava nao usa NewIO... cogitar fazer um proprio, que usa FileChannel
    		//java.nio.file.Files.move(origem.toPath(), destino.toPath(), StandardCopyOption.REPLACE_EXISTING);
    		moveArquivoComRetry(origem, destino, true/*permiteContornoSeLock*/);

    	} else if (origem.isDirectory()) {
        	if (!destino.exists()) {
        		throw new IOException("destino n�o existe: "+destino);
        	}
    		List<File> arquivos = listaPathsArquivosDiretorioRecursivamente(origem, 0);
    		for (File file : arquivos) {
    			File arqDestino = new File(destino, file.getAbsolutePath().substring(origem.getAbsolutePath().length()+1) );
    			if (!arqDestino.getParentFile().exists()) {
    				arqDestino.getParentFile().mkdirs();
    			}
    			//Files.move(file,arqDestino);
    			//file.renameTo(dest)
    			//java.nio.file.Files.move(file.toPath(), arqDestino.toPath(), StandardCopyOption.REPLACE_EXISTING);
        		moveArquivoComRetry(file, arqDestino, true/*permiteContornoSeLock*/);
			}

    		{ // move diretorios vazios da origem...
    			List<File> pastasVazias = listaPathsDiretorioRecursivamente(origem, true ,0);
    			for (File file : pastasVazias) {
    				File novaPastaVazia = new File(destino, file.getAbsolutePath().substring(origem.getAbsolutePath().length()+1) );
        			if (!novaPastaVazia.exists()) {
        				novaPastaVazia.mkdirs();
        			}
				}
    		}

    		apagaDiretorio(origem, true);
    	} else {
    		throw new IOException("origem n�o existe: "+origem);
    	}
    	//okFIXME: quando migrar para Java7, usar o java.nio.Files
    }

    /**
     * Move o arquivo origem para a pasta/arquivo destino. Se falhar, faz tentativas por at� 3s antes de falhar (lancando excecao). Util para aguardar algum bloqueio temporario do sistema operacional.
     * @param origem DEVE ser arquivo (nao pasta)
     * @param destino DEVE ser arquivo (nao pasta)
     * @param permiteContornoSeLock se true, caso deu problema ao movimentar no periodo de 3s, serah tentado o metodo de contorno: copia arquivo para destino e exclui arquivo da origem quando der (possivelmente qnd VM Java for encerrada)
     * @throws Exception se ocorreu erro na movimentacao, ateh mesmo na de contorno (copia do arquivo e exclusao origem)
     * @return true caso movimentacao ocorreu ok. 
     * 		   false caso movimentacao feita parcialmente (foi necessaria COPIA do arquivo no destino e exclusao da origem ficarah para depois)
     */
    public static boolean moveArquivoComRetry(final File origem, final File destino, boolean permiteContornoSeLock) throws IOException {
		try {
			UtilitariosThread.runComRetry(
				new UtilitariosThread.RunnableComRetornoETeste<Void>() {
					@Override
					public Void runComRetorno() throws Exception {
						if (!origem.canWrite()) {
							throw new IOException("sem acesso exclusao arquivo origem: "+origem.getAbsolutePath());
						}
						destino.delete(); //deleta destino caso exista... seria sobrescrito mesmo...
						com.google.common.io.Files.move(origem, destino);
			            //java.nio.file.Files.move(origem.toPath(), destino.toPath(), StandardCopyOption.REPLACE_EXISTING); //isso lanca em HOMCD - java-1.7.0-openjdk-1.7.0.131.x86_64 - java.nio.file.FileSystemException: /opt/appfiles/h_34535_esafira/autoscp/pbihrfta/outbox/TesteSISAM.zip: Function not implemented
						return null;
					}
					@Override
					public boolean testeOk(Void retorno, Exception excecaoRetorno) {
						return excecaoRetorno==null;
					}
				}
				, 30000/*ms*/);
			return true;
		} catch (IOException e) {
			//Se caiu aqui, nao conseguiu mover usando abordagem do Sistema Operacional (algum lock, permissao, etc.)
			//JSLogger.loga(NivelLog.ALERTA, "Movimentacao de ["+origem.getAbsolutePath()+"] para ["+destino.getAbsolutePath()+"] usando metodo de contorno: copia seguida de exclusao");
			if (!copiaArquivo(origem, destino, false)) {
				throw e;
			}
			if (!origem.delete()) {
				origem.deleteOnExit();
			}
			return false;
		} catch (Exception e) {
			throw new IOException("Falha ao mover",e);
		}
	}
    
	/**
     * @param dirOrigem pasta origem que serah copiada
     * @param pastaDestinoExtracao pasta destino onde serao copiados os arquivos do dirOrigem
     * @param recursivo se true,  copia pastas contidas dentro do dirOrigem
     * @return os arquivos copiados
     * @throws IOException
     */
    public static List<File> copiaDiretorio(File dirOrigem, File pastaDestinoExtracao, boolean recursivo) throws IOException {
		List<File> origens = recursivo ? getArquivosDiretorio(dirOrigem) : Arrays.asList(dirOrigem.listFiles());
		List<File> origensRelativo = new ArrayList<>(origens.size());

		//System.out.println("pasta origem: "+dirOrigem);
		for (File arq : origens) {
			File arqRelativo = new File(arq.getAbsolutePath().substring(dirOrigem.getAbsolutePath().length()));
			origensRelativo.add(arqRelativo);
			//System.out.println("arq: "+arqRelativo);
		}

		for (File arqRel : origensRelativo) {
			File arqOrigem = new File(dirOrigem,arqRel.getPath());
			File arqDestino = new File(pastaDestinoExtracao,arqRel.getPath());
			if (arqOrigem.isDirectory()) {
				arqDestino.mkdirs();
			} else {
				copiaArquivo(arqOrigem, arqDestino, false);
			}
		}
		return origens;
	}

//	public static boolean equals(Blob blob1, Blob blob2) {
//		if ((blob1 != null && blob2 == null) || (blob1 == null && blob2 != null)) {
//			return false;
//		}
//		if (blob1 == blob2 || blob1.equals(blob2)) {
//			return true;
//		}
//		try {
//			InputStream b1 = blob1.getBinaryStream();
//			InputStream b2 = blob2.getBinaryStream();
//			if (b1.available()!=b2.available()) {
//				return false;
//			}
//
//			if ( (b1 instanceof TempFileInputStream) && (b2 instanceof TempFileInputStream) ) {
//				return equals( ((TempFileInputStream)b1).getFile(), ((TempFileInputStream)b2).getFile() );
//			} else {
//				return equals(b1, b2);
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//			throw new ObjetoExcecao(e);
//		}
//	}
//
//	public static boolean equals(InputStream b1, InputStream b2) {
//		if ((b1 != null && b2 == null) || (b1 == null && b2 != null)) {
//			return false;
//		}
//		if (b1 == b2 || b1.equals(b2)) {
//			return true;
//		}
//		if (!(b1 instanceof BufferedInputStream)) {
//			b1 = new BufferedInputStream(b1);
//		}
//		if (!(b2 instanceof BufferedInputStream)) {
//			b2 = new BufferedInputStream(b2);
//		}
//
//		try {
//			boolean notDone = true;
//			boolean equal = true;
//			int o = -1;
//			int c = -1;
//			while (notDone) {
//				o = b1.read(); //pode ler byte a byte pq estamos trabalhando com BufferedInputStream... nao onera tanto.
//				c = b2.read();
//				if (o == -1 || c == -1) {
//					notDone = false;
//				}
//				if (o != c) {
//					equal = false;
//					notDone = false;
//				}
//			}
//			return equal;
//		} catch (Exception e) {
//			e.printStackTrace();
//			throw new ObjetoExcecao(e);
//		}
//	}

	/**
	 * Compara dois arquivos. Se o CONTEUDO dos dois forem iguais, retorna TRUE.
	 * @param file1
	 * @param file2
	 * @return
	 * @throws IOException
	 */
	public static boolean equals(File file1, File file2) throws IOException {
		final int TAM_BUF=65535;
		if( (file1 != null && file2 == null) || (file1 == null && file2 != null) ) {
			return false;
		}
		if (file1.equals(file2)) {
			return true;
		}
		if (file1.length()!=file2.length()) {
			return false;
		}

        FileInputStream is1=null;
        FileInputStream is2=null;
		FileChannel fc1=(is1=new FileInputStream(file1)).getChannel();
		FileChannel fc2=(is2=new FileInputStream(file2)).getChannel();
        try {
        	fc1.position(0);
        	fc2.position(0);
        	ByteBuffer buf1 = ByteBuffer.allocate(TAM_BUF);
        	ByteBuffer buf2 = ByteBuffer.allocate(TAM_BUF);

			boolean notDone = true;
			boolean equal = true;
			//int lidosTotal=0;
			while (notDone) {
	            buf1.position(0);
	            buf2.position(0);

				int lidos1 = fc1.read(buf1);
				int lidos2 = fc2.read(buf2);
				if (lidos1<=0 || lidos2<=0) {
					notDone=false;
				}
				if (!buf1.equals(buf2)) {
					equal = false;
					notDone = false;
				}
				//lidosTotal+=lidos1;
			}
			//System.out.println("Lidos: "+lidosTotal);
			return equal;
        } finally {
        	try {
				fc1.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
        	try {
				fc2.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
        	if (is1!=null) {
				is1.close();
			}
        	if (is2!=null) {
				is2.close();
			}
        }
	}

	/**
	 * L� um {@link ByteBuffer} do NewIO como se fosse um InputStream, sem onerar.
	 * @author lucio
	 */
	public static class ByteBufferBackedInputStream extends InputStream{
		private ByteBuffer buf;
		public ByteBufferBackedInputStream( ByteBuffer buf){
			this.buf = buf;
		}
		public ByteBuffer getBuf() {
			return buf;
		}

		@Override
        public synchronized int read() throws IOException {
			if (!buf.hasRemaining()) {
				return -1;
			}
			return buf.get();
		}
		@Override
        public synchronized int read(byte[] bytes, int off, int len) throws IOException {
			len = Math.min(len, buf.remaining());
			buf.get(bytes, off, len);
			return len;
		}
		@Override
		public int available() throws IOException {
			return buf.remaining();
		}
	}

	/**
	 * Escreve em um {@link ByteBuffer} do NewIO como se fosse um OutputStream, sem onerar.
	 * @author lucio
	 */
	public static class ByteBufferBackedOutputStream extends OutputStream{
		private ByteBuffer buf;
		public ByteBufferBackedOutputStream(ByteBuffer buf){
			this.buf = buf;
		}
		public ByteBuffer getBuf() {
			return buf;
		}

		@Override
		public synchronized void write(int b) throws IOException {
			buf.put((byte) b);
		}
		@Override
		public synchronized void write(byte[] bytes, int off, int len) throws IOException {
			buf.put(bytes, off, len);
		}
	}

//	public static Blob clonaBlob(Blob origem) {
//		return clonaBlob(origem, 1)[0];
//	}
//
//	public static Blob[] clonaBlob(Blob origem, int n) {
//		Blob[] blobs = new Blob[n];
//		try {
//			InputStream stmOrigem = origem.getBinaryStream();
//			if ( (stmOrigem instanceof ISuporteObtencaoNovoStream)
//					&& ((ISuporteObtencaoNovoStream)stmOrigem).suportaObtencaoNovoStream()
//			) {
//				for (int i = 0; i < blobs.length; i++) {
//					blobs[i] = MArquivosBinariosSistema.getBlob( ((ISuporteObtencaoNovoStream)stmOrigem).getNewStream() );
//				}
//			} else {
//				TempOutputStream out = new TempOutputStream();
//				new BypassEncodeCodec(out).encodeFrom(stmOrigem); // leaky??
//				for (int i = 0; i < blobs.length; i++) {
//					blobs[i] = MArquivosBinariosSistema.getBlob(out.getInputStream());
//				}
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		return blobs;
//	}
//
//    @SuppressWarnings("resource")
//    public static File getResourceAsTempFile(String caminhoResource) throws IOException {
//        return getResourceAsTempFile(caminhoResource, UtilitariosArquivo.class.getClassLoader());
//    }
//
//	@SuppressWarnings("resource")
//	public static File getResourceAsTempFile(String caminhoResource, ClassLoader classloader) throws IOException {
//		String resource;
//		String[] split = caminhoResource.split("[//]");
//		if (split.length>1) {
//			resource = split[split.length-1];
//		} else {
//			resource = caminhoResource;
//		}
//
//		File result = TempOutputStream.getNewTempFile(extraiNomeAquivoSemExtensao(resource), getExtensaoSemPonto(resource));
//		OutputStream out = new FileOutputStream(result);
//		new BypassEncodeCodec(out ).encodeFrom(getResource(caminhoResource, classloader));
//		out.flush(); out.close();
//		result.deleteOnExit();
//		return result;
//	}

	/**
	 * @param arquivo
	 * @param sempreUsarSequencial se true, mesmo que "path/prefixoArquivo.extensao" nao exista, ele retornarah com o sequencial, no caso, 1: "path/prefixoArquivo1.extensao"
	 *
	 * @return
	 * um arquivo, no caminho path, no formato:
	 * "/[PATHINFORMADO]/[prefixoArquivo][Sequencial].[extensao]".
	 * Sendo o "sequencial" incrementado de forma que o arquivo retornado nao exista.
	 * Ex: path = "/home" ; prefixo = "meuArq"; extensao = "txt".
	 *     Se jah existe "/home/meuArq.txt" retorna "/home/meuArq1.txt"
	 *     Se jah existe "/home/meuArq1.txt" retorna "/home/meuArq2.txt" ...
	 *
	 */
	public static File getProximoNomeArquivoSequencialLivre(File arquivo, boolean sempreUsarSequencial) {
		return getProximoNomeArquivoSequencialLivre(arquivo, "", sempreUsarSequencial);
	}

	/**
	 * @param arquivo
	 * @param sufixoNomeArqCasoExista ex "_OLD" -> se arquivo jah existir (ex /home/meuArq.txt), retorna /home/meuArq_OLD1.txt
	 * @param sempreUsarSequencial se true, mesmo que "path/prefixoArquivo.extensao" nao exista, ele retornarah com o sequencial, no caso, 1: "path/prefixoArquivo1.extensao"
	 *
	 * @return
	 * um arquivo, no caminho path, no formato:
	 * "/[PATHINFORMADO]/[prefixoArquivo][Sequencial].[extensao]".
	 * Sendo o "sequencial" incrementado de forma que o arquivo retornado nao exista.
	 * Ex: path = "/home" ; prefixo = "meuArq"; extensao = "txt".
	 *     Se jah existe "/home/meuArq.txt" retorna "/home/meuArq1.txt"
	 *     Se jah existe "/home/meuArq1.txt" retorna "/home/meuArq2.txt" ...
	 *
	 */
	public static File getProximoNomeArquivoSequencialLivre(File arquivo, String sufixoNomeArqCasoExista, boolean sempreUsarSequencial) {
		File path = arquivo.getParentFile();
		String prefixoArquivo = extraiNomeAquivoSemExtensao( arquivo.getName() );
		String extensao = getExtensaoSemPonto(arquivo);
		return getProximoNomeArquivoSequencialLivre(path, prefixoArquivo, extensao, sufixoNomeArqCasoExista, sempreUsarSequencial);
	}

	/**
	 * @param path
	 * @param prefixoArquivo
	 * @param extensao
	 * @return um arquivo, no caminho path, no formato:
	 * "/[PATHINFORMADO]/[prefixoArquivo][Sequencial].[extensao]".
	 * Sendo o "sequencial" incrementado de forma que o arquivo retornado nao exista.
	 * Ex: path = "/home" ; prefixo = "meuArq"; extensao = "txt".
	 *     Se jah existe "/home/meuArq.txt" retorna "/home/meuArq1.txt"
	 *     Se jah existe "/home/meuArq1.txt" retorna "/home/meuArq2.txt" ...
	 */
	public static File getProximoNomeArquivoSequencialLivre(File path, String prefixoArquivo, String extensao) {
		return getProximoNomeArquivoSequencialLivre(path, prefixoArquivo, extensao, false);
	}

	/**
	 * @param path
	 * @param prefixoArquivo
	 * @param extensao
	 * @param sufixoNomeArqCasoExista
	 * @param sempreUsarSequencial se true, mesmo que "path/prefixoArquivo.extensao" nao exista, ele retornarah com o sequencial, no caso, 1: "path/prefixoArquivo1.extensao"
	 * @return um arquivo, no caminho path, no formato:
	 * "/[PATHINFORMADO]/[prefixoArquivo][Sequencial].[extensao]".
	 * Sendo o "sequencial" incrementado de forma que o arquivo retornado nao exista.
	 * Ex: path = "/home" ; prefixo = "meuArq"; extensao = "txt".
	 *     Se jah existe "/home/meuArq.txt" retorna "/home/meuArq1.txt"
	 *     Se jah existe "/home/meuArq1.txt" retorna "/home/meuArq2.txt" ...
	 */
	public static File getProximoNomeArquivoSequencialLivre(File path, String prefixoArquivo, String extensao, boolean sempreUsarSequencial) {
		return getProximoNomeArquivoSequencialLivre(path, prefixoArquivo, extensao, "", sempreUsarSequencial);
	}

	/**
	 * @param path
	 * @param prefixoArquivo
	 * @param extensao
	 * @param sufixoNomeArqCasoExista ex: se "_OLD"
	 *        path = "/home" ; prefixo = "meuArq"; extensao = "txt")
	 *        Se jah existe "/home/meuArq.txt" retorna "/home/meuArq_OLD1.txt"
	 *        Se jah existe "/home/meuArq_OLD1.txt" retorna "/home/meuArq_OLD2.txt"
	 * @param sempreUsarSequencial se true, mesmo que "path/prefixoArquivo.extensao" nao exista, ele retornarah com o sequencial, no caso, 1: "path/prefixoArquivo1.extensao"
	 * @return um arquivo, no caminho path, no formato:
	 * "/[PATHINFORMADO]/[prefixoArquivo][Sequencial].[extensao]".
	 * Sendo o "sequencial" incrementado de forma que o arquivo retornado nao exista.
	 * Ex: path = "/home" ; prefixo = "meuArq"; extensao = "txt".
	 *     Se jah existe "/home/meuArq.txt" retorna "/home/meuArq1.txt"
	 *     Se jah existe "/home/meuArq1.txt" retorna "/home/meuArq2.txt" ...
	 */
	public static File getProximoNomeArquivoSequencialLivre(File path, String prefixoArquivo, String extensao, String sufixoNomeArqCasoExista, boolean sempreUsarSequencial) {
		if (extensao!=null && extensao.startsWith(".")) {
			extensao = extensao.substring(1);
		}
		String seqInicial = (sempreUsarSequencial) ? "1" : "";
		File result = new File(path, prefixoArquivo + seqInicial + ( (extensao==null || extensao.isEmpty()) ? "" : "."+extensao) );
		int seq=1;
		while (result.exists()) {
			result = new File(path, prefixoArquivo + sufixoNomeArqCasoExista + seq + ( (extensao==null || extensao.isEmpty()) ? "" : "."+extensao) );
			seq++;
		}
		return result;
	}

//	public static void main4(String[] args) throws IOException {
//
////		{
////			File origem = new File("/home/91913837068/e-Safira_AD_Gestor_TRE/tmp/1529514775613-0/07473661795_0720100201500482_1.iso");
////			File destino = new File("/home/91913837068/e-Safira_AD_Gestor_TRE/07473661795_0720100201500482_1LUCIO.iso");
////			UtilitariosArquivo.moveArquivoOuPasta(origem, destino);
////		}
//
//		File arq = getProximoNomeArquivoSequencialLivre(new File("/home/88331423020/"), "teste", ".txt", true);
//		//arq.createNewFile();
//		System.out.println(arq);
//
//		try {
//			if (true) {
//				throw new Exception("teste nivel1", new IOException("teste nivel 2", new IOException("problema sem espa�o suficiente no disco")));
//			}
//		}catch (Exception e) {
//			System.out.println(UtilitariosExcecao.getMensagemUserFriendlyParaExcecao(e));
//			e.printStackTrace();
//		}
//	}


//	/**
//	 * @param pasta
//	 * @return uma lista com todos arquivos do diretorio, olhando subdiretorios tambem
//	 */
//	public static List<String> listaArquivosRecursivo(File pasta) {
//		List<String> arquivos = new LinkedList<String>();
//		for (File filho : pasta.listFiles()) {
//			arquivos.add(filho.getAbsolutePath());
//			if (filho.isDirectory()) {
//				arquivos.addAll(listaArquivosRecursivo(filho));
//			}
//		}
//		return arquivos;
//	}

//	/**
//	 * copia a origem no destino, dando flush...
//	 * @param origem
//	 * @param destino
//	 * @throws IOException
//	 */
//	@SuppressWarnings("resource")
//	public static void copiaStreams(InputStream origem, OutputStream destino) throws IOException {
//		new BypassDecodeCodec(origem).decodeTo(destino);
//		destino.flush();
//	}
//
//	/**
//	 * @param pathDiscoOuArquivo caminho que contem o disco passado, pode ser qualquer caminho, desde
//	 * @return nro de Bytes livres no disco passado
//	 * @throws IOException
//	 */
//	public static long getEspacoLivreDisco(File pathDiscoOuArquivo) throws IOException {
//		String pathDisco = pathDiscoOuArquivo.getAbsolutePath();
//		return FileSystemUtils.freeSpace(pathDisco);
//	}

//	/**
//	 * @return em Megabytes
//	 * @throws IOException
//	 */
//	public static long getEspacoLivreDiscoArqsTemporarios() throws IOException {
//		return getEspacoLivreDisco(getPastaDeArquivosTemporariosSistemaOperacional()) / 1024 / 1024;
//	}
//
//	/**
//	 * @return em Megabytes
//	 * @throws IOException
//	 */
//	public static long getEspacoLivreDiscoPastaSistema() throws IOException {
//		return getEspacoLivreDisco( FabricaUtilitarios.getPastaAplicacao() )  / 1024 / 1024;
//	}
//
//	public static void main(String[] args) {
//	    String cmdWindows = " O volume na unidade C n�o tem nome.\n" +
//	            " O N�mero de S�rie do Volume � 862D-AF1D\n" +
//	            "\n" +
//	            " Pasta de c:\\Users\\ligle\\Videos\n" +
//	            "\n" +
//	            "10/12/2019  17:10    <DIR>          .\n" +
//	            "10/12/2019  17:10    <DIR>          ..\n" +
//	            "25/11/2019  19:24    <DIR>          Captures\n" +
//	            "               0 arquivo(s)              0 bytes\n" +
//	            "               3 pasta(s)   42.759.860.224 bytes dispon�veis\n" +
//	            "";
//
//        List<String> lines = Arrays.asList( cmdWindows.split("[\n]") );
//
//
//	    {
//	        // now iterate over the lines we just read and find the LAST
//	        // non-empty line (the free space bytes should be in the last element
//	        // of the ArrayList anyway, but this will ensure it works even if it's
//	        // not, still assuming it is on the last non-blank line)
//	        String line = null;
//	        long bytes = -1;
//	        int i = lines.size() - 1;
//	        int bytesStart = 0;
//	        int bytesEnd = 0;
//	        outerLoop: while (i > 0) {
//	            line = lines.get(i);
//	            if (line.length() > 0) {
//	                // found it, so now read from the end of the line to find the
//	                // last numeric character on the line, then continue until we
//	                // find the first non-numeric character, and everything between
//	                // that and the last numeric character inclusive is our free
//	                // space bytes count
//	                int j = line.length() - 1;
//	                innerLoop1: while (j >= 0) {
//	                    char c = line.charAt(j);
//	                    if (Character.isDigit(c)) {
//	                      // found the last numeric character, this is the end of
//	                      // the free space bytes count
//	                      bytesEnd = j + 1;
//	                      break innerLoop1;
//	                    }
//	                    j--;
//	                }
//	                innerLoop2: while (j >= 0) {
//	                    char c = line.charAt(j);
//	                    if (!Character.isDigit(c) && c != ',' && c != '.') {
//	                      // found the next non-numeric character, this is the
//	                      // beginning of the free space bytes count
//	                      bytesStart = j + 1;
//	                      break innerLoop2;
//	                    }
//	                    j--;
//	                }
//	                break outerLoop;
//	            }
//	        }
//
//	        // remove commas and dots in the bytes count
//	        StringBuffer buf = new StringBuffer(line.substring(bytesStart, bytesEnd));
//	        for (int k = 0; k < buf.length(); k++) {
//	            if (buf.charAt(k) == ',' || buf.charAt(k) == '.') {
//	                buf.deleteCharAt(k--);
//	            }
//	        }
//	        bytes = Long.parseLong(buf.toString());
//	        System.out.println("Total bytes: "+bytes);
//	    }
//
//    }
//
//	public static boolean isExcecaoFaltaEspacoDisco(Throwable e) {
//		if (e==null) {
//			return false;
//		}
//
//		if (e instanceof IOException) {
//			switch (SistemaOperacional.getSistemaOperacionalAtual()) {
//			case WINDOWS :
//				if (e.getLocalizedMessage()!=null
//					&& (e.getLocalizedMessage().toLowerCase().contains("Espa�o insuficiente no disco".toLowerCase())
//						|| e.getLocalizedMessage().toLowerCase().contains("enough space on the disk".toLowerCase())
//					)
//				) {
//					return true;
//				}
//				//Espa�o insuficiente no disco
//				//Windows: There is not enough space on the disk
//			case LINUX : default :
//				if (e.getLocalizedMessage()!=null
//					&& (e.getLocalizedMessage().toLowerCase().contains("Not enough space".toLowerCase())
//						|| e.getLocalizedMessage().toLowerCase().contains("espa�o suficiente".toLowerCase())
//						|| e.getLocalizedMessage().toLowerCase().contains("space left on device".toLowerCase())
//						|| e.getLocalizedMessage().toLowerCase().contains("espa�o dispon�vel".toLowerCase())
//					)
//				) {
//					return true;
//				}
//				//Solaris/Linux?: Not enough space
//				//GCJ: No space left on device
//			}
//
//		}
//
//		//se chegou aki, nao eh erro de espaco em disco... verifica na causa, recursivamente...
//		Throwable causa = e.getCause();
//		return isExcecaoFaltaEspacoDisco(causa);
//	}
//

	private static File pastaArqsTemporariosSisOperacional = null;

	/**
	 * @return a pasta de arquivos temporario definida no sistema operacional
	 */
	public static File getPastaDeArquivosTemporariosSistemaOperacional() {
		if (pastaArqsTemporariosSisOperacional==null) {
			File f;
			try {
				//if running with security manager
				f = File.createTempFile("test", null);
				pastaArqsTemporariosSisOperacional = f.getParentFile();
				try {
					f.deleteOnExit(); f.delete();
				} catch (Exception e) {} //faz nada, jah temos a pasta temporaria...
			} catch (IOException e) {
				//if running w/o security manager
				String prop = System.getProperty("java.io.tmpdir");
				if (prop==null) {
					throw new RuntimeException("Sistema sem pasta tempor�ria definida!");
				}
				pastaArqsTemporariosSisOperacional = new File(prop);
			}
		}

		return pastaArqsTemporariosSisOperacional;
	}

//	public static File getPastaDeArquivosTemporariosJSerpro() {
//		File result = null;
//		if (!ConstantesGlobais.getSistemaEmTempoEdicao()) {
//			if (JSerproApp.getConfiguracoesApp()!=null) {
//				result = JSerproApp.getConfiguracoesApp().getPastaArquivosTemporarios();
//			}
//		}
//		if (result==null) {
//			result = UtilitariosArquivo.getPastaDeArquivosTemporariosSistemaOperacional();
//		}
//		if (!result.exists()) {
//			result.mkdirs();
//		}
//		return result;
//	}

	/**
	 * Pode parecer estranho, mas se f1=new File("c:\temp\..\x.txt") e f2=new File("c:\x.txt"), o f1.equals(f2) retorna false!
	 * @param f1
	 * @param f2
	 * @return
	 */
	public static boolean arquivosIguais(File f1, File f2) {
		if (f1==null) {
			return f2==null;
		}
		if (f2==null) {
			return f1==null;
		}

		String canPath1;
		String canPath2;
		try {
			canPath1 = f1.getCanonicalPath();
			canPath2 = f2.getCanonicalPath();
		} catch (IOException e) {
			new Exception("Erro ao obter canonical path para ["+f1.getAbsolutePath()+"] ou ["+f2.getAbsolutePath()+"]. Usando absolutepath...",e).printStackTrace();
			canPath1 = f1.getAbsolutePath();
			canPath2 = f2.getAbsolutePath();
		}
		return canPath1.equals(canPath2);
	}

	/**
	 * @param arqOuPastaContido
	 * @param pastaParaQueContem
	 * @return true se "arqOuPastaContido" est� contido em "pastaParaQueContem" (em um ou mais niveis)
	 */
	public static boolean arquivoContidoPasta(File arqOuPastaContido, File pastaParaQueContem) {
		if (arqOuPastaContido==null) {
			return false;
		}
		File arqAtu = arqOuPastaContido; //.getParentFile() -> vemos ele proprio, pois um dir "esta contido" nele mesmo...
		while (arqAtu!=null) {
			if (arquivosIguais(pastaParaQueContem, arqAtu)) {
				return true;
			}
			arqAtu = arqAtu.getParentFile();
		}
		return false;
	}

//	/**
//	 * @param arquivo
//	 * @return true se o arquivo est� contido em pasta temporaria...
//	 */
//	public static boolean arquivoContidoPastaTemporaria(File arquivo) {
//		return arquivoContidoPasta(arquivo, getPastaDeArquivosTemporariosJSerpro() )
//				|| arquivoContidoPasta(arquivo, getPastaDeArquivosTemporariosSistemaOperacional() );
//	}

    /**
     * Bug? do FileChooser, fc.getSelectedFile() nao indica o arquivo corretamente se o foco estah ainda no campo de arquivo.
     * Este metodo Seta o arquivo sendo digitado no FileChooser e retorna o arquivo corretamente
     * @return
     */
    public static File getArquivoSelecionadoFileChooser(JFileChooser fc) {
        if (fc.getUI() instanceof BasicFileChooserUI) {
        	BasicFileChooserUI ui = (BasicFileChooserUI) fc.getUI();
        	fc.setSelectedFile(new File(ui.getFileName()));
        }
        return new File(fc.getCurrentDirectory(), fc.getSelectedFile().getName());
    }

    /**
     * Faz o rename do "arquivo" para o "arqAposRename". Equivalente a arquivo.rename(arqAposRename);
     * Por�m, se n�o conseguiu renomear, tenta por ateh 3s fazer outras tentativas para renomear (pode ser que o arquivo desbloqueie para rename apos uma destas tentativas)
     * @param arquivo
     * @param arqAposRename
     * @return true caso o rename tenha sucesso (foi efetivado),
     *         false se nao conseguiu fazer o rename.
     */
    public static boolean renameComRetry(File arquivo, File arqAposRename) {
    	return renameComRetry(arquivo, arqAposRename, 3000);
    }

    /**
     * Faz o rename do "arquivo" para o "arqAposRename". Equivalente a arquivo.rename(arqAposRename);
     * Por�m, se n�o conseguiu renomear, tenta por 10 tentativas renomear (pode ser que o arquivo desbloqueie para rename apos uma destas tentativas)
     * @param arquivo
     * @param arqAposRename
     * @param qtdMsParaDarRetry tempo maximo (em ms) para ficar tentando renomear ateh conseguir.
     * @return true caso o rename tenha sucesso (foi efetivado),
     *         false se nao conseguiu fazer o rename (dentro do tempo maximo estipulado).
     */
    public static boolean renameComRetry(File arquivo, File arqAposRename, int qtdMsParaDarRetry) {
    	long msAntes = System.currentTimeMillis();
		do {
			if (arquivo.renameTo(arqAposRename)) {
				return true;
			}
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				break;
			}
		} while ( (System.currentTimeMillis()-msAntes) < qtdMsParaDarRetry);
		return false;
	}

//	/**
//	 * Lista (tipo um dir/w ou ls) os arquivos da pasta passada.
//	 * @param pasta
//	 * @param filenameFilter se <>null, so lista os arquivos que passam pelo Filenamefilter.
//	 * @param recursivo se true, entra nas subpastas.
//	 * @return
//	 */
//	public static String listaArquivosPasta(File pasta, FilenameFilter filenameFilter, boolean recursivo) {
//		return listaArquivosPasta(pasta, filenameFilter, recursivo, 0, true, true);
//	}
//	private static String listaArquivosPasta(File pasta, FilenameFilter filenameFilter, boolean recursivo, int nivel, boolean colocarData, boolean colocarTamanho) {
//		File[] arqs;
//		StringBuilder result = new StringBuilder();
//		if (filenameFilter==null) {
//			arqs = pasta.listFiles();
//		} else {
//			arqs = pasta.listFiles(filenameFilter);
//		}
//		if (arqs==null) {
//			arqs=new File[] {};
//		}
//		List<File> arqsLista = Arrays.asList(arqs);
//		Collections.sort(arqsLista, new Comparator<File>() {
//			@Override
//			public int compare(File o1, File o2) {
//				return o1.getName().toLowerCase().compareTo(o2.getName().toLowerCase());
//			}
//		});
//		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
//		DecimalFormat nf = new DecimalFormat("999999999999");
//		for (File f : arqsLista) { //primeiro os arquivos...
//			if (!f.isDirectory()) {
//				result.append(UtilitariosString.repeteString(" ", nivel*4, ""));
//				if (colocarData) {
//					result.append( "["+df.format(new Date(f.lastModified())) +"]").append(" ");
//				}
//				if (colocarTamanho) {
//					result.append( "["+nf.format( f.length()/1024) +"kb]").append(" ");
//				}
//				result.append(f.getName());
//				result.append('\n');
//			}
//		}
//		for (File f : arqsLista) { //depois as pastas...
//			if (f.isDirectory()) {
//				result.append(UtilitariosString.repeteString(" ", nivel*4, ""))
//				.append("Pasta: ").append(f.getName());
//				result.append('\n');
//				if (recursivo) {
//					result.append( listaArquivosPasta(f, filenameFilter, true, nivel+1, colocarData, colocarTamanho) );
//				}
//			}
//		}
//
//		return result.toString();
//	}
//
//	public static void main7(String[] args) throws IOException {
//
//		System.out.println("bytes = "+getEspacoLivreDisco(new File("c:\\users\\ligle\\e-Safira_AD_Gestor_DES\\")));
//		System.out.println("Mbytes = "+getEspacoLivreDiscoPastaSistema());
//		System.out.println("Mbytes temp = "+getEspacoLivreDiscoArqsTemporarios());
//		System.out.println("Formatado = "+UtilitariosString.formataNumeroBytes( getEspacoLivreDiscoPastaSistema() * 1024 * 1024 ));
//
//		if (true) {
//			return;
//		}
//		FilenameFilter fn = new FilenameFilter() {
//			@Override
//			public boolean accept(File dir, String name) {
//				return !dir.getName().contains("ajuda");
//			}
//		};
//		System.out.println(listaArquivosPasta(new File("/home/91913837068/e-Safira_AD_Gestor_DES"), fn, true));
//
//		FileInputStream in = new FileInputStream("/home/91913837068/e-Safira_AD_Gestor_DES/build.properties");
//		FileChannel fcout = new FileOutputStream("/home/91913837068/e-Safira_AD_Gestor_DES/build.properties_COPIADO").getChannel();
//		copiaArquivo(in, fcout);
//		fcout.close();
//
//		FileInputStream in2 = new FileInputStream("/home/91913837068/e-Safira_AD_Gestor_DES/eSafira2.0Final_comp ultra.eBK");
//		FileChannel fcout2 = new FileOutputStream("/home/91913837068/e-Safira_AD_Gestor_DES/eSafira2.0Final_comp ultra.eBK_COPIADO").getChannel();
//		copiaArquivo(in2, fcout2);
//		fcout2.close();
//	}

	/**
	 * Dado o arquivo passado, testa se o mesmo representa um arquivo .ZIP (pelo cabecalho... nao faz teste de integridade geral).
	 * @param arq
	 * @return
	 * @throws IOException
	 */
	public static boolean isZipFile(File arq) throws IOException {
		byte[] headerZip = new byte[]{0x50, 0x4B, 0x03, 0x04, 0x14, 0x00}; //, 0x08, 0x08, 0x08, 0x00};
		byte[] headerArquivo = new byte[headerZip.length];
		BufferedInputStream isArq = new BufferedInputStream(new FileInputStream(arq), 100);
		try {
			if (isArq.read(headerArquivo)<headerZip.length) {
				return false;
			}
			if (! Arrays.equals(headerZip, headerArquivo) ) {
				return false;
			}
			return true;
		} finally {
			isArq.close();
		}
	}

	/**
	 * Se arquivo nao existente, cria eventualmente as pastas onde est� contido e cria um arquivo com 0 bytes.
	 * @param arquivo
	 * @throws IOException
	 */
	public static void criaArquivoSeNaoExistente(File arquivo) throws IOException {
		if (!arquivo.exists()) {
			arquivo.getParentFile().mkdirs();
			FileOutputStream os = new FileOutputStream(arquivo);
			os.flush();
			os.close();
		}
	}
	
	public static Date getMaxDataModifiedArquivos(List<File> arquivos) {
		long max=0;
		for (File f : arquivos) {
			if (max<f.lastModified()) {
				max = f.lastModified();
			}
		}
		return new Date(max);
	}
	
	/**
	 * @param arqOrigem ex: c:\temp\origem\pastaOrigem1\pasta2\arqOrigem1.txt
	 * @param pastaDestino ex: c:\destino\
	 * @param pastaComumOrigem ex: c:\temp\origem\ | assim, o destino final serah: c:\destino\pastaOrigem1\pasta2\arqOrigem1.txt
	 * @throws IOException 
	 */
	public static void moveArquivoOuPastaPreservandoPastaOrigem(File arqOrigem, File pastaDestino,
			File pastaComumOrigem
	) throws IOException {
		File pastaDestinoFinal;
		if (pastaComumOrigem==null) {
			pastaDestinoFinal = pastaDestino;
		} else {
			pastaDestinoFinal = new File( pastaDestino, 
					arqOrigem.getParentFile().getAbsolutePath().replaceFirst("^"+Pattern.quote(pastaComumOrigem.getAbsolutePath()) , "") );
		}
		//moveArquivoOuPasta(arqOrigem, pastaDestinoFinal); //FIXME! NAO ESTAH MOVENDO CERTO ISSO! ESTAH CRIANDO UM ARQUIVO com o nome da pasta!
		System.out.println("movendo "+arqOrigem.getAbsolutePath()+ " ---> "+ (new File(pastaDestinoFinal, arqOrigem.getName()).getAbsolutePath()) );
	}

	/**
	 * @param numbytes
	 * @return algo como abaixo.
     * Example output:
     *                              SI     BINARY
     * 
     *                   0:        0 B        0 B
     *                  27:       27 B       27 B
     *                 999:      999 B      999 B
     *                1000:     1.0 kB     1000 B
     *                1023:     1.0 kB     1023 B
     *                1024:     1.0 kB    1.0 KiB
     *               1728:     1.7 kB    1.7 KiB
     *              110592:   110.6 kB  108.0 KiB
     *             7077888:     7.1 MB    6.8 MiB
     *           452984832:   453.0 MB  432.0 MiB
     *         28991029248:    29.0 GB   27.0 GiB
     *       1855425871872:     1.9 TB    1.7 TiB
     * 9223372036854775807:     9.2 EB    8.0 EiB   (Long.MAX_VALUE)
	 */
	public static String formataNumeroBytes(long numbytes) {
		return formataNumeroBytes(numbytes, false);
	}
    
	/**
	 * @param numbytes
	 * @return algo como abaixo.
     * Example output:
     *                              SI     BINARY
     * 
     *                   0:        0 B        0 B
     *                  27:       27 B       27 B
     *                 999:      999 B      999 B
     *                1000:     1.0 kB     1000 B
     *                1023:     1.0 kB     1023 B
     *                1024:     1.0 kB    1.0 KiB
     *               1728:     1.7 kB    1.7 KiB
     *              110592:   110.6 kB  108.0 KiB
     *             7077888:     7.1 MB    6.8 MiB
     *           452984832:   453.0 MB  432.0 MiB
     *         28991029248:    29.0 GB   27.0 GiB
     *       1855425871872:     1.9 TB    1.7 TiB
     * 9223372036854775807:     9.2 EB    8.0 EiB   (Long.MAX_VALUE)
	 */
	public static String formataNumeroBytes(long numbytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (numbytes < unit) {
			return numbytes + " B";
		}
        int exp = (int) (Math.log(numbytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp-1) + (si ? "" : "i");
        return String.format("%.1f %sB", numbytes / Math.pow(unit, exp), pre);
	}

	
	private static Metadata readVideoMetadata(File file) throws IOException, ImageProcessingException {
		try (InputStream inputStream = new BufferedInputStream(new FileInputStream(file))) {
			Metadata metadata = new Metadata();
			Mp4Reader.extract(inputStream, new Mp4BoxHandler(metadata));
			new FileSystemMetadataReader().read(file, metadata);
			
			return metadata;
		}

//		Metadata metadata = ImageMetadataReader.readMetadata(file);
//
//		for (Directory directory : metadata.getDirectories()) {
//		    for (Tag tag : directory.getTags()) {
//		        System.out.format("[%s] - %s = %s",
//		            directory.getName(), tag.getTagName(), tag.getDescription());
//		    }
//		    if (directory.hasErrors()) {
//		        for (String error : directory.getErrors()) {
//		            System.err.format("ERROR: %s", error);
//		        }
//		    }
//		}
//		
//		Metadata metadata = new Metadata();
//		InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
//		RandomAccessReader reader = new RandomAccessStreamReader(inputStream);
//		new ExifReader().extract(reader, metadata);
//		return metadata;
	}

	/**
	 * Teste ExIf de arquivo de video
	 * @param args
	 * @throws IOException
	 * @throws XMPException
	 * @throws ImageProcessingException
	 */
	public static void main(String[] args) throws IOException, XMPException, ImageProcessingException {
		//File arq = new File("C:\\Users\\s919138370\\Documents\\SerproHomeoffice\\git\\esafira-sitio\\htdocs_ssl\\PapeisTrabalho\\MaterialVideo\\1- e-safira-seort  - objetivos, conceitos, instalação.mp4");
		File arq = new File("C:\\Users\\s919138370\\Documents\\SerproHomeoffice\\eSafiraEmDatas_sincronizar\\_HOMCD\\HOMCD_Wildfly\\Apache\\papeistrabalho\\MaterialVideo\\Aduaneiro 1 - Exercício 1_segundo treinamento para Aduana.mp4");
		Metadata metadata = readVideoMetadata(arq);
		for (XmpDirectory xmpDirectory : metadata.getDirectoriesOfType(XmpDirectory.class)) {

		    // Usually with metadata-extractor, you iterate a directory's tags. However XMP has
		    // a complex structure with many potentially unknown properties. This doesn't map
		    // well to metadata-extractor's directory-and-tag model.
		    //
		    // If you need to use XMP data, access the XMPMeta object directly.
		    XMPMeta xmpMeta = xmpDirectory.getXMPMeta();

		    // Iterate XMP properties
		    XMPIterator itr = xmpMeta.iterator();
		    while (itr.hasNext()) {
		        XMPPropertyInfo property = (XMPPropertyInfo) itr.next();

		        // Print details of the property
		        System.out.println(property.getPath() + ": " + property.getValue());
		    }
		}

		for (Directory directory : metadata.getDirectories()) {
		    for (Tag tag : directory.getTags()) {
		        System.out.format("[%s] - %s = %s",
		            directory.getName(), tag.getTagName(), tag.getDescription());
		        System.out.println("  -> "+tag.getTagType() + "(CLASSE="+directory.getClass().getSimpleName()+")"  );
		    }
		    if (directory.hasErrors()) {
		        for (String error : directory.getErrors()) {
		            System.err.format("ERROR: %s", error);
		        }
		    }
		}
		//System.out.println(metadata);
		
		System.out.println("\n\n\n\n");
		

		Mp4Directory mp4Dir = metadata.getFirstDirectoryOfType(Mp4Directory.class);
		
		Map<Integer, Tag> mapTags = mp4Dir.getTags().stream().collect(
				Collectors.toMap( tag -> tag.getTagType(), tag -> tag)
				);
		
		System.out.println("Duracao = "+
				mapTags.get( Mp4Directory.TAG_DURATION_SECONDS ).getDescription()
			);
		
		
		
//		InputStream fin = new FileInputStream(arq);
		//fout = new FileOutputStream(Fm_filePathOut);


//		List<com.icafe4j.image.meta.Metadata> metaList = new ArrayList<com.icafe4j.image.meta.Metadata>();
//		metaList.add(populateExif(JpegExif.class));
		
	}
//	private com.icafe4j.image.meta.exif.Exif populateExif(Class<?> exifClass) throws IOException {
//
//		com.icafe4j.image.meta.exif.Exif exif = new com.icafe4j.image.meta.jpeg.JpegExif();
//
//		exif.addImageField(com.icafe4j.image.meta.exif.ExifTag.WINDOWS_XP_AUTHOR, FieldType.WINDOWSXP, "Toto");
//		exif.addImageField(com.icafe4j.image.meta.exif.ExifTag.WINDOWS_XP_KEYWORDS, FieldType.WINDOWSXP, "Copyright;Authorbisou");
//		// Insert ThumbNailIFD
//		// Since we don't provide thumbnail image, it will be created later from the input stream
//		exif.setThumbnailRequired(true);
//	}
	
}