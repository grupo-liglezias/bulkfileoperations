package iglezias.util;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Pattern;

public class UtilsFFMpeg {
	public static final boolean USAR_NVIDIA=false;
	
	/**
	 * Para manter os metadados (-map_metadata 0) : https://video.stackexchange.com/questions/23741/how-to-prevent-ffmpeg-from-dropping-metadata
	 * ffmpeg -i a.MOV -map_metadata 0 -c copy c.MOV	
	 */
	
	/**
	 * From BulkFileOperations
	 *	%1 = path completo arquivo
	 *  %2 = nome do arquivo
	 *  %3 = pasta destino
	 *  %4 = path do arquivo RELATIVO 'a pasta base 
	 */
	
	//public static final String COMANDO = "ffmpeg -hwaccel nvdec -hwaccel_output_format nvenc -i \"%1\" -vf yadif -c:v h264_nvenc -preset slow -crf 19 -c:a aac -b:a 64k \"%1.mp4\"";
	//public static final String COMANDO = "ffmpeg -hwaccel nvdec -hwaccel_output_format nvenc -i \"%1\" -vf yadif -c:v h264_nvenc -b:a 64k \"%3%2.mp4\"";
//	public static final String COMANDO = "ffmpeg -hwaccel nvdec -hwaccel_output_format nvenc -i \"%1\" "
//			+ "-vf yadif -c:v h264_nvenc -b:a 64k -af \"pan=mono|c0=FL\" \"%3%4%2.mp4\""; //BEM rapido, USA a GPU, mas quase nao compacta
	
	/**
	 * https://superuser.com/questions/639402/convert-specific-video-and-audio-track-only-with-ffmpeg
	 * -map 0:v:1 = faixa video 2
	 * -map 0:a:0 = faixa audio 1
	 * -map 0:a:1 = faixa audio 2 
	 */
	public static final String COMANDO_SO_FAIXA2 = "ffmpeg "+(USAR_NVIDIA?"-hwaccel nvdec -hwaccel_output_format nvenc":"")+" -i \"%1\" -map_metadata 0 "
			+ "-vf yadif -c:v libx265 -crf 29 -map 0:v:0 -map 0:a:1 \"%3%4%2.mp4\""; //BEM lento, nao usa GPU, mas compacta BEM. OBS: NAO RODA em equipamentos mais antigos (codec mais recente). "crf X" qnto maior X, maior a compressao. 24a30 pode ser bom
	
	
	
	/**
	 *  -af "pan=mono|c0=FL" = canal esquerdo audio 
	 *  -af "pan=mono|c0=FR" = canal direito audio
	 * BEM lento, nao usa GPU, mas compacta BEM. OBS: NAO RODA em equipamentos mais antigos (codec mais recente)."crf X" qnto maior X, maior a compressao. 24a30 pode ser bom	 */
	public static final String COMANDO_SO_CANAL_ESQUERDO = "ffmpeg "+(USAR_NVIDIA?"-hwaccel nvdec -hwaccel_output_format nvenc":"")+" -i \"%1\" -map_metadata 0 "
			+ "-vf yadif -c:v libx265 -crf 29 -af \"pan=mono|c0=FL\" \"%3%4%2.mp4\""; 
	
	/**	 * BEM lento, nao usa GPU, mas compacta BEM. OBS: NAO RODA em equipamentos mais antigos (codec mais recente). "crf X" qnto maior X, maior a compressao. 24a30 pode ser bom	 */
	public static final String COMANDO_AUDIO_INTEIRO_ALTACOMPRESSAO = "ffmpeg "+(USAR_NVIDIA?"-hwaccel nvdec -hwaccel_output_format nvenc":"")+" -i \"%1\" -map_metadata 0 "
			+ "-vf yadif -c:v libx265 -crf 29 \"%3%4%2.mp4\"";
	
	/**	 * BEM lento, nao usa GPU, mas compacta BEM. OBS: NAO RODA em equipamentos mais antigos (codec mais recente). "crf X" qnto maior X, maior a compressao. 24a30 pode ser bom	 */
	public static final String COMANDO_AUDIO_INTEIRO_MEDIACOMPRESSAO = "ffmpeg "+(USAR_NVIDIA?"-hwaccel nvdec -hwaccel_output_format nvenc":"")+" -i \"%1\" -map_metadata 0 "
			+ "-vf yadif -c:v libx265 -crf 26 \"%3%4%2.mp4\"";
	
	/** BEM lento, nao usa GPU, mas compacta BEM. OBS: NAO RODA em equipamentos mais antigos (codec mais recente). "crf X" qnto maior X, maior a compressao. 24a30 pode ser bom  */
	public static final String COMANDO_AUDIO_INTEIRO_BAIXACOMPRESSAO = "ffmpeg "+(USAR_NVIDIA?"-hwaccel nvdec -hwaccel_output_format nvenc":"")+" -i \"%1\" -map_metadata 0 "
			+ "-vf yadif -c:v libx265 -crf 23 \"%3%4%2.mp4\"";
	
	public static final String COMANDO_EXTRAI_AUDIO_PRIM_FAIXA = "ffmpeg "+(USAR_NVIDIA?"-hwaccel nvdec -hwaccel_output_format nvenc":"")+"-i \"%1\" -map 0:a:0 -q:a 0 "
			+ " \"%3%4%2.mp3\"";
	
	/**
	 * https://superuser.com/questions/578321/how-to-rotate-a-video-180-with-ffmpeg
	 * Rotaciona 180 graus o video e o comprime
	 * @deprecated TODO: FAzer esse
	 */
	public static final String COMANDO_AUDIO_INTEIRO_ALTACOMPRESSAO_ROT180graus = 
			"TODO: FAZER ISSOffmpeg -hwaccel nvdec -hwaccel_output_format nvenc -i \"%1\" -map_metadata 0 "
					+ "-vf yadif -c:v libx265 -crf 29 \"%3%4%2.mp4\"";
	
	/**
	 * https://superuser.com/questions/578321/how-to-rotate-a-video-180-with-ffmpeg
	 * Rotaciona 180 graus o video
	 */
	public static final String COMANDO_ROT180graus = 
			"ffmpeg -i \"%1\" -vf \"rotate=180*(PI/180),format=yuv420p\" " + 
			"-metadata:s:v rotate=0 -codec:v libx264 -codec:a copy \"%3%4%2.mp4\"";


	//ffmpeg -ss 00:00:00 -t 00:00:20 -i Minions.2015.720p.WEB-DL.Dual-WOLVERDONFILMES.COM.mkv -map 0 -map_metadata -1 -c:v copy -c:a copy -map_chapters -1 output.mkv
	public static final String COMANDO_REMOVER_CHAPTERS = 
			"ffmpeg -i \"%1\"  " 
			+ "-map 0 -map_metadata -1 -c:v copy -c:a copy -map_chapters -1 \"%3%4%2.mp4\"";
	
	
	/**
	 * https://stackoverflow.com/questions/58205533/how-to-create-a-ffmpeg-vidstab-batch-script-windows
	 * ffmpeg -i DSCF0229.MOV -vf vidstabdetect=shakiness=5:accuracy=15:stepsize=6:mincontrast=0.3:show=2 -y dummy.mp4

ffmpeg -i DSCF0229.MOV -vf scale=trunc((iw*1.15)/2)*2:trunc(ow/a/2)*2 -y scaled.mp4

ffmpeg -i scaled.mp4 -vf vidstabtransform=smoothing=30:input="transforms.trf":interpol=linear:crop=black:zoom=0:optzoom=1,unsharp=5:5:0.8:3:3:0.4 -y DSCF0229stabilized.mp4

https://www.imakewebsites.ca/posts/stabilizing-gopro-video-ffmpeg-vidstab/
https://www.youtube.com/watch?v=LvCjmg5Onss
https://www.imakewebsites.ca/posts/stabilizing-gopro-video-ffmpeg-vidstab/
**--> https://rainnic.altervista.org/en/how-stabilize-video-using-ffmpeg-and-vidstab


	 */
	//public static final String COMANDO_DETECTA_ESTABILIZACAO = "ffmpeg -i \"%1\" -map_metadata 0 -vf vidstabdetect=shakiness=5:accuracy=15:stepsize=6:mincontrast=0.3:show=2 -y \"%3%4%2.mp4\"";
	public static final String COMANDO_DETECTA_ESTABILIZACAO_1 = "ffmpeg -i \"%1\" -map_metadata 0 -vf vidstabdetect=shakiness=5:accuracy=15:stepsize=6:mincontrast=0.3:show=2 -y \"%3%4%2.mp4\"";
	public static final String COMANDO_DETECTA_ESTABILIZACAO_2 = "ffmpeg -i \"%1\" -map_metadata 0 -vf scale=trunc((iw*1.15)/2)*2:trunc(ow/a/2)*2 -y \"%3%4%2.mp4\"";
	public static final String COMANDO_DETECTA_ESTABILIZACAO_3 = "ffmpeg -i \"%1\" -map_metadata 0 -vf vidstabtransform=smoothing=30:input=\"transforms\":interpol=linear:crop=black:zoom=0:optzoom=1,unsharp=5:5:0.8:3:3:0.4 -y \"%3%4%2.mp4\"";	
	
	
	
	public static final String COMANDO_CODIFICACAO_GPU = "ffmpeg -hwaccel nvdec -hwaccel_output_format nvenc -i \"%1\" -map_metadata 0 "
			+ "-vf yadif -c:v h264_nvenc -b:a 32k \"%3%4%2.mp4\""; //BEM rapido, USA a GPU, mas quase nao compacta
	
	/**
	 * Extrai apenas os 5 primeiros minutos de aula01.mp4 para aula01a.mp4
	 * ffmpeg -ss 00:00:00 -i aula01.mp4 -t 00:05:00 -acodec copy -vcodec copy aula01a.mp4
	 * 
	 * https://trac.ffmpeg.org/wiki/Encode/H.264
	 * Preset
	 * 		A preset is a collection of options that will provide a certain encoding speed to compression ratio. A slower preset will provide better compression (compression is quality per filesize). This means that, for example, if you target a certain file size or constant bit rate, you will achieve better quality with a slower preset. Similarly, for constant quality encoding, you will simply save bitrate by choosing a slower preset.
	 * 		
	 * 		Use the slowest preset that you have patience for. The available presets in descending order of speed are:
	 * 		
	 * 		ultrafast
	 * 		superfast
	 * 		veryfast
	 * 		faster
	 * 		fast
	 * 		medium – default preset
	 * 		slow
	 * 		slower
	 * 		veryslow
	 * 		placebo – ignore this as it is not useful (see FAQ)
	 * https://stackoverflow.com/questions/68463989/ffmpeg-export-codec-not-supported-by-samsung
	 * 
	 * okCorrigido_TODO: se tiver 2 faixas de audio, fica soh a primeira nessa codificacao
	 */
	public static final String COMANDO_CODIFICACAO_PARA_TVS_ANTIGAS = //nao usa o x265 q as tvs velhas nao entendem... usa o x264
			//"ffmpeg -hwaccel nvdec -i \"%1\" -map_metadata 0 -c:v libx264 -preset slow -crf 26 -profile:v high -level:v 4.1 -pix_fmt yuv420p -c:a aac \"%3%4%2\"";
			"ffmpeg -hwaccel nvdec -i \"%1\" -map 0:v:0 "//-map_metadata 0 -map 0:v?"
			+ "-map 0:a "
			+ "-c:v libx264 "
			+ "-preset slow -crf 26 -profile:v high "
			+ "-level:v 4.1 -pix_fmt yuv420p "
			+ "-c:a copy "
			+ "\"%3%4%2\"";
//"ffmpeg -ss 00:00:00 -i \"%1\" -map_metadata 0 -t 00:01:00 -c:v libx264 -preset medium -crf 23 -profile:v high -level:v 4.1 -pix_fmt yuv420p -c:a aac \"%3%4%2.mp4\"";
//			"ffmpeg -f concat -safe 0 -i \"%1\" -map_metadata 0 -filter_complex \"[0:v]setpts={1/speed}*PTS,format=yuv420p[v];[0:a]volume={volume},atempo={speed}[a]\" "
//			+ "-map \"[v]\" -map \"[a]\" -y \"%3%4%2.mp4\"";
	
	
//			"ffmpeg -hwaccel nvdec -hwaccel_output_format nvenc -i \"%1\" -map_metadata 0 "
//			+ "-vf yadif -c:v yuv420p -b:a 64k \"%3%4%2.mp4\""; //BEM rapido, USA a GPU, mas quase nao compacta
//			"ffmpeg -f concat -safe 0 -i \"%1\" -map_metadata 0 -filter_complex \"[0:v]setpts={1/speed}*PTS,format=yuv420p[v];[0:a]volume={volume},atempo={speed}[a]\" "
//			+ "-map \"[v]\" -map \"[a]\" -y \"%3%4%2.mp4\"";
		

	/**
	 * https://superuser.com/questions/1041858/converting-5-1-audio-to-stereo-and-keeping-both-tracks?noredirect=1&lq=1
	 * 
	 * https://ffmpeg.org/ffmpeg.html#Stream-specifiers-1
	 * ffmpeg -i multichannel.mxf -map 0:v:0 -map 0:a:0 -map 0:a:0 
	 *                            -c:a:0 ac3 -b:a:0 640k -ac:a:1 2 
	 *                            -c:a:1 aac -b:2 128k 
	 *                            out.mp4
	 *   In the above example, a multichannel audio stream is mapped twice for output. 
	 *   The first instance is encoded with codec ac3 and bitrate 640k. 
	 *   The second instance is downmixed to 2 channels and encoded with codec aac. 
	 *   A bitrate of 128k is specified for it using absolute index of the output stream.
	 */
	public static final String COMANDO_AUDIO_51_TO_STEREO_BOTH_AUDIO_TRACKS = 
			"ffmpeg -i \"%1\" -map_metadata 0 -map 0:0 -map 0:1 -map 0:1 " //4 streams, sendo a primeira o video, a 2e3 mapeando pra mesma stream da origem,
			+ " -map 0:2? "
			+ " -map 0:3? "
			+ " -c:v copy " 
			+ " -c:a:0 aac -b:a:0 192k -ac 2 "
			+ " -c:a:1 copy "
			+ " -c:a:3 copy "
			//+ " -t 00:05:00 "
			+ " -metadata:s:a:0 title=\"pt-br_stereo\" "
			+ "  \"%3%4%2\"";
/*
 	"ffmpeg -i \"%1\" -map_metadata 0 -map 0:0 -map 0:1 -map 0:1 "
			+ " -c:v copy " 
			+ " -c:a:0 aac -b:a:0 192k -ac 2 "
			+ " -c:a:1 copy  \"%3%4%2\"";
	
	/**
	 * -vf \"transpose=2\"     ===> rotacao 90graus anti-clockwise
	 * https://ostechnix.com/how-to-rotate-videos-using-ffmpeg-from-commandline/#:~:text=FFMpeg%20has%20a%20feature%20called,flip%20them%20vertically%20and%20horizontally.&text=Here%2C%20transpose%3D1%20parameter%20instructs,video%20by%2090%20degrees%20clockwise.
	 */
	public static final String COMANDO_AUDIO_INTEIRO_ALTACOMPRESSAO_SEM_SOM = "ffmpeg "+(USAR_NVIDIA?"-hwaccel nvdec -hwaccel_output_format nvenc":"")+" -i \"%1\" -map_metadata 0 "
			+ "-vf yadif -vf \"transpose=2\" -an -c:v libx265 -crf 29 \"%3%4%2.mp4\""; //BEM lento, nao usa GPU, mas compacta BEM. OBS: NAO RODA em equipamentos mais antigos (codec mais recente). "crf X" qnto maior X, maior a compressao. 24a30 pode ser bom

	
	/**
	 * https://stackoverflow.com/questions/52582215/commands-to-cut-videos-in-half-horizontally-or-vertically-and-rejoin-them-later
	 * ffmpeg -i input -filter_complex "[0]crop=iw/2:ih:0:0[left];[0]crop=iw/2:ih:ow:0[right]" -map "[left]" left.mp4 -map "[right]" right.mp4
	 */
	public static final String nao_funciona_COMANDO_REMOVE_3D_CORTA_METADE_HORIZONTAL = "ffmpeg -i \"%1\" -c:a copy -filter_complex \"[0]crop=iw/2:ih:0:0[left]\" -map \"[left]\" \"%3%4%2.mp4\"";
	
	//ffmpeg -ss 00:00:00 -t 00:00:20 -i Meu.Malvado.Favorito.2.2013.1080p.3D.HSBS.Pt-BR.x264.mkv -vf "stereo3d=sbs2l:ml" -c:v libx264 -c:a aac -strict experimental output_2d.mkv
	public static final String COMANDO_REMOVE_3D_CORTA_METADE_HORIZONTAL = "ffmpeg -i \"%1\" -vf \"stereo3d=sbs2l:ml\" -c:v libx264 -c:a aac -strict experimental \"%3%4%2.mp4\"";
	
	//fmpeg -i input.mp4 -vf "scale=1280:720:force_original_aspect_ratio=decrease,pad=1280:720:(ow-iw)/2:(oh-ih)/2" -c:v libx264 -c:a copy output_720p.mp4
	//ffmpeg -i input.mp4 -vf "scale=1280:720" -c:v libx264 -c:a copy output_stretched.mp4
	public static final String COMANDO_PERSPECTIVA_16_POR_9_WIDE = 
			"ffmpeg -i \"%1\" -vf \"scale=1280:720\" -c:v libx264 -c:a copy \"%3%4%2.mp4\"";
	
	
	/**
	 * @param iniSegundos
	 * @param qtdSegundos
	 * @return algo como -ss 00:00:00 -t 00:01:00 para tempo=60s
	 */
	public static String getLimiteTempoFFMPEG(int iniSegundos, int qtdSegundos) {
		String strTimeIni = getStringTime(iniSegundos);
		String strTimeQtdTotal = getStringTime(qtdSegundos);
		
		return " -ss "+strTimeIni+" -t "+strTimeQtdTotal+" ";
	}

	/**
	 * @param qtdSegundos
	 * @return ex: 72s -> "00:01:12" 
	 */
	public static String getStringTime(int qtdSegundos) {
		int hor = qtdSegundos / (60 * 60);
		int min = (qtdSegundos / 60) - (60*hor);
		int seg = qtdSegundos - (60*60*hor) - (60*min);
		DecimalFormat fmt = new DecimalFormat("00");
		String strTimeQtdTotal = fmt.format(hor)+":"+fmt.format(min)+":"+fmt.format(seg);
		return strTimeQtdTotal;
	}
	
	public static String adicionaLimiteTempoComandoFFMPEG(String comando, int iniSegundos, int qtdSegundos) {
		//String[] strs = comando.split(Pattern.quote("-i"));
		int primParam = comando.indexOf("-");
		return comando.substring(0, primParam)+ getLimiteTempoFFMPEG(iniSegundos, qtdSegundos) 
				+ comando.substring(primParam); 
	}
	
	public static void main(String[] args) {
		System.out.println(adicionaLimiteTempoComandoFFMPEG("ffmpeg -i \"%1\" -map_metadata 0 -c:v libx264 -preset medium -crf 23 -profile:v high -level:v 4.1 -pix_fmt yuv420p -c:a aac \"%3%4%2.mp4\"", 0,5));
		System.out.println(getLimiteTempoFFMPEG(0,65));
		System.out.println(getLimiteTempoFFMPEG(0,125));
		System.out.println(getLimiteTempoFFMPEG(0,3671));
		System.out.println(getLimiteTempoFFMPEG(0,3599));
	}
	
	
	/**
	 * A partir da linha de inicio do retorno do ffmpeg, retorna a duracao do video
	 * ex: "  Duration: 00:30:25.66, start: 20151.479433, bitrate: 1767 kb/s", retorna data "Wed Jan 01 00:30:25"
	 * @param linha
	 * @return null caso nao seja uma linha deste tipo
	 */
	public static Date getDurationFromString(String linha) {
		//  Duration: 00:30:25.66, start: 20151.479433, bitrate: 1767 kb/s
		if (Utils.substringEx(linha,15).contains("Duration: ")) {
			try {
				String[] sp1 = linha.split(Pattern.quote("Duration: "));
				String[] sp2 = sp1[1].split(Pattern.quote(".")); //00:30:25
				String[] sp3 = sp2[0].split(Pattern.quote(":")); 
				return new GregorianCalendar(2020,0,1, Integer.parseInt(sp3[0]), Integer.parseInt(sp3[1]), Integer.parseInt(sp3[2])).getTime();
			} catch (Exception e) {
				return null;
			}
		} else {
			return null;
		}
	}

	/**
	 * A partir da linha de inicio do retorno do ffmpeg, retorna o tempo do video do corrente progresso da conversao
	 * ex: "frame=45966 fps=124 q=36.1 size=   70144kB time=00:25:33.05 bitrate= 374.8kbits/s dup=28 drop=0 speed=4.12x", retorna data "Wed Jan 01 00:25:33"
	 * @param linha
	 * @return null caso nao seja uma linha deste tipo
	 */
	public static Date getCurrentProcTimeFromString(String linha) {
		//frame=45966 fps=124 q=36.1 size=   70144kB time=00:25:33.05 bitrate= 374.8kbits/s dup=28 drop=0 speed=4.12x
		if (Utils.substringEx(linha,14).contains("frame=")) {
			try {
				String[] sp1 = linha.split(Pattern.quote(" time="));
				String[] sp2 = sp1[1].split(Pattern.quote(".")); //00:25:33
				String[] sp3 = sp2[0].split(Pattern.quote(":")); 
				return new GregorianCalendar(2020,0,1, Integer.parseInt(sp3[0]), Integer.parseInt(sp3[1]), Integer.parseInt(sp3[2])).getTime();
			} catch (Exception e) {
				return null;
			}
		} if (Utils.substringEx(linha,40).contains(" time=")) { //size=    6912kB time=00:07:33.33 bitrate= 124.9kbits/s speed=16.7x    
			try {
				String[] sp1 = linha.split(Pattern.quote(" time="));
				String[] sp2 = sp1[1].split(Pattern.quote(".")); //00:25:33
				String[] sp3 = sp2[0].split(Pattern.quote(":")); 
				return new GregorianCalendar(2020,0,1, Integer.parseInt(sp3[0]), Integer.parseInt(sp3[1]), Integer.parseInt(sp3[2])).getTime();
			} catch (Exception e) {
				return null;
			}
	    } else {
			return null;
		}
	}

}
