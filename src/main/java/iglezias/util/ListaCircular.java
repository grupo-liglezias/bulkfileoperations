package iglezias.util;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Lista com tamanho limitado. Quando tamanho estourado, remove primeiro elemento inserido
 * @author 91913837068
 *
 * @param <E>
 */
public class ListaCircular<E> extends ArrayList<E> {
	private int qtdElementos;

	public ListaCircular(int qtdElementos) {
		super((int) (qtdElementos*1.2) );
		this.qtdElementos = qtdElementos;
	}

	int idxAtual = -1;

	@Override
	public boolean add(E e) {
		idxAtual++;
		if (idxAtual>=qtdElementos) {
			idxAtual=0;
		}
		if (idxAtual>=this.size()) {
			super.add(e);
		} else {
			this.set(idxAtual, e);
		}
		return true;
	}

	@Override
	public boolean addAll(Collection<? extends E> c) {
		for (E e : c) {
			this.add(e);
		}
		// TODO Otimizar...
		return !c.isEmpty();
	}

	@Override
	public E remove(int index) {
		// TODO Auto-generated method stub
		return super.remove(index);
	}
}