/**
 *
 */
package iglezias.util;

/**
 * @author 91913837068
 * Classe utilitaria para questoes com Threads.
 *
 * @see veja tamb�m {@link TarefaCancelavel}
 */
public class UtilitariosThread {

	public static abstract class RunnableComRetorno<ObjRetorno> implements Runnable {
		private ObjRetorno ultResult=null;

		@Override
		public void run() {
			try {
				ultResult = runComRetorno();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}

		public abstract ObjRetorno runComRetorno() throws Exception;

		public ObjRetorno getUltRetorno() {
			return ultResult;
		}
	}

//	/**
//	 * Dispara a thread passada e aguarda sua conclusao.
//	 * @param thread
//	 * @param timeoutInMs 0 para nao ter timeout. >0 para ter timeout
//	 * @throws TimeoutException se o timeout atingido antes de a thread passada acabar
//	 *         Exception caso a thread passada dispare alguma excecao
//	 * @return o resultado do runComRetorno da thread passada, caso nao caia por timeout antes (dispara excecao);
//	 */
//	@SuppressWarnings("unchecked")
//	public static <Ret> Ret disparaThreadComTimeOut(final long timeoutInMs, String nomeThread, final RunnableComRetorno<Ret> thread) throws TimeoutException, Exception {
//		return (Ret) disparaThreadComTimeOut(timeoutInMs, nomeThread, (Runnable) thread);
//	}
//
//	/**
//	 * Dispara a thread passada e aguarda sua conclusao.
//	 * @param thread
//	 * @param timeoutInMs 0 para nao ter timeout. >0 para ter timeout
//	 * @throws TimeoutException se o timeout atingido antes de a thread passada acabar
//	 *         Exception caso a thread passada dispare alguma excecao
//	 * @return Se thread for uma instancia de {@link RunnableComRetorno} entao vai retornar o resultado do runComRetorno;
//	 *         caso contrario, retorna null
//	 */
//	public static Object disparaThreadComTimeOut(final long timeoutInMs, final Runnable thread) throws TimeoutException, Exception {
//		return disparaThreadComTimeOut(timeoutInMs, (String)null, thread);
//	}
//
//
//	/**
//	 * Dispara a thread passada e aguarda sua conclusao.
//	 * @param thread
//	 * @param timeoutInMs 0 para nao ter timeout. >0 para ter timeout
//	 * @throws TimeoutException se o timeout atingido antes de a thread passada acabar
//	 *         Exception caso a thread passada dispare alguma excecao
//	 * @return o resultado do runComRetorno da thread passada, caso nao caia por timeout antes (dispara excecao);
//	 */
//	@SuppressWarnings("unchecked")
//	public static <Ret> Ret disparaThreadComTimeOut(final long timeoutInMs, final RunnableComRetorno<Ret> thread) throws TimeoutException, Exception {
//		return (Ret) disparaThreadComTimeOut(timeoutInMs, (String)null, (Runnable) thread);
//	}
//
//	/**
//	 * Dispara a thread passada e aguarda sua conclusao.
//	 * @param thread
//	 * @param timeoutInMs 0 para nao ter timeout. >0 para ter timeout
//	 * @throws TimeoutException se o timeout atingido antes de a thread passada acabar
//	 *         Exception caso a thread passada dispare alguma excecao
//	 * @return Se thread for uma instancia de {@link RunnableComRetorno} entao vai retornar o resultado do runComRetorno;
//	 *         caso contrario, retorna null
//	 */
//	public static Object disparaThreadComTimeOut(final long timeoutInMs, String nomeThread, final Runnable thread) throws TimeoutException, Exception {
//		//dispara 2 threads. Uma para rodar a thread passada em si.
//		//Outra para retornar quando o timeout for atingido
//		//TSTST. Claro que nao precisa de 2 threads pra isso. Basta fazermos um join com o timeout especificado...
//
//		final ParamReferenciaObj<Object> retorno = new ParamReferenciaObj<>(null);
//		final ParamReferenciaObj<Exception> erro = new ParamReferenciaObj<>(null);
//		final ParamReferenciaObj<Boolean> terminou = new ParamReferenciaObj<>(false); //sincronismo para erro e retorno
//		Thread tarefa = new Thread( (nomeThread!=null)?nomeThread : "threadComTimeout" ) {
//			@Override
//			public void run() {
//				try {
//					Object ret;
//					if (thread instanceof RunnableComRetorno) {
//						ret = ((RunnableComRetorno)thread).runComRetorno();
//					} else {
//						thread.run();
//						ret=null;
//					}
//					if (!Thread.currentThread().isInterrupted()) {
//						synchronized (terminou) {
//							if (!terminou.parametro) {
//								retorno.parametro = ret;
//							}
//						}
//					}
//				} catch (Exception e) {
////					if (e instanceof InterruptedException) {
////						//faz nada se thread interrompida...
////					} else {
//						//provurar thread de pintura for�ada fora
//						synchronized (terminou) {
//							if (!terminou.parametro) {
//								erro.parametro = e;
//							}
//						}
////					}
//				}
//			}
//		};
//		tarefa.setUncaughtExceptionHandler(new UncaughtExceptionHandler() {
//			@Override
//			public void uncaughtException(Thread t, Throwable e) {
//				System.err.println("Erro na thread: "+t.getName()+" - "+e.getClass().getSimpleName()+" ("+e.getLocalizedMessage()+")");
//			}
//		});
//		tarefa.start();
//
//    	try {
//    		tarefa.join(timeoutInMs);
//    		if (tarefa.isAlive()) {
//    			throw new InterruptedException(); //interrompida ou "caida" por timeout, mesmo tratamento...
//    		}
//		} catch (InterruptedException e) {
//			synchronized (terminou) {
//				if (!terminou.parametro) {
//					erro.parametro = new TimeoutException("Queda por timeout");
//					terminou.parametro=true;
//				}
//			}
//		}
//
//    	synchronized (terminou) {
//			terminou.parametro=true;
//			if (tarefa.isAlive()) {
//				try {
//					//System.out.println("VAI INTERROMPER");
//					tarefa.interrupt();
//					//tarefa.stop(); //nao usar isso: causa crash na JVM!
//				} catch (Exception e) {
//					//faz nada
//				}
//			}
//
//    		if (erro.parametro!=null) {
//    			throw erro.parametro;
//    		} else {
//    			return retorno.parametro;
//    		}
//    	}
//	}
//
//	/**
//	 * Retorna a Thread (nao dah start), que ira rodar o codigo "runnableComRetorno".
//	 * O retorno e eventual excecao do processamento serao retornados, por referencia, nos parametros "retorno" e "erro"
//	 * @param nomeThread
//	 * @param retorno virah null no {@link ParamReferenciaObj#parametro} caso houve algum erro.
//	 *        senao, retorna o {@link RunnableComRetorno#runComRetorno()}
//	 * @param erro virah null no {@link ParamReferenciaObj#parametro} caso nao houve nenhum erro.
//	 *        se houver excecao nao tratada em  {@link RunnableComRetorno#runComRetorno()}, ela vir� aqui retornada.
//	 * @param runnableComRetorno
//	 * @return
//	 */
//	public static <T extends Object> Thread disparaThreadComRetorno(String nomeThread, final ParamReferenciaObj<T> retorno, final ParamReferenciaObj<Exception> erro,
//			final RunnableComRetorno<T> runnableComRetorno
//	) {
//		Thread tarefa = new Thread( (nomeThread!=null)?nomeThread : "threadComRetorno" ) {
//			@Override
//			public void run() {
//				try {
//					retorno.parametro = runnableComRetorno.runComRetorno();
//				} catch (Exception e) {
//					erro.parametro = e;
//				}
//			}
//		};
//		tarefa.setUncaughtExceptionHandler(new UncaughtExceptionHandler() {
//			@Override
//			public void uncaughtException(Thread t, Throwable e) {
//				System.err.println("Erro na thread: "+t.getName()+" - "+e.getClass().getSimpleName()+" ("+e.getLocalizedMessage()+")");
//			}
//		});
//		return tarefa;
//	}
//
//
//	public static interface ITarefaProdutora {
//		public void produz(OutputStream out) throws Exception;
//		public String getNomeTarefa();
//	}
//
//	public static interface ITarefaConsumidora {
//		public void consome(InputStream in) throws Exception;
//		public int getTamBuffer();
//		public String getNomeTarefa();
//	}
//
//	/**
//	 * Caso tipico de uso, porem de facil utilizacao, de {@link PipedInputStream} e {@link PipedOutputStream}
//	 *
//	 * OBS: este m�todo � s�ncrono. Ou seja, s� retorna ao chamador ap�s o produtor ter acabado de produzir
//	 *      E seus consumidores terem acabado de consumir essa producao.
//	 *
//	 * Veja um exemplo em {@link #testDisparaThreadProdutoraEConsumidora()}
//	 *
//	 * @param produtor -> implemente o metodo {@link ITarefaConsumidora#consome(InputStream)}
//	 * @param consumidores -> cada consumidor deve implementar o metodo {@link ITarefaConsumidora#consome(InputStream)}
//	 * Assim que o "produtor" dispuser bytes na stream, estes bytes sao disponibilizados aos consumidores.
//	 *
//	 * @throws Exception caso o produtor lance erro, todos os consumidores sao interrompidos e essa excecao eh lancada neste metodo
//	 *
//	 */
//	public static void disparaThreadProdutoraEConsumidora(final ITarefaProdutora produtor, ITarefaConsumidora... consumidores
//	) throws Exception {
//
//		final PipedOutputStreamEx produzScript = new PipedOutputStreamEx();
//		List<PipedInputStreamEx> ins = new ArrayList<>();
//		for (ITarefaConsumidora iTarefaConsumidora : consumidores) {
//			ins.add(new PipedInputStreamEx(iTarefaConsumidora.getTamBuffer()));
//		}
//		produzScript.connect(ins.toArray(new PipedInputStreamEx[0]));
//
//		List<ParamReferenciaObj<Exception>> excecoesConsumidores = new ArrayList<>();
//		List<Thread> tarefasConsumidores = new ArrayList<>();
//
//
//		ParamReferenciaObj<Object> retornoProdutor = new ParamReferenciaObj<>(); //nao retorna nada...
//		ParamReferenciaObj<Exception> erroProdutor = new ParamReferenciaObj<>();
//		Thread tarefaProdutor = UtilitariosThread.disparaThreadComRetorno(produtor.getNomeTarefa(), retornoProdutor, erroProdutor,
//			new RunnableComRetorno<Object>() {
//				@Override
//				public Object runComRetorno() throws Exception {
//					produtor.produz(produzScript);
//					produzScript.close();
//			        return null;
//				}
//			}
//		);
//
//		int idx = -1;
//		for (final ITarefaConsumidora cons : consumidores) {
//			idx++;
//			final PipedInputStreamEx in = ins.get(idx);
//			ParamReferenciaObj<Object> retornoConsumidor = new ParamReferenciaObj<>(); //nao retorna nada...
//			ParamReferenciaObj<Exception> erroConsumidor = new ParamReferenciaObj<>();
//			excecoesConsumidores.add(erroConsumidor);
//			Thread tarefaConsumidor = UtilitariosThread.disparaThreadComRetorno(cons.getNomeTarefa(), retornoConsumidor, erroConsumidor,
//				new RunnableComRetorno<Object>() {
//					@Override
//					public Object runComRetorno() throws Exception {
//						cons.consome(in);
//						in.close();
//						return null;
//					}
//				}
//			);
//			tarefasConsumidores.add(tarefaConsumidor);
//		}
//
//		tarefaProdutor.start();
//		for (Thread tConsome : tarefasConsumidores) {
//			tConsome.start();
//		}
//
//
//		{ //aguarda as threads (produtor e consumidores) acabarem...
//			tarefaProdutor.join();
//			if (erroProdutor.parametro!=null) {
//				for (Thread tConsome : tarefasConsumidores) {
//					tConsome.interrupt();
//				}
//				throw erroProdutor.parametro;
//			}
//
//			int idx2=-1;
//			for (Thread tConsome : tarefasConsumidores) {
//				idx2++;
//				tConsome.join();
//				if (excecoesConsumidores.get(idx2).parametro!=null) {
//					throw excecoesConsumidores.get(idx2).parametro;
//				}
//			}
//		}
//	}
//
//	/**
//	 * Thread que permite, atrav�s de {@link #novoMonitoramento(NivelLog, String)}
//	 * logar a mensagem especificada caso o {@link #tempoParaComecarALogar} seja estrapolado.
//	 * @author 91913837068
//	 */
//	public static class ThreadLogaSeTempoMaior extends Thread {
//		public static interface ILogaSeTempoMaior {
//			public void loga(long tempo, String msgOriginal, NivelLog nivelLog, int nroVezMonitoramento );
//		}
//
//		public static class LogaSeTempoMaiorDefault implements ILogaSeTempoMaior {
//			@Override
//			public void loga(long tempo, String msgOriginal, NivelLog nivelLog, int nroVezMonitoramento) {
//				String msg = msgOriginal;
//				String tempoS = tempo + "ms";
//				msg = msgOriginal.replaceAll("\\$1", tempoS); //+" "+nroVezMonitoramento;
//				JSLogger.loga(nivelLog, msg);
//			}
//		}
//
//		private ILogaSeTempoMaior logador = new LogaSeTempoMaiorDefault();
//		private int tempoParaComecarALogar;
//		private int intervaloRepeticao;
//		private boolean terminou = false;
//		private String msgLog;
//		private long timestampMonitora;
//		private NivelLog nivelLog;
//		private SemaforoBooleano semaforoPrimeiroMonitoramento = new SemaforoBooleano(false);
//
//		/**
//		 * Pode-se sobrescrever o que fazer para logar quando tempo extrapolado.
//		 * @param logador
//		 */
//		public synchronized void setLogador(ILogaSeTempoMaior logador) {
//			this.logador = logador;
//		}
//
//		/**
//		 * @param nomeThread
//		 * @param tempoParaComecarALogar tempo que deve-se se esperar para logar a primeira vez
//		 * @param intervaloRepeticao tempo de recorrencia dos novos logs que sucedem a primeira vez
//		 */
//		public ThreadLogaSeTempoMaior(String nomeThread, int tempoParaComecarALogar, int intervaloRepeticao) {
//			super(nomeThread);
//			this.tempoParaComecarALogar = tempoParaComecarALogar;
//			this.intervaloRepeticao = intervaloRepeticao;
//		}
//
//		@Override
//		public synchronized void start() {
//			if (this.tempoParaComecarALogar<=0) {
//				//faz nada...
//			} else {
//				super.start();
//			}
//		}
//
//		@Override
//		public void run() {
//			try {
//				this.semaforoPrimeiroMonitoramento.aguardaSinalVerde();
//			} catch (Exception e1) {}
//
//			int tempoAguardeAtual = this.tempoParaComecarALogar;
//			int vezMonitoramento = 1;
//			while (!this.terminou) {
//				long timestampInicio;
//				synchronized (this) {
//					timestampInicio = this.timestampMonitora;
//				}
//				try {
//					Thread.sleep(tempoAguardeAtual);
//					synchronized (this) {
//						if (!this.terminou) {
//							if (timestampInicio == this.timestampMonitora) { //significa estourou o tempo do ultimo "novoMonitoramento"
//								this.logador.loga(System.currentTimeMillis()-this.timestampMonitora, this.msgLog, this.nivelLog, vezMonitoramento);
//								vezMonitoramento++;
//								tempoAguardeAtual = this.intervaloRepeticao;
//							} else { //houve novo monitoramento
//								vezMonitoramento=1;
//								tempoAguardeAtual = this.tempoParaComecarALogar;
//							}
//						}
//					}
//				} catch (InterruptedException e) {
//					synchronized (this) {
//						if (!this.terminou) {
//							//houve novo monitoramento
//							vezMonitoramento=1;
//							tempoAguardeAtual = this.tempoParaComecarALogar;
//						}
//					}
//				}
//			}
//		}
//
//		/**
//		 * @param nivelLog
//		 * @param msgLog a mensagem para logar quando estourar o tempo. Pode conter o parametro $1, que sera substituido pelo tempo decorrido deste monitoramento
//		 */
//		public synchronized void novoMonitoramento(NivelLog nivelLog, String msgLog) {
//			if (this.tempoParaComecarALogar<=0) {
//				return; //faz nada
//			}
//			this.nivelLog = nivelLog;
//			this.msgLog = msgLog;
//			this.timestampMonitora = System.currentTimeMillis();
//			this.semaforoPrimeiroMonitoramento.setaSinalVerde();
//			this.interrupt(); //forca iniciar novo monitoramento...
//		//	this.interrupted(); //
//		}
//
//		public synchronized void termina() {
//			this.terminou = true;
//			this.semaforoPrimeiroMonitoramento.setaSinalVerde(); //teoricamente, nao precisa, jah que vamos interromper no comando a seguir... mas...
//			if (this.tempoParaComecarALogar<=0) {
//				return; //faz nada
//			}
//			this.interrupt();
//		}
//	}
//	
//	public static Thread getThreadComNome(String nome) { //NAO adianta: nao retorna as threads do TOMCAT como um todo....
//		Collection<Thread> todasThreads = getTodasThreads();
//		
//		for (Thread t : todasThreads) {
//			if (t.getName().equals(nome)) {
//				return t;
//			}
//		}
//		return null;
//	}
//
//	/**
//	 * @return
//	 */
//	public static List<Thread> getTodasThreads() {
//		//Thread.getAllStackTraces().keySet();
//
//		List<Thread> todasThreads = new ArrayList<>();
//		ThreadGroup rootGroup = getRootThreadGroup();
//		//Now, call the enumerate() function on the root group repeatedly. The second argument lets you get all threads, recursively:
//
//		Thread[] threads = new Thread[rootGroup.activeCount()];
//		int totThreads;
//		while ( (totThreads=rootGroup.enumerate(threads, true)) 
//				== threads.length
//		) {
//		    threads = new Thread[threads.length * 2];
//		}
//		for (Thread t : threads) {
//			if (t!=null) {
//				todasThreads.add(t);
//			}
//		}
//		return todasThreads;
//	}
//
//	public static ThreadGroup getRootThreadGroup() {
//		ThreadGroup rootGroup = Thread.currentThread().getThreadGroup();
//		ThreadGroup parentGroup;
//		while ((parentGroup = rootGroup.getParent()) != null) {
//		    rootGroup = parentGroup;
//		}
//		return rootGroup;
//	}
//	
    public static abstract class RunnableComRetornoETeste<T> extends RunnableComRetorno<T> {
    	private int msEstouroTimeout=-1;

		public abstract boolean testeOk(T retorno, Exception excecaoRetorno);

		public void setEstourouTimeout(int msEstouroTimeout) {
			this.msEstouroTimeout = msEstouroTimeout;
		}
		/**
		 * @return >=0 se houve estouro. o valor eh o Timeout em MS que estourou. <0 se nao estourou timeout
		 */
		public int getEstouroTimeout() {
			return msEstouroTimeout;
		}
    }

    public static <T> T runComRetry(RunnableComRetornoETeste<T> run, int msMaximoEspera) throws Exception {
    	return runComRetry(run, 100, msMaximoEspera);
    }
    
    /**
     * Executa varias vezes (retry) o run passado, tantas quantas forem necessarias para o run estar ok, ou tempoLimite excedido
     * @param <T>
     * @param run
     * @param msNovaTentativa
     * @param msMaximoEspera
     * @return null caso nao consiga nas varias tentativas e timeout estourado (para distinguir um retorno null ou um timeout, olhe {@link RunnableComRetornoETeste#getEstouroTimeout()}
     * @throws Exception
     */
    public static <T> T runComRetry(RunnableComRetornoETeste<T> run, int msNovaTentativa, int msMaximoEspera) throws Exception {
    	long msAntes = System.currentTimeMillis();
    	boolean deveRepetir;
		T ultRes;
		Exception ultimaExcecao;
		do {
			ultimaExcecao = null;
			ultRes=null;
			try {
				ultRes = run.runComRetorno();
				deveRepetir = !run.testeOk(ultRes, null);
			} catch (Exception e) {
				deveRepetir = !run.testeOk(null, e);
				//System.out.println(UtilitariosExcecao.getMensagemUserFriendlyParaExcecao(e));
				ultimaExcecao = e;
			}
			if (deveRepetir) {
				try {
					Thread.sleep(msNovaTentativa);
				} catch (InterruptedException e) {
					break;
				}
			}
		} while ( deveRepetir && ((System.currentTimeMillis()-msAntes) < msMaximoEspera)  );
		if (deveRepetir) {
			run.setEstourouTimeout(msMaximoEspera);
		}
		if (ultimaExcecao!=null) {
			throw ultimaExcecao;
		} else {
			return ultRes;
		}
    }
	
}
