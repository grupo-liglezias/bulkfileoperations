package iglezias.bulkFileOperations;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import iglezias.util.ObjetoMonitoracaoLight;

public class BulkFileOperations {
	
	public static final String CHAVE_MONITORACAO_PRINCIPAL="principal";
	
	public static interface IAoFinalizarOperacao {
		/**
		 * @param arqs lista de arquivos que estao sendo processados
		 * @param idxArqFinalizado indice de arqs do corrente arquivo que acabou de ser finalizado
		 * @param param1PathCompletoArquivo
		 * @param param2NomeArquivo
		 * @param param3PastaDestino
		 * @param param4PathRelativoArquivo
		 * @return se false, abortarah o processamento (nao processarah novos arquivos)
		 */
		public boolean aoFinalizarOperacao(List<File> arqs, int idxArqFinalizado/*, String param1PathCompletoArquivo, String param2NomeArquivo*/
				, String param3PastaDestino, String param4PathRelativoArquivo) throws IOException;
	}

	public static interface IProgresso {
		/**
		 * @param arqs
		 * @param idxArqProgresso
		 * @param pastaDestino
		 * @param pathRelativoArquivo
		 * @param totKBytesProcessados o total de bytes jah processados, contando como ZERO os bytes processados do corrente arquivo.
		 * @param linhaOutCorrenteArquivo pode vir nulo quando recem trocou o arquivo em processamento.
		 *        cada linha emitida pelo comando, para processar o corrente arquivo, sera aqui passada em um evento onProgress
		 * @throws IOException
		 */
		public void onProgress(List<File> arqs, int idxArqProgresso/*, String param1PathCompletoArquivo, String param2NomeArquivo*/
				, String pastaDestino, String pathRelativoArquivo, int totKBytesProcessados, String linhaOutCorrenteArquivo) throws IOException;
	}

	private int idxAtu;
	private String pathRelativoAtual;
	private String pastaDestinoAtual;
	private List<File> arqsAtu;
	protected int totKBytesProcessados;
	
	public static interface ICallBackComando {
		public String getComando(String comandoOriginal, List<File> arqs, int idxArqProgresso/*, String param1PathCompletoArquivo, String param2NomeArquivo*/
				, String pastaDestino, String pathRelativoArquivo, int totKBytesProcessados);
	}
	
	
	/**
	 *	%1 = path completo arquivo
	 *  %2 = nome do arquivo
	 *  %3 = pasta destino
	 *  %4 = path do arquivo RELATIVO 'a pasta base 
	 * @param comando pode vir null, mas entao deve ser definido via buscaComando
	 * @param arqs
	 * @param pastaDestino
	 * @param pastaBase
	 * @param prog
	 * @param aoFinalizarOperacao
	 * @param buscaComando comando customizado para um dado arquivo
	 * @throws IOException
	 */
	public void efetuaOperacoesSincronamente(final String comando, List<File> arqs, String pastaDestino
			, String pastaBase, final IProgresso prog, IAoFinalizarOperacao aoFinalizarOperacao
			, ICallBackComando buscaComando
	) throws IOException {
		ObjetoMonitoracaoLight.iniciarContagem(CHAVE_MONITORACAO_PRINCIPAL);
		try {
			String pastaDestinoParaReplace = pastaDestino.replaceAll(Pattern.quote("\\"), "\\\\\\\\");
			this.pastaDestinoAtual = pastaDestino;
			this.arqsAtu = arqs;


			IFeedBackLinha feedBackLinha = new IFeedBackLinha() {
				@Override
				public void feedBacklinha(String linha) throws IOException {
					if (prog!=null) {
						prog.onProgress(BulkFileOperations.this.arqsAtu, BulkFileOperations.this.idxAtu, BulkFileOperations.this.pastaDestinoAtual, BulkFileOperations.this.pathRelativoAtual
								, BulkFileOperations.this.totKBytesProcessados, linha);
					}
				}
			};

			BulkFileOperations.this.totKBytesProcessados = 0;
			for (idxAtu = 0; idxAtu < arqs.size(); idxAtu++) {
				File f = arqs.get(idxAtu);
				if (!f.exists()) {
					System.err.println("Arquivo nao existe. Pulando: ["+f.getAbsolutePath()+"]");
					continue;
				}
				long tamArq = f.length(); //guardamos antes que o arquivo seja mexido/movido...
				String caminho = f.getAbsolutePath().replaceAll(Pattern.quote("\\"), "\\\\\\\\");
				String nome = f.getName().replaceAll(Pattern.quote("\\"), "\\\\\\\\");
				
				String pathRelativoParaReplace;
				{
					Path pOrigem = Paths.get(f.getParentFile().getAbsolutePath());
					Path pBase = Paths.get(pastaBase);
					if (pBase.getNameCount() == pOrigem.getNameCount()) {
						this.pathRelativoAtual = "";
					} else {
						this.pathRelativoAtual = pOrigem.subpath(pBase.getNameCount(), pOrigem.getNameCount()).toString()+File.separatorChar;
					}
					{
						File pDestino = new File(this.pastaDestinoAtual,this.pathRelativoAtual);
						//System.out.println("Pasta: "+pDestino.getAbsolutePath());
						if (!pDestino.exists()) {
							pDestino.mkdirs();
						}
					}
					pathRelativoParaReplace = this.pathRelativoAtual.replaceAll(Pattern.quote("\\"), "\\\\\\\\");

					//				if (pOrigem.getNameCount()>3) {
					//					System.out.println(caminho);
					//					System.out.println(pOrigem);
					//					System.out.println(pBase);
					//					System.out.println("Relat="+relat + " / "+pBase.getNameCount() + " / "+pOrigem.getNameCount());
					//					System.out.println("FINAL="+this.pastaDestinoAtual+relat+nome);
					//					System.out.println("Final repl="+pastaDestinoParaReplace+pathRelativoParaReplace+nome);
					//					System.out.println();
					//				}
					////				 *  %3%4%2.mp4\
					////  			 *	%1 = path completo arquivo
					////				 *  %2 = nome do arquivo
					////				 *  %3 = pasta destino
					////				 *  %4 = path do arquivo RELATIVO 'a pasta base 
					//				if (true) continue;
				}

				String comandoAtu;
				if (buscaComando!=null) {
					comandoAtu = buscaComando.getComando(comando, BulkFileOperations.this.arqsAtu, BulkFileOperations.this.idxAtu, BulkFileOperations.this.pastaDestinoAtual, BulkFileOperations.this.pathRelativoAtual
							, BulkFileOperations.this.totKBytesProcessados);
				} else {
					comandoAtu = comando;
				}
				if (prog!=null) {
					prog.onProgress(BulkFileOperations.this.arqsAtu, BulkFileOperations.this.idxAtu, BulkFileOperations.this.pastaDestinoAtual, BulkFileOperations.this.pathRelativoAtual
							, BulkFileOperations.this.totKBytesProcessados, "");
				}
				if (comandoAtu!=null) {
					comandoAtu = comandoAtu.replaceAll(Pattern.quote("%1"), caminho);
					comandoAtu = comandoAtu.replaceAll(Pattern.quote("%2"), nome);
					comandoAtu = comandoAtu.replaceAll(Pattern.quote("%3"), pastaDestinoParaReplace);
					comandoAtu = comandoAtu.replaceAll(Pattern.quote("%4"), pathRelativoParaReplace);
					System.out.println("** RODANDO["+idxAtu+"]:\n"+comandoAtu);
					System.out.println("\nRetorno["+idxAtu+"]: "+executaComando(feedBackLinha, quebraParametrosComando(comandoAtu)));
					System.out.println("\n");
				} else {
					System.out.println("\n\n** NAO RODADO["+idxAtu+"]:\n"+nome);
				}

				if (aoFinalizarOperacao!=null) {
					if (!aoFinalizarOperacao.aoFinalizarOperacao(
								arqs, this.idxAtu, this.pastaDestinoAtual, this.pathRelativoAtual)
					) {
						System.err.println("Operacao cancelada. Último arquivo: ["+f.getAbsolutePath()+"]");
						break;
					}
				}
				BulkFileOperations.this.totKBytesProcessados += (tamArq / 1024);
				//			if (i>-1) {
				//				break;
				//			}
			}
		} finally {
			ObjetoMonitoracaoLight.finalizarContagem(CHAVE_MONITORACAO_PRINCIPAL);
		}
	}
	
	private String[] quebraParametrosComando(String comando2) {
		List<String> result = new ArrayList<>();
		StringBuilder str = new StringBuilder();
		boolean emAspas=false;
		for (char c : comando2.toCharArray()) {
			if ('"' == c) {
				emAspas = !emAspas;
			}
			if (!emAspas &&  ' ' == c) {
				result.add(str.toString());
				str.setLength(0);
			} else {
				str.append(c);
			}
		}
		if (str.length()>0) {
			result.add(str.toString());
		}
		return result.toArray(new String[]{});
	}

	public static interface IFeedBackLinha {
		public void feedBacklinha(String linha) throws IOException;
	}
	
	public String executaComando(IFeedBackLinha aoLinhaOut, String... comando) throws IOException {
//		for (String strComando : comando) {
//			System.out.println(strComando);
//		}
//		if (true) {
//			return "";
//		}
		
		//Process p = Runtime.getRuntime().exec("ffmpeg");//comando);
		Process p = Runtime.getRuntime().exec(comando);
		StringBuilder resultStr = new StringBuilder();
		try {
			Process result = p;//p.onExit().get();
			{
				try (InputStream stmRes = result.getErrorStream()) {
					BufferedReader input = new BufferedReader(new InputStreamReader(stmRes));
					String linha;
					while ((linha = input.readLine())!=null) {
						resultStr.append(linha);
						//System.out.println(linha);
						if (aoLinhaOut!=null) {
							aoLinhaOut.feedBacklinha(linha);
						}
					}
				}
			}
//			{
//				try (InputStream stmRes = result.getInputStream()) {
//					BufferedReader input = new BufferedReader(new InputStreamReader(stmRes));
//					String linha;
//					while ((linha = input.readLine())!=null) {
//						resultStr.append(linha);
//						//System.out.println("Linha "+nrLinha+": "+linha);
//					}
//				}
//			}
//			{
//				try (InputStream stmRes = result.getErrorStream()) {
////					BufferedReader input = new BufferedReader(new InputStreamReader(stmRes));
//					int byteLido;
//					while ((byteLido = stmRes.read())!=-1) {
//						System.out.print((char)byteLido);
//						//resultStr.append(linha);
//						//System.out.println("Linha "+nrLinha+": "+linha);
//					}
//				}
//			}
			
		} catch (Exception e) {//| InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
		return resultStr.toString();

//			InputStream isProcesso;
//			try {
//				
//				isProcesso = p.getErrorStream(); //primeiro tenta com a saida de erro... (nao me pergunta pq, mas o retorno sai na saida de erro no windows e no linux! vai saber!)
//				VersaoJava result = getVersaoJava(isProcesso);
//				isProcesso.close();
//				if (result==null) {
//					throw new Exception("versao do java nao detectada na Saida de erro.");
//				} else {
//					return result;
//				}
//			} catch (Exception e) {
//				System.err.println(e.getClass().getSimpleName()+"-"+e.getLocalizedMessage()+" : erro ao buscar versao java pela saida de erro, tentando agora pela saida padrao");
//				isProcesso = p.getInputStream();
//				VersaoJava result = getVersaoJava(isProcesso);
//				isProcesso.close();
//				return result;
//			}
	}
	
}
