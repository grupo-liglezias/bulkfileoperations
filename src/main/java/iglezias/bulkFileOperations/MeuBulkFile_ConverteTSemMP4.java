package iglezias.bulkFileOperations;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import iglezias.bulkFileOperations.BulkFileOperations.IAoFinalizarOperacao;
import iglezias.bulkFileOperations.BulkFileOperations.ICallBackComando;
import iglezias.bulkFileOperations.BulkFileOperations.IProgresso;
import iglezias.util.Dupla;
import iglezias.util.ObjetoMonitoracaoLight;
import iglezias.util.UtilitariosArquivo;
import iglezias.util.Utils;
import iglezias.util.UtilsFFMpeg;

public class MeuBulkFile_ConverteTSemMP4 extends BulkFileOperations implements IAoFinalizarOperacao, IProgresso, ICallBackComando {
	public final String pastaBase = "l:\\tempOrganizar\\conv\\toConvert\\"; //"l:\\tempOrganizar\\conv\\to2D\\"; //"C:\\tempExcluir\\XX_NET\\";
	public final File pastaBaseDone = new File("l:\\tempOrganizar\\conv\\doneConvert\\"); //new File("C:\\tempExcluir\\XX_NET_DONE\\");
	public final String pastaDestinoConversao = "l:\\tempOrganizar\\conv\\coded\\";
	
	public final String COMANDO_ATUAL_TS  = UtilsFFMpeg.COMANDO_SO_CANAL_ESQUERDO;
	public final String COMANDO_ATUAL_A2  = UtilsFFMpeg.COMANDO_SO_FAIXA2;
	public final String COMANDO_ATUAL_MP4_E_OUTROS =
			//UtilsFFMpeg.COMANDO_ROT180graus;
			//UtilsFFMpeg.COMANDO_AUDIO_INTEIRO_ALTACOMPRESSAO;
			//UtilsFFMpeg.COMANDO_AUDIO_INTEIRO_MEDIACOMPRESSAO;
			UtilsFFMpeg.COMANDO_AUDIO_51_TO_STEREO_BOTH_AUDIO_TRACKS; 
			//UtilsFFMpeg.COMANDO_CODIFICACAO_PARA_TVS_ANTIGAS;
			//UtilsFFMpeg.COMANDO_CODIFICACAO_GPU;
			//UtilsFFMpeg.COMANDO_REMOVE_3D_CORTA_METADE_HORIZONTAL;
			//UtilsFFMpeg.COMANDO_PERSPECTIVA_16_POR_9_WIDE;
			//UtilsFFMpeg.COMANDO_REMOVER_CHAPTERS;
	
	/**
	 * <=0 para nao limitar 
	 */
	public final int qtdSegundosLimitePorArquivo = 0; //20;//15;//2*60; //<=0 
	public final int qtdSegundosInicio=0;//15*60 + 6; //10*60; //0
	public final boolean moverParaDone=true;//false;
	
	private VgProgress vg;
	private File pastaFFMPeg;
	
	public MeuBulkFile_ConverteTSemMP4() {
		pastaBaseDone.mkdirs();
		this.pastaFFMPeg = new File(UtilitariosArquivo.getArquivoJarOuPastaBin(), "binaries");
	}
	
	public void processa() throws IOException {
		List<File> arqs = Utils.listaPathsArquivosDiretorioRecursivamente(new File(pastaBase), 0);
		
		vg = new VgProgress(0,Utils.getTamanhoTotalArquivosEmKb(arqs), "Convertendo arquivos TS...");
		vg.setVisible(true);
		this.efetuaOperacoesSincronamente(arqs, pastaDestinoConversao, pastaBase);
		vg.setVisible(false);
	}
	
	private void efetuaOperacoesSincronamente(List<File> arqs, String pastadestinoconversao2, String pastabase2) throws IOException {
		this.efetuaOperacoesSincronamente(null, arqs, pastaDestinoConversao, pastaBase, this, this, this);
	}

	private File arqCorrente = null;
	private Date duracaoCorrente = null;
	private Date currTimeCorrente = null;
	
	@Override
	public String getComando(String comandoOriginal, List<File> arqs, int idxArqProgresso, String pastaDestino,
			String pathRelativoArquivo, int totKBytesProcessados
	) {
		String ffmpegFolder = pastaFFMPeg.getAbsolutePath()+File.separatorChar; //"C:\\Users\\iglezias\\git\\bulkfileoperations\\src\\main\\java\\binaries\\";
		File f = arqs.get(idxArqProgresso);
		
		File arqFeito = new File(pastaDestino,pathRelativoArquivo+f.getName());
		if (arqFeito.exists()) {
			return null;
		}
		
		String res;
		if (f.getName().toLowerCase().endsWith(".a2")) {
			res = ffmpegFolder+this.COMANDO_ATUAL_A2;
		} else if (f.getName().toLowerCase().endsWith(".ts")) {
			res =  ffmpegFolder+this.COMANDO_ATUAL_TS;
		} else {
			res = ffmpegFolder+this.COMANDO_ATUAL_MP4_E_OUTROS;
		}
		if (this.qtdSegundosLimitePorArquivo>0) {
			res = UtilsFFMpeg.adicionaLimiteTempoComandoFFMPEG(res,this.qtdSegundosInicio, this.qtdSegundosLimitePorArquivo);
		}
		return res;
	}
	
	private int idxArqAntesProgress=-1;
	
	@Override
	public void onProgress(List<File> arqs, int idxArqProgresso, String param3PastaDestino,
			String param4PathRelativoArquivo, int totKBytesProcessados, String linhaOutCorrenteArquivo
	) throws IOException {
		if (this.arqCorrente==null) {
			this.arqCorrente = arqs.get(idxArqProgresso);
		}
		if (this.duracaoCorrente==null) {
			this.duracaoCorrente = UtilsFFMpeg.getDurationFromString(linhaOutCorrenteArquivo);//"  Duration: 00:30:25.66, start: 20151.479433, bitrate: 1767 kb/s");
			if (this.duracaoCorrente!=null) {
				System.out.println("Duração total do arquivo: "+duracaoCorrente);
			}
		}
		
		if (this.idxArqAntesProgress!=idxArqProgresso) {
			this.vg.setMaxProgress(Utils.getTamanhoTotalArquivosEmKb(arqs)); //soh fazer quando trocar para prox arquivo
		}
		this.idxArqAntesProgress = idxArqProgresso;
		
		Date curTime = UtilsFFMpeg.getCurrentProcTimeFromString(linhaOutCorrenteArquivo);//"frame=45966 fps=124 q=36.1 size=   70144kB time=00:25:33.05 bitrate= 374.8kbits/s dup=28 drop=0 speed=4.12x");
		if (curTime!=null) {
			this.currTimeCorrente = curTime;
		}
		
		double percentualProg;
		int kbytesArqAtual;
		double percTotal;
		
		if (this.duracaoCorrente!=null && this.currTimeCorrente!=null) {
			percentualProg = getPercentualTempo(this.currTimeCorrente, this.duracaoCorrente); 
			kbytesArqAtual = (int) ( (this.arqCorrente.length() / 1024) * percentualProg );
		} else {
			percentualProg=0;
			kbytesArqAtual=0;
		}
		percTotal = (totKBytesProcessados+kbytesArqAtual) / (double) this.vg.getMaxProgress() * 100;

		SimpleDateFormat df = new SimpleDateFormat("d-HH:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone("GMT")); //se nao setarmos GMT0 aki, a formatacao da hora fica errada (pega o fuso do brasil). Ex: 1h12min fica 20h12min (pq fica gmt-3)...		
		Dupla<Long, Long> estimativaTempoTotal = ObjetoMonitoracaoLight.getEstimativaTempoTotal(CHAVE_MONITORACAO_PRINCIPAL, percTotal);
		Date tDecorrido = new Date(); /*tDecorrido.setTime(0);*/ tDecorrido.setTime(estimativaTempoTotal.o1);
		String tTotalStr;
		String tRestStr;
		if (estimativaTempoTotal.o2==0) {
			tTotalStr = "?";
			tRestStr  = "?";
		} else {
			Date tTotal = new Date(); /*tTotal.setTime(0);*/ tTotal.setTime(estimativaTempoTotal.o2);
			Date tRestante = new Date(); tRestante.setTime(estimativaTempoTotal.o2-estimativaTempoTotal.o1);
			tTotalStr = df.format(tTotal);
			tRestStr  = df.format(tRestante);
		}
		
		this.vg.setProgress(totKBytesProcessados+kbytesArqAtual, 
				"<html><body>"+
				"<b>Arquivo</b> "+(idxArqProgresso+1)+" de "+arqs.size()+" <b>TamAtual(Kb)</b> = "+((int) (this.arqCorrente.length()/1024))+
				"<br><b>Kbytes</b> = "+(totKBytesProcessados+kbytesArqAtual)+" de "+this.vg.getMaxProgress()+
				"<br>Tempo decorrido (dia-hora:min:seg): "+df.format(tDecorrido)+
				"<br>Tempo total/restante estimado: "+tTotalStr+" /  <font color=\"red\">"+tRestStr+"</font>"+//" "+percTotal+
				"<br>Arquivo = "+arqs.get(idxArqProgresso).getName()+
				"<br><b>% arquivo</b> = <font color=\"red\">"+
				Utils.getHTMLProgressoEmFormaTabela(50,(int)(percentualProg*100), true)
				//DecimalFormat.getPercentInstance().format(percentualProg)
				+"</font>"+
				"</body></html>");
		System.out.println(linhaOutCorrenteArquivo);
	}

	@Override
	public boolean aoFinalizarOperacao(List<File> arqs, int idxArqFinalizado, /*String param1PathCompletoArquivo,
			String param2NomeArquivo,*/ String param3PastaDestino, String param4PathRelativoArquivo
	) throws IOException {
		File arq = arqs.get(idxArqFinalizado);
		File arqFeito = new File(param3PastaDestino,param4PathRelativoArquivo+arq.getName()); //+".mp4");
		System.out.println("FINALIZOU "+arq.getAbsolutePath());
		System.out.println("tam ArqOriginal(Mb) = "+arq.length()/1024/1024);
		System.out.println("tam ArqFinal(Mb) = "+arqFeito.length()/1024/1024);
		System.out.println("Compressão: "+DecimalFormat.getPercentInstance().format(1-(arqFeito.length()/(double)arq.length())));
		if (arqFeito.length()>200*1024) { //300kb, tah ok para achar q converteu...
			if (moverParaDone) {
				File arqDestinoDone = new File(pastaBaseDone,param4PathRelativoArquivo+File.separatorChar+arq.getName());
				arqDestinoDone.getParentFile().mkdirs();
				UtilitariosArquivo.moveArquivoOuPasta(arq, arqDestinoDone);
				//java.nio.file.Files.move( arq.toPath(), arqDestinoDone.toPath());
				System.out.println("Movido arq original para: "+arqDestinoDone.getAbsolutePath());
			}
		}
		System.out.println("\n\n\n");
		this.arqCorrente=null;
		this.duracaoCorrente=null;
		this.currTimeCorrente=null;
		return true;
	}
	
	
	public static void main(String[] args) throws IOException {
//		System.out.println(new File(UtilitariosArquivo.getArquivoJarOuPastaBin(), "binaries"));
//		System.exit(0);
		MeuBulkFile_ConverteTSemMP4 b = new MeuBulkFile_ConverteTSemMP4();
		try {
			Thread.sleep(1);//15*60*1000);
			b.processa();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			System.exit(0);
		}
	}
	
	public static void main2(String[] args) throws InterruptedException {
		Date dt = UtilsFFMpeg.getDurationFromString("  Duration: 00:30:25.66, start: 20151.479433, bitrate: 1767 kb/s");
		System.out.println(dt);
		Date dt2 = UtilsFFMpeg.getDurationFromString("Duration: 01:31:28.66, start: 20151.479433, bitrate: 1767 kb/s");
		System.out.println(dt2);
		
		Date dt3 = UtilsFFMpeg.getCurrentProcTimeFromString("frame=45966 fps=124 q=36.1 size=   70144kB time=00:25:33.05 bitrate= 374.8kbits/s dup=28 drop=0 speed=4.12x");
		System.out.println(dt3);
		
		Map<TimeUnit, Long> dif = Utils.computeDiff(dt3,dt);
		System.out.println(dif.get(TimeUnit.MINUTES)+" : "+dif.get(TimeUnit.SECONDS) ); //30:25 - 25:33 =
		
		VgProgress vg = new VgProgress(1,150,"titulo");
		vg.setVisible(true);
		for (int i = 1; i <= 150; i++) {
			Thread.sleep(90);
			vg.setProgress(i, i+" de 150");
		}
		vg.setVisible(false);
		System.exit(0);
	}

	
	public static double getPercentualTempo(Date currTime, Date totTime) {
		//Map<TimeUnit, Long> segTot = Utils.computeDiff(dataZero, totTime);
		long msTot = totTime.getTime() - dataZero.getTime();
		long msCurr = currTime.getTime() - dataZero.getTime();
		return msCurr / (double) msTot;
	}
	
	private static Date dataZero=new GregorianCalendar(2020,0,1,0,0,0).getTime();
	public static Date getDataZero() {
		return dataZero;  
	}
	
}
