package iglezias.bulkFileOperations;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JProgressBar;

public class VgProgress extends JFrame {
	private JProgressBar progress;
	private JLabel label;

	public VgProgress(int min, int max, String titulo) {
		this.setSize(500, 250);
		this.setTitle(titulo);
		this.getContentPane().setLayout(new BorderLayout());
		this.progress = new JProgressBar(min, max);
		this.progress.setStringPainted(true);
		this.label = new JLabel();
		this.getContentPane().add(this.progress, BorderLayout.CENTER);			
		this.getContentPane().add(this.label, BorderLayout.SOUTH);			
	}

	public void setProgress(int progresso, String txt) {
		this.progress.setValue(progresso);
		this.label.setText(txt);
	}
	
	public int getMaxProgress() {
		return this.progress.getMaximum();
	}
	
	public void setMaxProgress(int maximo) {
		this.progress.setMaximum(maximo);
	}
	
	public int getProgress() {
		return this.progress.getValue();
	}
}