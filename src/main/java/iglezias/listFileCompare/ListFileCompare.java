/**
 * 
 */
package iglezias.listFileCompare;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.checkerframework.checker.nullness.qual.Nullable;

import com.google.common.base.Predicate;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Multimap;

import iglezias.util.UtilitariosArquivo;

/**
 * @author ligle
 *
 */
public class ListFileCompare {
	
	/**
	 * em Bytes
	 */
	public static final long toleranciaTamArq = 4*1024;

	/**
	 * em Milisegundos 
	 */
	protected static final long toleranciaMsDtArq = 1 * 60 * 60 *1000; //1h
	
	public static final File pastaArquivosParaConferir = new File("C:\\tempOrganizar\\ParaOrganizar\\2021-11-16\\");
	
	public static final File[] pastasParaBuscarExistencia = new File[] {
		new File("F:\\tempOrganizar\\")
		, new File("D:\\ImagensCompartilhadas\\FotosAtuais\\FotosPorData\\")
		//, new File("D:\\ImagensCompartilhadas\\FotosAtuais\\FotosPorData\\2020\\")	
	};
	
	public static final File pastaMoverNaoConferidos = new File("C:\\tempOrganizar\\ParaOrganizar\\2021-11-16Faltantes\\");// new File("C:\\tempOrganizar\\ParaOrganizar\\");
		
	
	private static final Long dtHojeComp = new Date().getTime() - (48 * 60 * 60 * 1000); //anteontem

	
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		System.out.println("Inicio.... ");
		final List<File> paraConferir = UtilitariosArquivo.getArquivosDiretorio(pastaArquivosParaConferir);
		Multimap<String, File> buscas = ArrayListMultimap.create();
		for (File pasta : pastasParaBuscarExistencia) {
			for (File arq : UtilitariosArquivo.getArquivosDiretorio(pasta)) {
				buscas.put( arq.getName().toUpperCase(), arq); //se arq mesmo nome em mais de um path, adiciona varios arqs na mesma chave (chave eh apenas o nome do arquivo)
			}
		}
		
		ImmutableList<File> faltantes = FluentIterable.from(paraConferir)
			.filter(new Predicate<File>() { //filtra arquivos que NAO estejam nos arquivos paraConferir
				@Override
				public boolean apply(@Nullable File arqParaConferir) {
					File arqAchado = null;
					Collection<File> arqsConferencia = buscas.get(arqParaConferir.getName().toUpperCase());
					if (arqsConferencia!=null && !arqsConferencia.isEmpty()) {
						File arqMenorDif=null;
						Long menorDif=Long.MAX_VALUE;
						for (File fCoincidente : arqsConferencia) {
							long difSize = Math.abs(fCoincidente.length() -  arqParaConferir.length() );
							if (difSize<menorDif) {
								menorDif = difSize;
								arqMenorDif=fCoincidente;
							}
						}
						
						if (menorDif<=toleranciaTamArq) {
							arqAchado = arqMenorDif;
							System.out.println(arqMenorDif.getAbsolutePath() + "["+tamArq(arqMenorDif.length())+"]"
									+ "\t\t\torig="+arqParaConferir.getAbsolutePath()  + "["+tamArq(arqParaConferir.length())+"]");
						} else if (arqMenorDif.lastModified() < dtHojeComp
								&& arqParaConferir.lastModified() < dtHojeComp
								&& Math.abs(arqMenorDif.lastModified() - arqParaConferir.lastModified()) < toleranciaMsDtArq
						) {
							arqAchado = arqMenorDif;
							System.out.println("Alerta! "+arqMenorDif.getAbsolutePath() + "["+tamArq(arqMenorDif.length())+" Dt:"+dtArq(arqMenorDif)+"]"
									+ "\t\t\torig="+arqParaConferir.getAbsolutePath()  + "["+tamArq(arqParaConferir.length())+" Dt:"+dtArq(arqParaConferir)+"]"
							);
						} else {
							System.err.println("DIF size: "+arqMenorDif.getAbsolutePath() + "["+tamArq(arqMenorDif.length())+" Dt:"+dtArq(arqMenorDif)+"]" 
									+ "\t\t\torig="+arqParaConferir.getAbsolutePath()  + "["+tamArq(arqParaConferir.length())+" Dt:"+dtArq(arqParaConferir)+"]"
							);
						}
						
					}
					return arqAchado==null; 
				}
			})
			.toList();
		
		System.out.println();
		System.out.println();
		System.out.println();
		
		System.err.println("Faltam os seguintes: ("+faltantes.size()+")");
		for (File arqFaltante : faltantes) {
			System.err.println(arqFaltante.getAbsolutePath());
			if (pastaMoverNaoConferidos!=null && pastaMoverNaoConferidos.exists()) {
				UtilitariosArquivo.moveArquivoOuPastaPreservandoPastaOrigem(arqFaltante, pastaMoverNaoConferidos, pastaArquivosParaConferir);
			}
		}
		

		System.out.println("\n\n\nFim. ");

	}

	
	protected static SimpleDateFormat fmtData = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	protected static String dtArq(@Nullable File arqParaConferir) {
		return fmtData.format( new Date(arqParaConferir.lastModified()));
	}

	public static String tamArq(long length) {
		return UtilitariosArquivo.formataNumeroBytes(length);
	}

}
