/**
 * 
 */
package iglezias.apendices;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.checkerframework.checker.nullness.qual.Nullable;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import iglezias.util.Dupla;
import iglezias.util.UtilitariosArquivo;
import iglezias.util.Utils;

/**
 * @author iglezias
 *
 */
public class ComparaListaArquivos {

	//private static String pasta1 = "L:\\tempOrganizar\\Videos_NetGravacoes_convertidas\\";
	private static String pasta1 = "D:\\tempExcluir\\conv\\doneConvert\\";
	//private static String pasta1 = "L:\\tempOrganizar\\VideosNET_TS_ConverterParaMKV\\";
	private static String pasta2 = "D:\\tempExcluir\\conv\\coded\\";
	
	private static String pastaMove = "D:\\tempExcluir\\conv\\coded_comBatimento\\";

	public static void main(String[] args) {
		mostraDiferenca(pasta1, pasta2);
	}
	
	private static void mostraDiferenca(String pasta12, String pasta22) {
		System.out.println("Listando arquivos das pastas...");
		File path1 = new File(pasta1);
		List<File> lista1 = Utils.listaPathsArquivosDiretorioRecursivamente(path1, 0);

		File path2 = new File(pasta2); 
		List<File> lista2 = Utils.listaPathsArquivosDiretorioRecursivamente(path2, 0);
		System.out.println("FIM Listando arquivos das pastas...\n\n\n");
		
		ImmutableSet<String> l1 = FluentIterable.from(lista1)
				.transform(new Function<File,String>() {
					@Override
					public @Nullable String apply(@Nullable File input) {
						return UtilitariosArquivo.extraiNomeAquivoSemExtensao(input.getName());
					}
				})
				.toSet();
		
		final FluentIterable<File> iterador2 = FluentIterable.from(lista2); 
		
		ImmutableList<File> doisMenosUm = iterador2.filter(new Predicate<File>() {
			@Override
			public boolean apply(@Nullable File input) {
				return ! l1.contains(UtilitariosArquivo.extraiNomeAquivoSemExtensao(input.getName()));
			}
		}).toList();
		
		System.out.println("path2 - path1 ("+pasta22+" - "+ pasta12+")");
		for (File file : doisMenosUm) {
			System.out.println(file.getName());
		}
		System.out.println("FIM diferenca");
	}

	/**
	 * @param args
	 */
	public static void main2(String[] args) {
		System.out.println("Listando arquivos das pastas...");
		File path1 = new File(pasta1);
		List<File> lista1 = Utils.listaPathsArquivosDiretorioRecursivamente(path1, 0);

		File path2 = new File(pasta2); 
		List<File> lista2 = Utils.listaPathsArquivosDiretorioRecursivamente(path2, 0);
		System.out.println("FIM Listando arquivos das pastas...\n\n\n");
		
		final FluentIterable<File> iterador2 = FluentIterable.from(lista2); 
		
		final File fpastaMove = new File(pastaMove);
		fpastaMove.mkdirs();
		
		final List<Dupla<File, File>> mover = new ArrayList<>();
		
		System.out.println("Arquivos nos 2 lugares: ");
		FluentIterable<File> itResult = FluentIterable.from(lista1).filter(new Predicate<File>() {
			@Override
			public boolean apply(@Nullable final File f1) {
				return iterador2.anyMatch(new Predicate<File>() {
					@Override
					public boolean apply(@Nullable File f2) {
						String nome1 = getLadoEsquerdoPonto(Utils.extraiNomeAquivoSemExtensao(f1.getName()));
						String nome2 = getLadoEsquerdoPonto(Utils.extraiNomeAquivoSemExtensao(f2.getName()));
						
						boolean res = nome1.equalsIgnoreCase(nome2) && !f1.getAbsolutePath().equalsIgnoreCase(f2.getAbsolutePath());
						if (res) {
							System.out.println("1="+f1.getAbsolutePath()+" ["+f1.length()/1024/1024+"]\t\t\t\t2="+f2.getAbsolutePath()+" ["+f2.length()/1024/1024+"]");
							
							File arqMover;
							String nomeArqPastaMover;
							if (f1.length()>f2.length()) {
								arqMover = f1;
								nomeArqPastaMover = f2.getName();
							} else {
								arqMover = f2;
								nomeArqPastaMover = f1.getName();
							}
							mover.add(new Dupla<File,File>(arqMover, new File(fpastaMove, nomeArqPastaMover)));
//							try {
//								//java.nio.file.Files.move( arqMover.toPath(), new File(fpastaMove, nomeArqPastaMover).toPath()); //nao move agora, pq o f.lenght() ve na hora, se estiver movido, retorna zero...
//							} catch (IOException e) {
//								e.printStackTrace();
//							}
						}
						return res;
					}
				});
			}
		});

		ImmutableList<File> res = itResult.toList();//necessario, para haver a iteracao dos filtros acima 
		for (File f: res) { 
			//System.out.println(f.getAbsolutePath());
		}

		System.out.println("\n\n\nInicio movendo para: ["+fpastaMove.getAbsolutePath()+"]");
		for (Dupla<File,File> moveAtu : mover) {
			try {
				java.nio.file.Files.move( moveAtu.o1.toPath(), moveAtu.o2.toPath());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		System.out.println("FIM Inicio movendo para: ["+fpastaMove.getAbsolutePath()+"]");
		
	}
	
	public static String getLadoEsquerdoPonto(String str) {
		int idx = str.indexOf('.');
		if (idx<0) {
			return str;
		} else {
			return str.substring(0, idx);
		}
	}

}
